//
//  HomeTableViewController.m
//  OnDemand Timesheets
//
//  Created by Joey Kjornes on 3/16/17.
//  Copyright © 2017 Jed Mahonis Group. All rights reserved.
//

#import "HomeTableViewController.h"
#import "UIImageView+AFNetworking.h"
#import "EditWorkerProfileTableViewController.h"
#import "LoginTableViewController.h"
#import "RSVP.h"
#import "Request.h"
#import "DirectRequestDetailViewController.h"

@interface HomeTableViewController () {
    NSMutableArray *directRequestsArray;
}

@end

@implementation HomeTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"PCA OnDemand";
    _currentUser = User.currentUser.copy;
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    } else {
        // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions =
        UNAuthorizationOptionAlert
        | UNAuthorizationOptionSound
        | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
        }];
        
        // For iOS 10 data message (sent via FCM)
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
#endif
    }
    NSString *logoURLString = [NSString stringWithFormat:@"%@", User.currentUser.agency.logoURL];
    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:logoURLString]]];
   // self.agencyImage.image = [UIImage imageNamed:logoURLString];
    if ([logoURLString isEqualToString:@""]) {
        self.helloUserLabel.text = [NSString stringWithFormat:@"Hello, %@ %@", User.currentUser.pca.firstName, User.currentUser.pca.lastName];
    }else{
        
    self.helloUserLabel.text = @" ";
    [self.agencyImage setImage:image];
    }
    self.navigationItem.hidesBackButton = YES;

  //  self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Logout" style:UIBarButtonItemStylePlain target:self action:@selector(logout)];
    
    self.findShiftsButton.layer.borderColor = [UIColor colorWithRed:55.0/255.0 green:99.0/255.0 blue:135.0/255.0 alpha:1.0].CGColor;
    self.findShiftsButton.layer.borderWidth = 1.0f; //make border 1px thick
    self.seeShiftsButton.layer.borderColor = [UIColor colorWithRed:55.0/255.0 green:99.0/255.0 blue:135.0/255.0 alpha:1.0].CGColor;
    self.seeShiftsButton.layer.borderWidth = 1.0f; //make border 1px thick
    
//    _locManager = [[CLLocationManager alloc] init];
//    _locManager.delegate = self;
//    _locManager.distanceFilter = kCLDistanceFilterNone;
//    _locManager.desiredAccuracy = kCLLocationAccuracyBest;
//    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
//        [self.locManager requestWhenInUseAuthorization];
//    [_locManager startUpdatingLocation];

    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways) {
        _locManager = [[CLLocationManager alloc] init];
        _locManager.delegate = self;
        _locManager.distanceFilter = kCLDistanceFilterNone;
        _locManager.desiredAccuracy = kCLLocationAccuracyBest;
        [self.locManager requestWhenInUseAuthorization];
        [_locManager startUpdatingLocation];
    }
    directRequestsArray = [NSMutableArray new];
    [self getDataForRequests];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getDataForRequests {
    if (self.currentUser.pca.directRequests.count > 0) {
        Request *request = self.currentUser.pca.directRequests[0];
        DirectRequestDetailViewController *vc = [[UIStoryboard storyboardWithName:@"DirectRequestDetail" bundle:nil] instantiateViewControllerWithIdentifier:@"DirectRequestDetailViewController"];
        vc.request = request;
        [self presentViewController:vc animated:NO completion:NULL];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
}

-(IBAction)settingButtonPressed:(UIButton *)button {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [[actionSheet popoverPresentationController] setSourceView:self.view];
    [[actionSheet popoverPresentationController] setSourceRect:CGRectMake((self.view.frame.size.width) - 50,-200,100,200)];
    [[actionSheet popoverPresentationController] setPermittedArrowDirections:UIPopoverArrowDirectionUp];

    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Edit Profile" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self editProfileTouched];
    }]];
    
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Log Out" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self logoutTouched];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:YES completion:^{

        }];
    }]];

    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

-(void)dismissPopover {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

-(void)editProfileTouched {
    EditWorkerProfileTableViewController *vc = [[UIStoryboard storyboardWithName:@"OnDemandWorker" bundle:nil] instantiateViewControllerWithIdentifier:@"EditWorkerProfileTableViewController"];
    [self.navigationController showViewController:vc sender:self];
}

-(void)logoutTouched {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Confirm" message:@"Are you sure you want to logout?" preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:NULL]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
      //  LoginTableViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"loginTableViewController"];
     //   [self.navigationController showViewController:vc sender:self];
        [UIApplication sharedApplication].keyWindow.rootViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"mainNavCont"];

      //  [self.navigationController popToRootViewControllerAnimated:YES];
    }]];
    
    alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
    [self presentViewController:alert animated:YES completion:NULL];
}

#pragma mark - Table view data source



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 360;
    }else if (indexPath.row == 2){
        return 200;
    }
    return 80;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"optionsPopoverSegue"]) {
        UserSettingsOptionsTableViewController *vc = [segue destinationViewController];
        vc.modalPresentationStyle = UIModalPresentationPopover;
        vc.popoverPresentationController.delegate = self;
        vc.delegate = self;
    }
}


-(UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller {
    return UIModalPresentationNone;
}


#pragma mark - Get Location Permission

-(void)getLocationPermission {
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways) {
        
    } else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
        _locManager = [CLLocationManager new];
        _locManager.delegate = self;
        [_locManager requestWhenInUseAuthorization];
    } else {
        NSString *appName = [[[NSBundle mainBundle] infoDictionary] objectForKey:(id)kCFBundleNameKey];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Location" message:[NSString stringWithFormat:@"To get the most out of %@, it is best to have location enabled. Would you like to change this in your phone settings?", appName] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Sure" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self goToPhoneSettings];
        }]];
        //        [alert addAction:[UIAlertAction actionWithTitle:@"No thanks" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        //            AddressDetailViewController *vc = [[UIStoryboard storyboardWithName:@"JobDetail" bundle:nil] instantiateViewControllerWithIdentifier:@"AddressDetailViewController"];
        //            vc.office = _assignment.office;
        //            [self.navigationController showViewController:vc sender:self];
        //        }]];
        [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:NULL]];
        [self presentViewController:alert animated:YES completion:NULL];
    }
}

-(void)goToPhoneSettings {
    NSURL *appSettings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    [[UIApplication sharedApplication] openURL:appSettings];
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == kCLAuthorizationStatusAuthorizedAlways || status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [self getLocationPermission];
    } else if (status == kCLAuthorizationStatusNotDetermined) {
        [self.locManager requestWhenInUseAuthorization];
    } else if (status == kCLAuthorizationStatusDenied || kCLAuthorizationStatusRestricted) {
        NSString *appName = [[[NSBundle mainBundle] infoDictionary] objectForKey:(id)kCFBundleNameKey];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Location" message:[NSString stringWithFormat:@"To get the most out of %@, it is best to have location enabled. Would you like to change this in your phone settings?", appName] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self goToPhoneSettings];
        }]];
        //        [alert addAction:[UIAlertAction actionWithTitle:@"No thanks" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        //            AddressDetailViewController *vc = [[UIStoryboard storyboardWithName:@"JobDetail" bundle:nil] instantiateViewControllerWithIdentifier:@"AddressDetailViewController"];
        //            vc.office = _assignment.office;
        //            [self.navigationController showViewController:vc sender:self];
        //        }]];
        [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:NULL]];
        [self presentViewController:alert animated:YES completion:NULL];
    }
}




-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    [self.locManager stopUpdatingLocation];
    self.locManager = manager;
    if (locations.count > 0) {
    }
    
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
