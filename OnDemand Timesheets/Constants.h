//
//  Constants.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 3/25/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#ifndef Homecare_Timesheet_Demo_Constants_h
#define Homecare_Timesheet_Demo_Constants_h


#ifdef DEBUG
#define kURL @"https://staging.homecaretimesheetsapp.com/api/v1"
#define kClientID @"22_1gzq7xm54rms4000o04gcgckkso4gs0o8g4s00g8s0sw44socc"
#else
#define kURL @"https://api.homecaretimesheetsapp.com/api/v1"
#define kClientID @"11_6cr2zvwer3sww48c8g8so0s88kowo4ggwkg8o8kwogkss0s48g"
#endif

#ifdef DEBUG
#define kDebug YES
#else
#define kDebug NO
#endif


#define kDateFormat @"yyyy'-'MM'-'dd'T'HH':'mm':'ssZZZ"
#define kDirectRequestDateFormat @"yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSSZ"
#define kDisplayDateFormat @"MMM dd, hh:mm a"

#endif
