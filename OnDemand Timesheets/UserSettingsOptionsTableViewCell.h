//
//  UserSettingsOptionsTableViewCell.h
//  OnDemand Timesheets
//
//  Created by Joey Kjornes on 4/25/17.
//  Copyright © 2017 Jed Mahonis Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserSettingsOptionsTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *optionNameLabel;

@end
