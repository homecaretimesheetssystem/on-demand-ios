//
//  PCA.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 1/22/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"

typedef NS_ENUM(NSUInteger, Gender) {
    Gender_NotSpecified,
    Gender_Female,
    Gender_Male,
};

@interface PCA : NSObject <NSCopying, NSCoding>

@property (nonatomic, strong) NSNumber *idNumber;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *umpi;
@property (nonatomic, strong) NSDictionary *randomSessionData;
@property (nonatomic, strong) NSNumber *companyAssignedID;
@property (nonatomic, strong) NSArray *servicesArray;
@property (nonatomic, strong) NSNumber *isAvailable;
@property (nonatomic) Gender gender;
@property (nonatomic, strong) NSString *profileImageURL;
@property (nonatomic) BOOL hasFilledOutProfile;
@property (nonatomic, strong) NSString *homeAddress;
@property (nonatomic, strong) NSMutableArray *countyArray;
@property (nonatomic, strong) NSString *aboutMe;
@property (nonatomic, strong) NSMutableArray *directRequests;
@property (nonatomic, strong) NSString *phoneNumber;
@property (nonatomic, strong) NSString *email;

// Qualifications
@property (nonatomic, strong) NSMutableArray *qualificationsArray;
@property (nonatomic) BOOL cprCertified;
@property (nonatomic) BOOL experienceWithBehaviors;
@property (nonatomic) BOOL experienceWithChildren;
@property (nonatomic) BOOL experienceMakingTransfers;
@property (nonatomic) NSString *yearsOfExperience;
@property (nonatomic) BOOL hasDriversLicense;
@property (nonatomic) BOOL available;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;
-(NSString *)stringForGender;

@end
