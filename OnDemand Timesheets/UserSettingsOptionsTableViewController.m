//
//  UserSettingsOptionsTableViewController.m
//  airvūz
//
//  Created by Scott Mahonis on 2/20/17.
//  Copyright © 2017 JMG. All rights reserved.
//

#import "UserSettingsOptionsTableViewController.h"

@interface UserSettingsOptionsTableViewController ()

@end

@implementation UserSettingsOptionsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _optionsArray = [NSArray arrayWithObjects:@"Edit Profile", @"Log Out", nil];
    [self.tableView reloadData];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _optionsArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UserSettingsOptionsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserSettingsOptionsTableViewCell" forIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.optionNameLabel.text = _optionsArray[indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.delegate dismissPopover];
    switch (indexPath.row) {
        case 0:
            [self.delegate editProfileTouched];
            break;
        case 1:
            [self.delegate logoutTouched];
            break;
            
        default:
            break;
    }
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
