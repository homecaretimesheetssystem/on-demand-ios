//
//  DirectRequestDetailViewController.m
//  OnDemand Timesheets
//
//  Created by Scott Mahonis on 3/26/18.
//  Copyright © 2018 Jed Mahonis Group. All rights reserved.
//

#import "DirectRequestDetailViewController.h"

@interface DirectRequestDetailViewController ()

@end

@implementation DirectRequestDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self formatUIWithRequest];
    self.view.backgroundColor = [UIColor colorWithWhite:0.5 alpha:1.0];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)formatUIWithRequest {
    self.acceptButton.layer.borderWidth = 1;
    self.declineButton.layer.borderWidth = 1;
    self.acceptButton.layer.borderColor = self.acceptButton.titleLabel.textColor.CGColor;
    self.declineButton.layer.borderColor = self.declineButton.titleLabel.textColor.CGColor;
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:kDateFormat];
    NSDate *startDate = [formatter dateFromString:self.request.shiftStartTime];
    NSDate *endDate = [formatter dateFromString:self.request.shiftEndTime];
    [formatter setDateFormat:kDisplayDateFormat];
    [UIView animateWithDuration:1.0 animations:^{
        self.recipientNameLabel.text = [NSString stringWithFormat:@"%@ %@", self.request.recipient.firstName, self.request.recipient.lastName];
        self.startTimeLabel.text = [NSString stringWithFormat:@"Start Time: %@", [formatter stringFromDate:startDate]];
        self.endTimeLabel.text = [NSString stringWithFormat:@"End Time:   %@", [formatter stringFromDate:endDate]];
        self.addressLabel.text = [NSString stringWithFormat:@"Address: %@", self.request.recipientAddress];
        self.additionalRequirementsLabel.text = [NSString stringWithFormat:@"Additional Requirements: %@", self.request.additionalRequirements.length > 0 ? self.request.additionalRequirements : @"None"];
        [self showButtonsAndStopActivityIndicator];
    }];
}

-(void)showButtonsAndStopActivityIndicator {
    self.acceptButton.alpha = 1.0;
    self.declineButton.alpha = self.acceptButton.alpha;
    [self.activityIndicator stopAnimating];
}

-(IBAction)acceptButtonPressed:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Confirm" message:@"By accepting this Shift, you agree to arrive at the stated time and provide services according to recipient's request, and in accordance with all rules and laws rules related to services." preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:NULL]];
    [alert addAction:[UIAlertAction actionWithTitle:@"I Agree" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSDate *currentDate = [NSDate date];
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setDateFormat:kDateFormat];
        NSString *dateStr = [format stringFromDate:currentDate];
        self.acceptButton.alpha = 0.0;
        self.declineButton.alpha = self.acceptButton.alpha;
        [self.activityIndicator startAnimating];
        [NetworkingHelper postAnRSVPWithPCAID:User.currentUser.pca.idNumber andRequestID:self.request.requestID andETA:dateStr andCancellationReason:@"" andAcceptedBool:YES andDeclinedBool:NO andSuccess:^(id responseObject) {
            [self showButtonsAndStopActivityIndicator];
            
            //  NSLog(@"success rsvp %@", responseObject);
            //       NSMutableArray *mutArr = [NSMutableArray new];
            //     for (NSDictionary *dict in responseObject) {
            //       RSVP *rsvp = [[RSVP alloc] initWithDictionary:dict];
            //        [mutArr addObject:rsvp];
            //    }
            //    NSLog(@"mut array %@", mutArr);
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Success" message:@"Your RSVP has been sent to the recipient. You will be notified if they cancel your RSVP." preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self handleSuccessfulAcceptOrDeclineOfRequest];
            }]];
            alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
            [self presentViewController:alert animated:YES completion:NULL];
            
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            [self showButtonsAndStopActivityIndicator];
            NSLog(@"failure %@", error);
            id responseObject = [NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:0 error:nil];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:[NSString stringWithFormat:@"%@", responseObject[@"error"]] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            }]];
            [alert addAction:[UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                [self dismissViewControllerAnimated:YES completion:NULL];
            }]];
            alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
            [self presentViewController:alert animated:YES completion:NULL];
            
        }];
        
    }]];
    alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
    [self presentViewController:alert animated:YES completion:NULL];
}

-(IBAction)declineButtonPressed:(id)sender {
    NSDate *currentDate = [NSDate date];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:kDateFormat];
    NSString *dateStr = [format stringFromDate:currentDate];
    self.acceptButton.alpha = 0.0;
    self.declineButton.alpha = self.acceptButton.alpha;
    [self.activityIndicator startAnimating];
    [NetworkingHelper postAnRSVPWithPCAID:User.currentUser.pca.idNumber andRequestID:self.request.requestID andETA:dateStr andCancellationReason:@"" andAcceptedBool:NO andDeclinedBool:YES andSuccess:^(id responseObject) {
        [self showButtonsAndStopActivityIndicator];
        [self handleSuccessfulAcceptOrDeclineOfRequest];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self showButtonsAndStopActivityIndicator];
        NSLog(@"failure %@", error);
        id responseObject = [NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:0 error:nil];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:[NSString stringWithFormat:@"%@", responseObject[@"error"]] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:NULL]];
        [alert addAction:[UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [self dismissViewControllerAnimated:YES completion:NULL];
        }]];
        alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
        [self presentViewController:alert animated:YES completion:NULL];
        
    }];
    
}

-(void)handleSuccessfulAcceptOrDeclineOfRequest {
    User *currUser = User.currentUser;
    NSMutableArray *directRequestsArray = currUser.pca.directRequests;
    if (directRequestsArray.count > 0) {
        [directRequestsArray removeObjectAtIndex:0];
        [currUser save];
    }
    if (directRequestsArray.count > 0) {
       
        self.request = directRequestsArray[0];
        [self formatUIWithRequest];
    } else {
        [self dismissViewControllerAnimated:NO completion:NULL];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
