//
//  PushNotificationView.m
//  Kindu
//
//  Created by Scott Mahonis on 1/15/16.
//  Copyright © 2016 The Jed Mahonis Group. All rights reserved.
//

#import "PushNotificationView.h"
#import "AppDelegate.h"

@implementation PushNotificationView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(instancetype)initWithFrame:(CGRect)frame {
    if (self == [super initWithFrame:frame]) {
        self.layer.cornerRadius = 12;
        self.clipsToBounds = YES;
//        self.backgroundColor = [UIColor colorWithWhite:0.9 alpha:0.5];
      //  self.backgroundColor = [UIColor clearColor];
        self.backgroundColor = [UIColor lightGrayColor];
        UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleRegular];
        
        UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blur];
        visualEffectView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
        [self addSubview:visualEffectView];
        
        UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 30)];
        topView.backgroundColor = [UIColor colorWithWhite:0.95 alpha:0.75];
        [visualEffectView.contentView addSubview:topView];
        
        NSArray *iconsArray = [[NSBundle mainBundle] infoDictionary][@"CFBundleIcons"][@"CFBundlePrimaryIcon"][@"CFBundleIconFiles"];
        NSString *appIconImageName = [iconsArray lastObject];
        UIImageView *image = [[UIImageView alloc] initWithImage:[UIImage imageNamed:appIconImageName]];
        image.frame = CGRectMake(8, 5, 20, 20);
        image.layer.cornerRadius = 8;
        image.clipsToBounds = YES;
        _imageView = image;
        [visualEffectView.contentView addSubview:_imageView];
        
        
        UILabel *topLabel = [[UILabel alloc] initWithFrame:CGRectMake(_imageView.frame.origin.x + _imageView.frame.size.width + 8, _imageView.frame.origin.y, frame.size.width - 8 - (_imageView.frame.origin.x + _imageView.frame.size.width + 8), _imageView.frame.size.height)];
        NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
//        NSLog(@"infoDict:%@", infoDict);
        topLabel.text = infoDict[@"CFBundleDisplayName"];
        topLabel.textColor = [UIColor colorWithWhite:0.33 alpha:1.0];
        topLabel.font = [UIFont systemFontOfSize:14.0 weight:UIFontWeightLight];
        NSAttributedString *attributedAppName = [[NSAttributedString alloc] initWithString:topLabel.text attributes:@{NSFontAttributeName:topLabel.font}];
        CGRect topLabelFrame = topLabel.frame;
        topLabelFrame.size.width = attributedAppName.size.width;
        topLabel.frame = topLabelFrame;
        [visualEffectView.contentView addSubview:topLabel];
        
        
        UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(topView.frame.size.width - 38, topLabelFrame.origin.y + 1, 30, topLabelFrame.size.height)];
        timeLabel.text = @"now";
        timeLabel.textColor = [UIColor colorWithWhite:0.66 alpha:1.0];
        timeLabel.font = [UIFont systemFontOfSize:11.0 weight:UIFontWeightRegular];
        [visualEffectView.contentView addSubview:timeLabel];
        
        
        UILabel *bottomLabel = [[UILabel alloc] initWithFrame:CGRectMake(_imageView.center.x, topView.frame.origin.y + topView.frame.size.height, frame.size.width - 8 - (_imageView.frame.origin.x + _imageView.frame.size.width + 8), self.frame.size.height - (topLabel.frame.origin.y + topLabel.frame.size.height + 4))];
        bottomLabel.textColor = [UIColor colorWithRed:14./255. green:86./255. blue:120./255. alpha:1.0];
        bottomLabel.font = [UIFont systemFontOfSize:14.0 weight:UIFontWeightSemibold];
        bottomLabel.userInteractionEnabled = NO;
        bottomLabel.backgroundColor = [UIColor clearColor];//self.backgroundColor;
        bottomLabel.numberOfLines = 2;
        _messageLabel = bottomLabel;
        [visualEffectView.contentView addSubview:_messageLabel];
        
        UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
        aButton.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
        [aButton addTarget:self action:@selector(buttonTouched:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:aButton];
        
        
    }
    return self;
}

-(void)buttonTouched:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate notificationBannerTouched:sender withPushNotification:self.pushNotification];
}

@end
