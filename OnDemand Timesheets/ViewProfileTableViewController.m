//
//  ViewProfileTableViewController.m
//  OnDemand Timesheets
//
//  Created by Joey Kjornes on 5/22/17.
//  Copyright © 2017 Jed Mahonis Group. All rights reserved.
//

#import "ViewProfileTableViewController.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "Qualifications.h"
#import "SendDirectRequestTableViewController.h"

@interface ViewProfileTableViewController ()

@end

@implementation ViewProfileTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *logoURLString = [NSString stringWithFormat:@"https://s3.amazonaws.com/hc-timesheets-demo/%@", self.pca.profileImageURL];
    [self.pcaImage setImageWithURL:[NSURL URLWithString:logoURLString] placeholderImage:[UIImage imageNamed:@"Icon"]];
    self.pcaImage.layer.cornerRadius = self.pcaImage.frame.size.height /2;
    self.pcaImage.clipsToBounds = YES;
    self.title = [NSString stringWithFormat:@"%@ %@", self.pca.firstName, self.pca.lastName];

    NSMutableString *string = [NSMutableString new];
    for (int i = 0; i < self.pca.qualificationsArray.count; i++) {
        Qualifications *qual = self.pca.qualificationsArray[i];
        [string appendString:i == self.pca.qualificationsArray.count - 1 ? qual.qualificationName : [NSString stringWithFormat:@"%@\r", qual.qualificationName]];
    }
    if (string.length == 0) {
        string = [[NSMutableString alloc] initWithString:@"This PCA has not set their Qualifications yet."];
    }
    self.pcaQualificationsTextView.text = string.copy;
    self.pcaQualificationsTextView.textContainerInset = UIEdgeInsetsZero;
    self.pcaQualificationsTextView.textContainer.lineFragmentPadding = 0.0;
    self.pcaNameLabel.text = [NSString stringWithFormat:@"%@ %@", self.pca.firstName, self.pca.lastName];
    self.pcaGenderLabel.text = self.pca.stringForGender;
    self.pcaYOELabel.text = self.pca.yearsOfExperience;
    
    
    self.aboutPCATextView.textContainerInset = UIEdgeInsetsZero;
    self.aboutPCATextView.textContainer.lineFragmentPadding = 0.0;


    
    self.aboutPCATextView.editable = NO;
    self.aboutPCATextView.dataDetectorTypes = UIDataDetectorTypeAll;
    
    self.aboutPCALabel.text = [NSString stringWithFormat:@"About %@ %@:", self.pca.firstName, self.pca.lastName];
    self.aboutPCATextView.text = [NSString stringWithFormat:@"%@\r\r%@\r\r%@", self.pca.aboutMe, self.pca.phoneNumber, self.pca.email];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0 || section == 3) {
        return 1;
    }else if (section == 1){
        return 3;
    }else if (section == 2){
        return 1;
    }
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return @" ";
    }else if (section == 1){
        return @"Personal Information";
    }else if (section == 2){
        return @"Qualifications";
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[UIColor colorNamed:@"ButtonColor"]];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 250;
    }
    if (indexPath.section == 1) {
        if (indexPath.row == 2) {
            return 45. + [self heightForLabelForString:[NSString stringWithFormat:@"%@\r\r%@\r\r%@", self.pca.aboutMe, self.pca.phoneNumber, self.pca.email]];
        }
    }
    if (indexPath.section == 2) {
            return 12. + [self heightForTextViewForString:self.pcaQualificationsTextView.text];
        }
    
    return 50;
}


-(CGFloat)heightForLabelForString:(NSString *)string {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 1)];
    label.numberOfLines = 0;
    label.attributedText = [[NSAttributedString alloc] initWithString:string attributes:@{NSFontAttributeName:[UIFont preferredFontForTextStyle:UIFontTextStyleBody]}];
    [label sizeToFit];
    return label.frame.size.height;
}


-(CGFloat)heightForTextViewForString:(NSString *)string {
    UITextView *tv = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width -24, 1)];
    tv.attributedText = [[NSAttributedString alloc] initWithString:string attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:17.]}];
    [tv sizeToFit];
    return tv.frame.size.height;
}



/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (indexPath.section == 2) {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"qualificationStaticCell" forIndexPath:indexPath];
        Qualifications *q = self.qualificationsArray[0];
        self.pcaQualificationsTextView.text = q.qualificationName;
    return cell;
    }
    return nil;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.destinationViewController isKindOfClass:[SendDirectRequestTableViewController class]]) {
        SendDirectRequestTableViewController *vc = (SendDirectRequestTableViewController*)segue.destinationViewController;
        vc.pca = self.pca;
    }
}


@end
