//
//  Shift.h
//  OnDemand Timesheets
//
//  Created by Joey Kjornes on 3/13/17.
//  Copyright © 2017 Jed Mahonis Group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Request.h"
#import "RSVP.h"

@interface Shift : NSObject


@property (nonatomic, strong) NSNumber *accepted;
@property (nonatomic, strong) NSNumber *declined;
@property (nonatomic, strong) NSString *etaString;
@property (nonatomic, strong) NSNumber *rsvpID;
@property (nonatomic, strong) Request *request;
@property (nonatomic, strong) RSVP *rsvp;
@property (nonatomic, strong) NSArray *requestsArray;
@property (nonatomic, strong) NSArray *rsvpsArray;


@property (nonatomic, strong) NSString *recipientAddress;
@property (nonatomic, strong) NSString *shiftStartTime;
@property (nonatomic, strong) NSString *shiftEndTime;
@property (nonatomic, strong) NSNumber *genderPreference;


-(instancetype)initWithDictionary:(NSDictionary *)dictionary;



@end
