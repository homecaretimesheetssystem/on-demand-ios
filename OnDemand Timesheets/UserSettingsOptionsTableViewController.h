//
//  UserSettingsOptionsTableViewController.h
//  airvūz
//
//  Created by Scott Mahonis on 2/20/17.
//  Copyright © 2017 JMG. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol UserSettingsOptionsTableViewControllerDelegate <NSObject>

-(void)editProfileTouched;
-(void)logoutTouched;
-(void)dismissPopover;

@end

#import "UserSettingsOptionsTableViewCell.h"

@interface UserSettingsOptionsTableViewController : UITableViewController

@property (nonatomic, strong) NSArray *optionsArray;
@property (nonatomic) id<UserSettingsOptionsTableViewControllerDelegate>delegate;

@end
