//
//  UpcomingShiftsViewController.m
//  OnDemand Timesheets
//
//  Created by Joey Kjornes on 3/16/17.
//  Copyright © 2017 Jed Mahonis Group. All rights reserved.
//

#import "UpcomingShiftsViewController.h"
#import "EditWorkerProfileTableViewController.h"
#import "NetworkingHelper.h"
#import "UpcomingShiftsTableViewCell.h"
#import "AcceptedShiftViewController.h"
#import "LoadingTableViewCell.h"
#import "LoginTableViewController.h"

@interface UpcomingShiftsViewController ()

@end

@implementation UpcomingShiftsViewController{

BOOL loadingShifts;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _upcomingShiftsArray = [NSMutableArray new];
    [self loadUpcomingShifts];
    loadingShifts = YES;
    _locManager = [[CLLocationManager alloc] init];
    _locManager.delegate = self;
    _locManager.distanceFilter = kCLDistanceFilterNone;
    _locManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locManager requestWhenInUseAuthorization];
    [_locManager startUpdatingLocation];
    self.title = @"My Shifts";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshRequests:) name:@"push_type=3" object:nil];

    //self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Edit Profile" style:UIBarButtonItemStylePlain target:self action:@selector(editProfileButtonPressed)];
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:@selector(backButtonPressed:)];
//    self.navigationItem.rightBarButtonItem = saveButton;



    // Do any additional setup after loading the view.
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dismissPopover {
    [self dismissViewControllerAnimated:YES completion:NULL];
}


-(IBAction)settingButtonPressed:(UIButton *)button {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [[actionSheet popoverPresentationController] setSourceView:self.view];
    [[actionSheet popoverPresentationController] setSourceRect:CGRectMake((self.view.frame.size.width) - 50,-200,100,200)];
    [[actionSheet popoverPresentationController] setPermittedArrowDirections:UIPopoverArrowDirectionUp];

    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Edit Profile" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self editProfileTouched];
    }]];
    
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Log Out" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self logoutTouched];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:YES completion:^{

        }];
    }]];

    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

-(void)editProfileTouched{
    EditWorkerProfileTableViewController *vc = [[UIStoryboard storyboardWithName:@"OnDemandWorker" bundle:nil] instantiateViewControllerWithIdentifier:@"EditWorkerProfileTableViewController"];
    [self.navigationController showViewController:vc sender:self];
    
}

- (void)refreshRequests:(NSNotification *)notification{
    [self loadUpcomingShifts];
}

-(void)logoutTouched {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Confirm" message:@"Are you sure you want to logout?" preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:NULL]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    //    LoginTableViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"loginTableViewController"];
    //    [self.navigationController showViewController:vc sender:self];
        [UIApplication sharedApplication].keyWindow.rootViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"mainNavCont"];

     //   [self.navigationController popToRootViewControllerAnimated:YES];
    }]];
    
    alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
    [self presentViewController:alert animated:YES completion:NULL];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)loadUpcomingShifts {
 //   NSNumber *idnumber = [NSNumber numberWithInt:2];
    [NetworkingHelper seeUpcomingShiftsPCA:User.currentUser.pca.idNumber andSuccess:^(id responseObject) {
        NSMutableArray *mutArr = [NSMutableArray new];
        NSMutableArray *mutArrRSVP = [NSMutableArray new];
        for (NSDictionary *dictionaryRe in responseObject) {
            Request *req = [[Request alloc] initWithDictionary:dictionaryRe];
            [mutArrRSVP addObject:req];
        }
        self.arrayToPass = [mutArrRSVP copy];
        for (NSDictionary *dict in responseObject) {
            Shift *shift = [[Shift alloc] initWithDictionary:dict];
            NSDate *currentDate = [[NSDate alloc] init];
            NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:kDateFormat];
            NSDate *etaDate = [dateFormatter dateFromString:shift.etaString];
            //            NSString *output = [dateFormatter stringFromDate:currentDate];
            //            NSString *outputETA = [dateFormatter stringFromDate:etaDate];
            //            NSLog(@"eta date %@", etaDate);
            //            NSLog(@"output string current date %@", currentDate);
            
            NSComparisonResult result;
            //has three possible values: NSOrderedSame,NSOrderedDescending, NSOrderedAscending
            
            result = [etaDate compare:currentDate]; // comparing two dates
            
            if(result==NSOrderedAscending)
                NSLog(@"today is less");
            else if(result==NSOrderedDescending)
                NSLog(@"newDate is less");
            else if (result == NSOrderedSame) {
                NSLog(@"Both dates are same");
            }
            
            //    if (result == NSOrderedSame || result == NSOrderedAscending) {
            NSLog(@"in here 1");
         //   if ([shift.accepted isEqual:@1]) {
          //      NSLog(@"4");
                [mutArr addObject:shift];
                
                //    }
                
          //  }
            
            //   }
            //        for (NSDictionary *dict in responseObject[@"shift"]) {
            //            NSLog(@"in here 2");
            //            Request *request = [[Request alloc] initWithDictionary:dict];
            //            [mutArrRSVP addObject:request];
            //            NSLog(@"in here 3");
            //        }
            
        }
        
        
        
        
        
        //  _acceptedJobsArray = [mutArrRSVP mutableCopy];
        self->_upcomingShiftsArray = [mutArr mutableCopy];
        
        if (self->_upcomingShiftsArray.count > 0) {
            self->_recipientRequests = RecipientRequests_AtLeastOneRequest;
        }else if (self->_upcomingShiftsArray.count == 0){
            self->_recipientRequests = RecipientRequests_NoRequests;
        }
        self->loadingShifts = NO;
        [self.tableView reloadData];
        
    //    NSLog(@"accepted jobs %@", _upcomingShiftsArray);
        
        //    NSLog(@"upcoming jobs %@", _upcomingShiftsArray);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"upcoming shifts error %@", error);
        id responseObject = [NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:0 error:nil];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:[NSString stringWithFormat:@"%@", responseObject[@"error"]] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.tableView reloadData];
        }]];
        
        alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
        [self presentViewController:alert animated:YES completion:NULL];

    }];
    
}



-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
     if (section == 0 && _recipientRequests == RecipientRequests_AtLeastOneRequest){
        return _upcomingShiftsArray.count;
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 100;
    }
    return 100;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
        return @"Upcoming";
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UpcomingShiftsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UpcomingShiftsTableViewCell"];

        if (loadingShifts == YES) {
            cell.shiftTypeLabel.hidden = YES;
            cell.shiftStatusLabel.hidden = NO;
            cell.shiftTimeLabel.hidden = YES;
            cell.distanceLabel.hidden = YES;
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.shiftStatusLabel.text = @"Loading Shifts";
            cell.userInteractionEnabled = NO;
        }
       else if (_recipientRequests == RecipientRequests_NoRequests) {
            cell.shiftTypeLabel.hidden = YES;
            cell.shiftStatusLabel.hidden = NO;
            cell.shiftTimeLabel.hidden = YES;
           cell.accessoryType = UITableViewCellAccessoryNone;
            cell.distanceLabel.hidden = YES;
            cell.shiftStatusLabel.text = @"You have no shifts.";
            cell.userInteractionEnabled = NO;
            
            // return [self tableView:tableView loadingCellForRowAtIndexPath:indexPath andText:@"You have no impending shifts."];
        } else if (_recipientRequests == RecipientRequests_AtLeastOneRequest && indexPath.row >=0) {
            Shift *shift = _upcomingShiftsArray[indexPath.row];
            Request *req = _arrayToPass[indexPath.row];
            cell.tag = indexPath.row;
            cell.shiftStatusLabel.hidden = YES;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.userInteractionEnabled = YES;
            cell.shiftTypeLabel.hidden = NO;
            cell.shiftTimeLabel.hidden = NO;
            cell.distanceLabel.hidden = NO;
            cell.shiftTypeLabel.text = @"Personal Care Service";
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:kDateFormat];
            NSDate *date = [formatter dateFromString:shift.shiftStartTime];
            NSDate *date2 = [formatter dateFromString:shift.shiftEndTime];
            NSDate *date3 = [formatter dateFromString:shift.rsvp.eta];
            [formatter setDateFormat:kDisplayDateFormat];
            NSString *output = [formatter stringFromDate:date];
            NSString *output2 = [formatter stringFromDate:date2];
            NSString *output3 = [formatter stringFromDate:date3];

            [tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
            
            cell.shiftTimeLabel.text = [NSString stringWithFormat:@"%@ - %@", output, output2];
          //  cell.yourETALabel.text = [NSString stringWithFormat:@"My ETA:%@", output3];
            // cell.shiftTimeLabel.text = [NSString stringWithFormat:@"%@ - %@", shift.request.shiftStartTime, shift.request.shiftEndTime];
            CLLocation *currentLoc = self.locManager.location;
            
            CLGeocoder* geocoder = [[CLGeocoder alloc] init];
            [geocoder geocodeAddressString:req.recipientAddress
                         completionHandler:^(NSArray* placemarks, NSError* error){
                             for (CLPlacemark* aPlacemark in placemarks)
                             {
                                 double latString = aPlacemark.location.coordinate.latitude;
                                 double longString = aPlacemark.location.coordinate.longitude;
                                 CLLocation *recipLoc = [[CLLocation alloc] initWithLatitude:latString longitude:longString];
                                 if (currentLoc.coordinate.latitude != 0.0 && currentLoc.coordinate.longitude != 0.0) {
                                     CLLocationDistance meters = [recipLoc distanceFromLocation:currentLoc];
                                     NSLog(@"meters away %.2f", meters);
                                     if (meters == -1 || meters/1760 == -0) {
                                         cell.distanceLabel.text = @"?? miles away";
                                     }else{
                                         
                                         
                                         cell.distanceLabel.text = [NSString stringWithFormat:@"%.2f miles away", meters/1760];
                                     }
                                 } else {
                                     [geocoder reverseGeocodeLocation:recipLoc completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
                                         if (!error) {
                                             if (placemarks.count > 0) {
                                                 CLPlacemark *placeMark = placemarks[0];
                                                 cell.distanceLabel.text = [NSString stringWithFormat:@"%@, %@", [placeMark locality], [placeMark administrativeArea]];
                                             } else {
                                                 cell.distanceLabel.text = @"?? miles away";
                                             }
                                         } else {
                                             cell.distanceLabel.text = @"?? miles away";
                                         }
                                     }];
                                 }
                             }
                         }];
            
            cell.tag = indexPath.row;
            
            //  return cell;
            
        }
        return cell;
}

-(UITableViewCell *)tableView:(UITableView *)tableView loadingCellForRowAtIndexPath:(NSIndexPath *)indexPath andText:(NSString *)text {
    LoadingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"loadingCell"];
    if (cell.activityIndicator.isAnimating) {
        [cell.activityIndicator stopAnimating];
    }
    cell.statusLabel.text = text;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // if (indexPath.section == 1) {
    //  [self goToShiftDetail];
    //        if (_status == DataStatus_Error) {
    //            [self loadJobs];
    //        }
    //   }
}

-(UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller {
    return UIModalPresentationNone;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
     if ([[segue identifier] isEqualToString:@"showUpcomingShiftDetailsSegue"]){
        AcceptedShiftViewController *vc = [segue destinationViewController];
        UpcomingShiftsTableViewCell *cell = (UpcomingShiftsTableViewCell *)sender;
        Shift *shift = _upcomingShiftsArray[cell.tag];
        vc.requestIDNumber = shift.rsvpID;
        vc.acceptedJobArray = self.arrayToPass;
        vc.currentIndex = cell.tag;
     }else if ([segue.identifier isEqualToString:@"optionsPopoverSegue"]) {
         UserSettingsOptionsTableViewController *vc = [segue destinationViewController];
         vc.modalPresentationStyle = UIModalPresentationPopover;
         vc.popoverPresentationController.delegate = self;
         vc.delegate = self;
     }

    
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    [self.locManager stopUpdatingLocation];
    self.locManager = manager;
    if (locations.count > 0) {
     //   NSLog(@"self.locManager.location:%@", self.locManager.location);
    }
    
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
