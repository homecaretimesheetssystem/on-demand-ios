//
//  SeeRSVPsViewController.m
//  OnDemand Timesheets
//
//  Created by Joey Kjornes on 3/16/17.
//  Copyright © 2017 Jed Mahonis Group. All rights reserved.
//

#import "SeeRSVPsViewController.h"
#import "UpcomingShiftsViewController.h"
#import "EditWorkerProfileTableViewController.h"
#import "NetworkingHelper.h"
#import "UpcomingShiftsTableViewCell.h"
#import "AcceptedShiftViewController.h"
#import "LoadingTableViewCell.h"
#import "ViewProfileTableViewController.h"

@interface SeeRSVPsViewController ()

@end

@implementation SeeRSVPsViewController{
    
    BOOL loadingRecepientRequests;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _upcomingShiftsArray = [NSMutableArray new];
    [self loadUpcomingShifts];
    loadingRecepientRequests = YES;
    _upcomingStatus = UpcomingShiftStatus_Loading;
    _locManager = [[CLLocationManager alloc] init];
    _locManager.delegate = self;
    _locManager.distanceFilter = kCLDistanceFilterNone;
    _locManager.desiredAccuracy = kCLLocationAccuracyBest;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [self.locManager requestWhenInUseAuthorization];
    [_locManager startUpdatingLocation];
    self.title = @"Requests";
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshRSVPS:) name:@"push_type=2" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshRSVPS:) name:@"push_type=4" object:nil];
    

   // self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Edit Profile" style:UIBarButtonItemStylePlain target:self action:@selector(editProfileButtonPressed)];
    //    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:@selector(backButtonPressed:)];
    //    self.navigationItem.rightBarButtonItem = saveButton;
    
    
    
    // Do any additional setup after loading the view.
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)editProfileButtonPressed {
    EditWorkerProfileTableViewController *vc = [[UIStoryboard storyboardWithName:@"OnDemandWorker" bundle:nil] instantiateViewControllerWithIdentifier:@"EditWorkerProfileTableViewController"];
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)refreshRSVPS:(NSNotification *)notification{
    NSLog(@"refresh rsvps callled");
    [self loadUpcomingShifts];
}


-(void)loadUpcomingShifts {
//    NSNumber *idnumber = [NSNumber numberWithInt:22];
    [NetworkingHelper seeAllRequestsWithRecipID:User.currentUser.recipient.idNumber andSuccess:^(id responseObject) {
        NSMutableArray *mutArr = [NSMutableArray new];
        NSMutableArray *mutArrRSVP = [NSMutableArray new];
        for (NSDictionary *dict in responseObject) {
            Request *request = [[Request alloc] initWithDictionary:dict];
//            NSDate *currentDate = [[NSDate alloc] init];
//            NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
//            [dateFormatter setDateFormat:kDateFormat];
//            NSDate *etaDate = [dateFormatter dateFromString:request.rsvp.eta];
//            //            NSString *output = [dateFormatter stringFromDate:currentDate];
//            //            NSString *outputETA = [dateFormatter stringFromDate:etaDate];
//            //            NSLog(@"eta date %@", etaDate);
//            //            NSLog(@"output string current date %@", currentDate);
//            
//            NSComparisonResult result;
//            //has three possible values: NSOrderedSame,NSOrderedDescending, NSOrderedAscending
//            
//            result = [etaDate compare:currentDate]; // comparing two dates
//            
//            if(result==NSOrderedAscending)
//                NSLog(@"today is less");
//            else if(result==NSOrderedDescending)
//                NSLog(@"newDate is less");
//            else if (result == NSOrderedSame) {
//                NSLog(@"Both dates are same");
//            }
            [mutArr addObject:request];
            
            //    if (result == NSOrderedSame || result == NSOrderedAscending) {
//            NSLog(@"in here 1");
//            if ([shift.accepted isEqual:@1]) {
//                NSLog(@"4");
//                [mutArr addObject:shift];
//                
//                //    }
//                
//            }
            
            //   }
//                    for (NSDictionary *dict in responseObject[@"request"]) {
//                        NSLog(@"in here 2");
//                        Request *request = [[Request alloc] initWithDictionary:dict];
//                        [mutArrRSVP addObject:request];
//                        NSLog(@"in here 3");
//                    }
            
        }
        
        
        
        
        
        //  _acceptedJobsArray = [mutArrRSVP mutableCopy];
        self->_upcomingShiftsArray = [mutArr mutableCopy];
     //   NSLog(@"upcoming shifts array %@", _upcomingShiftsArray);
        
        if (self->_upcomingShiftsArray.count > 0) {
            self->_upcomingStatus = UpcomingShiftStatus_AtLeastOneShift;
        }else if (self->_upcomingShiftsArray.count == 0){
            self->_upcomingStatus = UpcomingShiftStatus_NoUpcomingShifts;
        }
        self->loadingRecepientRequests = NO;
        [self.tableView reloadData];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"upcoming shifts error %@", error);
        id responseObject = [NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:0 error:nil];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:[NSString stringWithFormat:@"%@", responseObject[@"error"]] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.tableView reloadData];
        }]];
        
        alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
        [self presentViewController:alert animated:YES completion:NULL];

    }];
    
}



-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0 && _upcomingStatus == UpcomingShiftStatus_AtLeastOneShift){
        return _upcomingShiftsArray.count;
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && _upcomingStatus == UpcomingShiftStatus_AtLeastOneShift) {
        Request *request = _upcomingShiftsArray[indexPath.row];
        return [request.rsvp.accepted isEqual:@1] ? 170 : 110;
    }else if (indexPath.section == 0 && _upcomingStatus == UpcomingShiftStatus_NoUpcomingShifts){
        return 200;
    }
    return 100;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"All Recent Requests";
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //   if (indexPath.section == 0){
    
    if (_upcomingStatus == UpcomingShiftStatus_Loading) {
        LoadingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"loadingCell"];
        if (cell.activityIndicator.isAnimating) {
            [cell.activityIndicator stopAnimating];
        }
        cell.statusLabel.text = @"Loading Requests";
        return cell;


    }
    
   else if (_upcomingStatus == UpcomingShiftStatus_NoUpcomingShifts) {
       
       LoadingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"loadingCell"];
       cell.statusLabel.text = @"You don't have any requests.";
       return cell;

    } else if (_upcomingStatus == UpcomingShiftStatus_AtLeastOneShift && indexPath.row >=0) {
        
        UpcomingShiftsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UpcomingShiftsTableViewCell"];
        Request *request = _upcomingShiftsArray[indexPath.row];
        cell.tag = indexPath.row;
        cell.viewProfileButton.tag = indexPath.row;
        cell.cancelButton.tag = indexPath.row;
        cell.shiftStatusLabel.hidden = YES;
        cell.shiftTypeLabel.hidden = NO;
        cell.shiftTimeLabel.hidden = NO;
        cell.noRecentRequestsLabel.hidden = YES;
        cell.cancelButton.hidden = NO;
        cell.startAnotherShiftButton.hidden = YES;
        cell.distanceLabel.hidden = NO;
        cell.shiftTypeLabel.text = @"PCA";
        cell.viewProfileButton.layer.borderColor = [UIColor colorWithRed:55.0/255.0 green:99.0/255.0 blue:135.0/255.0 alpha:1.0].CGColor;
        cell.viewProfileButton.layer.borderWidth = 1.0f; //make border 1px thick

        
   //     cell.backgroundColor = [self shiftIsAcceptedOrNot:request] ? [UIColor redColor] : [UIColor greenColor];
        cell.colorStatusImageView.backgroundColor = ([indexPath row]%2)?[UIColor clearColor]:[UIColor clearColor];

        
        if ([request.rsvp.accepted isEqual:@1]) {
            cell.colorStatusImageView.backgroundColor = ([indexPath row]%2)?[UIColor greenColor]:[UIColor greenColor];
        }if ([request.rsvp.declined isEqual:@1]){
            cell.colorStatusImageView.backgroundColor = ([indexPath row]%2)?[UIColor redColor]:[UIColor redColor];

        }
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:kDateFormat];
        NSDate *date = [formatter dateFromString:request.shiftStartTime];
        NSDate *date2 = [formatter dateFromString:request.shiftEndTime];
//        NSDate *etaDate = [formatter dateFromString:request.rsvp.eta];
        [formatter setDateFormat:kDisplayDateFormat];
        NSString *output = [formatter stringFromDate:date];
        NSString *output2 = [formatter stringFromDate:date2];
//        NSString *etaOutputString = [formatter stringFromDate:etaDate];
        [tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
        cell.cancelButton.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:@"Cancel" attributes:underlineAttribute];
        [cell.viewProfileButton addTarget:self action:@selector(viewProfilePressed:) forControlEvents:UIControlEventTouchUpInside];

        
        NSString *nameString = [NSString stringWithFormat:@"%@ %@", request.rsvp.pca.firstName, request.rsvp.pca.lastName];
        if ([request.rsvp.accepted isEqual:@1]) {
            cell.viewProfileButton.hidden = NO;
            cell.distanceLabel.text = nameString;
        }else if ([request.rsvp.declined isEqual:@1]){
            cell.distanceLabel.text = @"Shift Declined";
            cell.viewProfileButton.hidden = YES;
        }else{
            cell.distanceLabel.text = @"Not Accepted Yet";
            cell.viewProfileButton.hidden = YES;
        }

        cell.shiftTimeLabel.text = [NSString stringWithFormat:@"%@ - %@", output, output2];
        
        // cell.shiftTimeLabel.text = [NSString stringWithFormat:@"%@ - %@", shift.request.shiftStartTime, shift.request.shiftEndTime];
        CLLocation *currentLoc = self.locManager.location;
        CLGeocoder* geocoder = [[CLGeocoder alloc] init];
        [geocoder geocodeAddressString:request.recipientAddress
                     completionHandler:^(NSArray* placemarks, NSError* error){
                         for (CLPlacemark* aPlacemark in placemarks)
                         {
                             double latString = aPlacemark.location.coordinate.latitude;
                             double longString = aPlacemark.location.coordinate.longitude;
                             CLLocation *restaurantLoc = [[CLLocation alloc] initWithLatitude:latString longitude:longString];
                             CLLocationDistance meters = [restaurantLoc distanceFromLocation:currentLoc];
                            // cell.distanceLabel.text = [NSString stringWithFormat:@"%.2f miles", meters/1760];
                         }
                         
                     }];
        
        cell.tag = indexPath.row;
        
        
        return cell;
        
    }
    
    return nil;
}



-(UITableViewCell *)tableView:(UITableView *)tableView loadingCellForRowAtIndexPath:(NSIndexPath *)indexPath andText:(NSString *)text {
    LoadingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"loadingCell"];
    if (cell.activityIndicator.isAnimating) {
        [cell.activityIndicator stopAnimating];
    }
    cell.statusLabel.text = text;
    return cell;
}


-(IBAction)cancelShiftButtonPressed:(id)sender {
    UpcomingShiftsTableViewCell *cell = (UpcomingShiftsTableViewCell *)sender;
    Request *request = _upcomingShiftsArray[cell.tag];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Confirm" message:@"Do you want to cancel this request?" preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [NetworkingHelper deleteRequestWithRequestID:request.requestID andSuccess:^(id responseObject) {
            UIAlertController *alert2 = [UIAlertController alertControllerWithTitle:@"Success" message:@"Your request has been canceled." preferredStyle:UIAlertControllerStyleAlert];
            [alert2 addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.navigationController popViewControllerAnimated:YES];
            }]];
            
            alert2.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
            [self presentViewController:alert2 animated:YES completion:NULL];
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            id responseObject = [NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:0 error:nil];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:[NSString stringWithFormat:@"%@", responseObject[@"error"]] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.tableView reloadData];
            }]];
            
            alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
            [self presentViewController:alert animated:YES completion:NULL];

            
        }];
        
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
    [self presentViewController:alert animated:YES completion:NULL];
    

    
    
}

-(void)viewProfilePressed:(id)sender {
    UIButton *bttn = (UIButton *)sender;
    [self performSegueWithIdentifier:@"seePCAProfileSegue" sender:bttn];
    
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

}


-(IBAction)dismiss:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    //        [self.navigationController pushViewController:vc animated:YES];
    if ([[segue identifier] isEqualToString:@"showUpcomingShiftDetailsSegue"]){
        AcceptedShiftViewController *vc = [segue destinationViewController];
        UpcomingShiftsTableViewCell *cell = (UpcomingShiftsTableViewCell *)sender;
      //  NSLog(@"upcming shifts array %@", _upcomingShiftsArray);
        NSMutableArray *mutArray = [NSMutableArray new];
        Shift *shift = _upcomingShiftsArray[cell.tag];
        vc.RSVPIDNumber = shift.rsvpID;
        [mutArray addObject:shift.request];
        vc.acceptedJobArray = mutArray;
        vc.currentIndex = cell.tag;
    }
    
    if ([segue.identifier isEqualToString:@"seePCAProfileSegue"]) {
        ViewProfileTableViewController *vc = [segue destinationViewController];
        UIButton *bttn = (UIButton *)sender;
        Request *request = _upcomingShiftsArray[bttn.tag];
        vc.pca = request.rsvp.pca;
    }
    
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    [self.locManager stopUpdatingLocation];
    self.locManager = manager;
    if (locations.count > 0) {
 //       NSLog(@"self.locManager.location:%@", self.locManager.location);
    }
    
    
}


@end
