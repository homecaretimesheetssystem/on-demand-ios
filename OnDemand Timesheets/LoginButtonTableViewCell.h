//
//  LoginButtonTableViewCell.h
//  OnDemand Timesheets
//
//  Created by Joey Kjornes on 2/28/17.
//  Copyright © 2017 Jed Mahonis Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginButtonTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIButton *loginButton;


@end
