//
//  CountiesTableViewController.m
//  OnDemand Timesheets
//
//  Created by Joey Kjornes on 4/17/17.
//  Copyright © 2017 Jed Mahonis Group. All rights reserved.
//

#import "CountiesTableViewController.h"
#import "Counties.h"
#import "LoadingView.h"
#import "NetworkingHelper.h"
#import "CountiesTableViewCell.h"

@interface CountiesTableViewController ()

@end

@implementation CountiesTableViewController  {
    
    NSMutableArray *countiesArrayToChooseFrom;
    BOOL loadingTheCounties;
    BOOL countiesArrayChanged;
    BOOL isSearching;
    NSMutableArray *searchResultsArray;
    BOOL returnCurrentArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    loadingTheCounties = YES;
    countiesArrayChanged = NO;
    isSearching = NO;
    countiesArrayToChooseFrom = [NSMutableArray new];
    [self loadCounties];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(back:)];
    self.navigationItem.leftBarButtonItem=newBackButton;

    
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(sendCountiesBackToEditWorker)];
    
    self.navigationItem.rightBarButtonItem = saveButton;
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
    
}


-(void)back:(UIBarButtonItem *)sender
{
    
    
    if (countiesArrayChanged == YES) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Changes Made" message:@"Do you want to save changes to your profile?" preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            self->countiesArrayChanged = NO;
            [self sendCountiesBackToEditWorker];
            
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            self->countiesArrayChanged = NO;
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
            //  [self.navigationController popToRootViewControllerAnimated:YES];
            
        }]];
        [self presentViewController:alert animated:YES completion:NULL];
    }else{
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        
    }
    
}



-(void)loadCounties {
    LoadingView *loadingView = [LoadingView addLoadingViewWithText:@"Loading"];
    [NetworkingHelper getCountiesWithSuccess:^(id responseObject) {
        NSMutableArray *mutArr = [NSMutableArray new];
        for (NSDictionary *dict in responseObject) {
            Counties *counties = [[Counties alloc] initWithDictionary:dict];
            [mutArr addObject:counties];
        }
        [self->countiesArrayToChooseFrom addObjectsFromArray:mutArr];
        self->loadingTheCounties = NO;
        [loadingView hideActivityIndicator];
        [self.tableView reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"failure in loadcounties %@", error);
        id responseObject = [NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:0 error:nil];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:[NSString stringWithFormat:@"%@", responseObject[@"error"]] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.tableView reloadData];
        }]];
        
        alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
        [self presentViewController:alert animated:YES completion:NULL];

    }];
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (loadingTheCounties == YES) {
        return 0;
    }else if (![_searchBar.text isEqualToString:@""]){
        return searchResultsArray.count;
    }
    return countiesArrayToChooseFrom.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= 0) {
        CountiesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CountiesTableViewCell"];
        if ([_searchBar.text isEqualToString:@""]){
        
        
            Counties *counties = countiesArrayToChooseFrom[indexPath.row];
            cell.accessoryType = [self countyIsSelected:counties] ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
            cell.countyLabel.text = counties.countyName;
            return cell;
      }else if (![_searchBar.text isEqualToString:@""]){
           Counties *counties = searchResultsArray[indexPath.row];
           // cell.accessoryType = [self countyFromProfile:counties] ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
           cell.accessoryType = [self countyIsSelected:counties] ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
           cell.countyLabel.text = counties.countyName;
          return cell;
       }
    }
    return nil;
}

-(BOOL)countyFromProfile:(Counties *)county {
    BOOL found = NO;
    for (int i = 0; i < _workersActualCountiesArray.count; i++) {
        Counties *counties = _workersActualCountiesArray[i];
        if ([counties.countyID isEqualToNumber:county.countyID]) {
            found = YES;
            break;
        }
    }
    return found;
}


-(BOOL)countyIsSelected:(Counties *)county {
    BOOL found = NO;
    for (int i = 0; i < _workersActualCountiesArray.count; i++) {
        Counties *counties = _workersActualCountiesArray[i];
        if ([counties.countyID isEqualToNumber:county.countyID]) {
            found = YES;
            break;
        }
    }
    return found;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    countiesArrayChanged = YES;
    [self selectOrDeselectCountiesWithIndexPath:indexPath andTableView:tableView];

    
}


-(void)selectOrDeselectCountiesWithIndexPath:(NSIndexPath *)indexPath andTableView:(UITableView *)tableView {
    Counties *countyToRemove = nil;
    BOOL shouldRemoveFromArray = NO;
    Counties *c;

    if ([_searchBar.text isEqualToString:@""]){
    c = countiesArrayToChooseFrom[indexPath.row];
    for (Counties *counties in _workersActualCountiesArray) {
        if ([counties.countyID isEqualToNumber:c.countyID]) {
            countyToRemove = counties;
            shouldRemoveFromArray = YES;
            break;
        }
    }
    }else if (![_searchBar.text isEqualToString:@""]){
        c = searchResultsArray[indexPath.row];
        for (Counties *counties in _workersActualCountiesArray) {
            if ([counties.countyID isEqualToNumber:c.countyID]) {
                countyToRemove = counties;
                shouldRemoveFromArray = YES;
                break;
            }
        }
    }
    
    if (countyToRemove) {
        [_workersActualCountiesArray removeObject:countyToRemove];
    } else {
        [_workersActualCountiesArray addObject:c];
    }
    [self.tableView reloadData];
}


-(void)sendCountiesBackToEditWorker {
    if (_workersActualCountiesArray.count >0) {
    [self.delegate finishedSelectingCountiesWithArray:_workersActualCountiesArray];
    [self.navigationController popViewControllerAnimated:YES];
    }else if (_workersActualCountiesArray.count == 0){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"You need to choose at least one county." preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }]];
        [self presentViewController:alert animated:YES completion:NULL];
    
    }
}


#pragma mark Search Bar


-(IBAction)searchBarButtonItemTouched:(id)sender {
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self action:@selector(closeSearchBarButtonTouched:)];
    self.navigationItem.titleView = _searchBar;
    [_searchBar becomeFirstResponder];
    [self.tableView reloadData];
    
}

-(void)closeSearchBarButtonTouched:(id)sender {
    [_searchBar resignFirstResponder];
    self.navigationItem.titleView = nil;
   // self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(searchBarButtonItemTouched:)];
    isSearching = NO;
}



-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    
    if (searchBar.text.length > 0) {
        isSearching = YES;
    } else {
        [self closeSearchBarButtonTouched:self];
        isSearching = NO;
    }
    return YES;
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    isSearching = NO;
    [_searchBar resignFirstResponder];
    [self.tableView reloadData];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self filterContentForSearchText:searchText scope:[searchBar scopeButtonTitles][[searchBar selectedScopeButtonIndex]]];
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    if (![searchText isEqualToString:@""]) {
        isSearching = YES;
        searchResultsArray = [NSMutableArray new];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"countyName contains[c] %@",searchText];
        searchResultsArray = [NSMutableArray arrayWithArray:[countiesArrayToChooseFrom filteredArrayUsingPredicate:predicate]];
        returnCurrentArray = YES;
    } else {
        isSearching = NO;
        searchResultsArray = countiesArrayToChooseFrom;
        [self closeSearchBarButtonTouched:self];
    }
    [self.tableView reloadData];
    
}
//
//#pragma mark - Navigation
////sendSelectedCountiesToEditSegue
//// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    if ([segue.identifier isEqualToString:@"sendSelectedCountiesToEditSegue"]) {
//        EditWorkerProfileTableViewController *vc = [segue destinationViewController];
//        NSLog(@"workers county array passed %@", _workersActualCountiesArray);
//        vc.countiesArrayPassedToEditWorker = [NSMutableArray new];
//        vc.countiesArrayPassedToEditWorker = _workersActualCountiesArray;
//        
//    }
//    
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//}

@end
