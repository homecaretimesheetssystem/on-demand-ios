//
//  LoadingView.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 3/26/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingView : UIView

@property UILabel *label;
@property UIActivityIndicatorView *activityIndicator;

-(instancetype)initWithFrame:(CGRect)frame andText:(NSString *)text;

+(LoadingView *)addLoadingViewWithText:(NSString *)text;

- (void) showActivityIndicator;

- (void) hideActivityIndicator;

@end
