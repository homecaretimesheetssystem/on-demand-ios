//
//  Request.h
//  Homecare Timesheet Demo
//
//  Created by Joey Kjornes on 2/14/17.
//  Copyright © 2017 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RSVP.h"
#import "Recipient.h"

@interface Request : NSObject <NSCoding>

@property (nonatomic, strong) NSString *recipientAddress;
@property (nonatomic, strong) NSString *shiftStartTime;
@property (nonatomic, strong) NSString *shiftEndTime;
@property (nonatomic,strong) NSNumber *requestID;
@property (nonatomic, strong) NSNumber *genderPreference;
@property (nonatomic, strong) NSArray *rsvpArray;
@property (nonatomic, strong) NSArray *availableArray;
@property (nonatomic, strong) NSArray *takenArray;
@property (nonatomic, strong) Recipient *recipient;
@property (nonatomic, strong) NSArray *requestsArray;
@property (nonatomic, strong) RSVP *rsvp;
@property (nonatomic, strong) NSString *additionalRequirements;


-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
