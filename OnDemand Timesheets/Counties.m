//
//  Counties.m
//  OnDemand Timesheets
//
//  Created by Joey Kjornes on 3/17/17.
//  Copyright © 2017 Jed Mahonis Group. All rights reserved.
//

#import "Counties.h"

@implementation Counties

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
 //   NSLog(@"dictionary in counties %@", dictionary);
    if (self == [super init]) {
        if ([dictionary objectForKey:@"id"] != [NSNull null]) {
            _countyID = dictionary[@"id"];
        }else{
            _countyID = [NSNumber numberWithLong:0];
        }
        
        if ([dictionary objectForKey:@"name"] != [NSNull null]) {
            _countyName = dictionary[@"name"];
        }else{
            _countyName = @"";
        }
    }
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self == [super init]) {
        
        _countyName = [aDecoder decodeObjectForKey:@"name"];
        _countyID = [aDecoder decodeObjectForKey:@"id"];
        
        
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:_countyName forKey:@"name"];
    [aCoder encodeObject:_countyID forKey:@"id"];
    
}

-(NSString *)description {
    return [NSString stringWithFormat:@"County ID:%@, County Name:%@", _countyID, _countyName];
}

@end
