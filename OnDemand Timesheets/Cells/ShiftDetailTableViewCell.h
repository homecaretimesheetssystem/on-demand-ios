//
//  ShiftDetailTableViewCell.h
//  Homecare Timesheet Demo
//
//  Created by Joey Kjornes on 2/8/17.
//  Copyright © 2017 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShiftDetailTableViewCell : UITableViewCell <UITextFieldDelegate, UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *availableButton;
@property (weak, nonatomic) IBOutlet UIButton *acceptedButton;
@property (weak, nonatomic) IBOutlet UILabel *shiftTimesLabel;
@property (weak, nonatomic) IBOutlet UILabel *jobTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UITextView *qualificationsTextView;
@property (weak, nonatomic)IBOutlet UILabel *addressLabel;
@property (nonatomic, weak) IBOutlet UITextField *addressTextField;
@property (nonatomic, weak) IBOutlet UITextField *yearsOfExperienceTextField;
@property (nonatomic, weak) IBOutlet UIButton *autocompleteAddressButton;
@property (weak, nonatomic) IBOutlet UITextView *additionalRequirementsTextView;
@property (weak, nonatomic) IBOutlet UITextView *additionalRequirementsPlaceholderTextView;
@end
