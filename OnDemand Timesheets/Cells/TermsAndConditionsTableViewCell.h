//
//  TermsAndConditionsTableViewCell.h
//  Homecare Timesheet Demo
//
//  Created by Joey Kjornes on 2/15/17.
//  Copyright © 2017 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermsAndConditionsTableViewCell : UITableViewCell

@end
