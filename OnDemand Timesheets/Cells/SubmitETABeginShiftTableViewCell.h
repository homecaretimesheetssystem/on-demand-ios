//
//  SubmitETABeginShiftTableViewCell.h
//  Homecare Timesheet Demo
//
//  Created by Joey Kjornes on 2/8/17.
//  Copyright © 2017 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AcceptedShiftViewController.h"





@interface SubmitETABeginShiftTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *submitETAButton;
@property (weak, nonatomic) IBOutlet UIButton *chooseETAButton;
@property (weak, nonatomic) IBOutlet UIDatePicker *submitETADatePicker;



@end
