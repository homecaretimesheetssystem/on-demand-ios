//
//  CancelTableViewCell.h
//  Homecare Timesheet Demo
//
//  Created by Joey Kjornes on 2/16/17.
//  Copyright © 2017 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CancelTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *confirmCancelButton;
@property (strong, nonatomic) IBOutlet UITextField *cancelReasonTextfield;


@end
