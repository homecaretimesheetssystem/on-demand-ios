//
//  StartTimeCellTableViewCell.h
//  Homecare Timesheet Demo
//
//  Created by Joey Kjornes on 2/9/17.
//  Copyright © 2017 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StartTimeCellTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *startTimeLabel;
@property (weak, nonatomic) IBOutlet UIDatePicker *startTimePickerView;
@property (weak, nonatomic) IBOutlet UIImageView *downArrowImage;

@end
