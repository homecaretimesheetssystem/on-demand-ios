//
//  ShiftDetailTableViewCell.m
//  Homecare Timesheet Demo
//
//  Created by Joey Kjornes on 2/8/17.
//  Copyright © 2017 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "ShiftDetailTableViewCell.h"

@implementation ShiftDetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//#pragma mark - UITextViewDelegate
//
//-(void)textViewDidBeginEditing:(UITextView *)textView {
//    self.additionalRequirementsPlaceholderTextView.hidden = _additionalRequirementsTextView.text.length > 0;
//    [NSNotificationCenter.defaultCenter postNotificationName:@"updateAdditionalRequirementsString" object:nil userInfo:@{@"additionalRequirements":textView.text}];
//}
//
//-(void)textViewDidChange:(UITextView *)textView {
//    self.additionalRequirementsPlaceholderTextView.hidden = _additionalRequirementsTextView.text.length > 0;
//    [NSNotificationCenter.defaultCenter postNotificationName:@"updateAdditionalRequirementsString" object:nil userInfo:@{@"additionalRequirements":textView.text, @"isEditing":@YES}];
//}
//
//-(void)textViewDidEndEditing:(UITextView *)textView {
//    self.additionalRequirementsPlaceholderTextView.hidden = _additionalRequirementsTextView.text.length > 0;
//    [NSNotificationCenter.defaultCenter postNotificationName:@"updateAdditionalRequirementsString" object:nil userInfo:@{@"additionalRequirements":textView.text}];
//}



@end
