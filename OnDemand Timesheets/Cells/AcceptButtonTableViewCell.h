//
//  AcceptButtonTableViewCell.h
//  Homecare Timesheet Demo
//
//  Created by Joey Kjornes on 2/8/17.
//  Copyright © 2017 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AcceptButtonTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *acceptShiftButton;
@property (weak, nonatomic) IBOutlet UILabel *termsAndConditionsLabel;

@end
