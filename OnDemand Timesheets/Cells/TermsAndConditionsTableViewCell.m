//
//  TermsAndConditionsTableViewCell.m
//  Homecare Timesheet Demo
//
//  Created by Joey Kjornes on 2/15/17.
//  Copyright © 2017 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "TermsAndConditionsTableViewCell.h"

@implementation TermsAndConditionsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
