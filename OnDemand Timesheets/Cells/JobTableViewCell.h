//
//  JobTableViewCell.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 10/24/16.
//  Copyright © 2016 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JobTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *shiftDistanceLabel;
@property (nonatomic, weak) IBOutlet UILabel *startTimeLabel;
@property (nonatomic, weak) IBOutlet UILabel *shiftTypeLabel;
@property (nonatomic, weak) IBOutlet UITextView *jobTitleDistancetextView;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *qualificationsNeededtextViewHeight;


@end
