//
//  BeginShiftTableViewCell.h
//  Homecare Timesheet Demo
//
//  Created by Joey Kjornes on 2/16/17.
//  Copyright © 2017 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BeginShiftTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *beginShiftButton;


@end
