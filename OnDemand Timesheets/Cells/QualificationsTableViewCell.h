//
//  QualificationsTableViewCell.h
//  Homecare Timesheet Demo
//
//  Created by Joey Kjornes on 2/9/17.
//  Copyright © 2017 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QualificationsTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;

@end
