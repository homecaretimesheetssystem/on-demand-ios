//
//  YearsOfExperienceTableViewCell.h
//  Homecare Timesheet Demo
//
//  Created by Joey Kjornes on 2/21/17.
//  Copyright © 2017 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YearsOfExperienceTableViewCell : UITableViewCell <UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic, weak) IBOutlet UITextField *yearsOfExperiecneTextField;

@end
