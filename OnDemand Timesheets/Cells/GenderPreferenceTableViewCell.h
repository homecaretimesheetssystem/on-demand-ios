//
//  GenderPreferenceTableViewCell.h
//  Homecare Timesheet Demo
//
//  Created by Joey Kjornes on 2/13/17.
//  Copyright © 2017 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GenderPreferenceTableViewCell : UITableViewCell <UIPickerViewDelegate, UIPickerViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *genderPreferenceLabel;
@property (weak, nonatomic) IBOutlet UIPickerView *genderPicker;
@property (nonatomic, strong) NSArray *genderArray;
@property (nonatomic) BOOL pickerChanged;
@property (nonatomic, weak) IBOutlet UIImageView *downArrowImage;

@end
