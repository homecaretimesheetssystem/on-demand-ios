//
//  ProfilePictureTableViewCell.h
//  Homecare Timesheet Demo
//
//  Created by Joey Kjornes on 2/10/17.
//  Copyright © 2017 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfilePictureTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *profilePicture;
@property (nonatomic, weak) IBOutlet UIButton *tapToChangePictureButton;

@end
