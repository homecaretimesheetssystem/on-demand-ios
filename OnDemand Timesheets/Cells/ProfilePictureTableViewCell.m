//
//  ProfilePictureTableViewCell.m
//  Homecare Timesheet Demo
//
//  Created by Joey Kjornes on 2/10/17.
//  Copyright © 2017 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "ProfilePictureTableViewCell.h"

@implementation ProfilePictureTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
