//
//  ShiftConfirmationTableViewCell.h
//  Homecare Timesheet Demo
//
//  Created by Joey Kjornes on 2/8/17.
//  Copyright © 2017 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShiftConfirmationTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *shiftTimesLabel;
@property (weak, nonatomic) IBOutlet UILabel *jobTitleLabel;

@end
