//
//  GenderPreferenceTableViewCell.m
//  Homecare Timesheet Demo
//
//  Created by Joey Kjornes on 2/13/17.
//  Copyright © 2017 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "GenderPreferenceTableViewCell.h"

@implementation GenderPreferenceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _pickerChanged = NO;
  //  _genderPicker = [UIPickerView new];//you need to set frame or other properties and add to your view...you can either use XIB or can code...
    _genderPicker.delegate = self; // Also, can be done from IB, if you're using
    _genderPicker.dataSource = self;// Also, can be done from IB, if you're using

    _genderArray = [NSArray arrayWithObjects:@"No Preference", @"Female", @"Male", nil];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    
    return 1;//Or return whatever as you intend
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    
    return _genderArray.count;
}


- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    _pickerChanged = YES;
    return [NSString stringWithFormat:@"%@",_genderArray[row]];//Or, your suitable title; like Choice-a, etc.
}


- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    self.genderPreferenceLabel.text = _genderArray[row];
    
}
@end
