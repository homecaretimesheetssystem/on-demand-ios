//
//  JMGCameraViewController.m
//  camera
//
//  Created by Scott Mahonis on 7/23/16.
//  Copyright © 2016 JMG. All rights reserved.
//

#import "JMGCameraViewController.h"
#import "CircleFilterView.h"
#import "AppDelegate.h"

@interface JMGCameraViewController () {
    BOOL isExiting; UIView *whiteCircleContainerViewForShutterButton; UIButton *circleButton; UIButton *cancelButton; UIButton *pickImageFromPhotoLibraryButton; UIView *cameraOverlayBottomView; BOOL photoLibraryStatusBarAppearance; UIButton *flipCameraButton; UILabel *titleAboveCameraLabel; BOOL captureMode; UIView *clearViewWithWhiteCircle; CircleFilterView *circleFilterView;
}

@end

@implementation JMGCameraViewController

@synthesize  imagePicker, picture, cameraView;
#pragma mark - View Delegate
- (void)viewDidLoad {
    [super viewDidLoad];
    // Check if a user's launched this app before.
    isExiting = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setFramesForStatusBarOrientation) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    photoLibraryStatusBarAppearance = NO;
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    self.cameraBackgroundColor = [UIColor grayColor];
    
    if (self.cameraBackgroundColor == [UIColor whiteColor]) {
        self.uploadBarButton.tintColor = [UIColor darkGrayColor];
        self.retakePictureBarButton.tintColor = [UIColor darkGrayColor];
    }
    
    height = self.view.bounds.size.height;
    width = self.view.bounds.size.width;
    
    self.view.backgroundColor = _cameraBackgroundColor;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    [self.tabBarController.tabBar setHidden:YES];
    
    AppDelegate *del = (AppDelegate *)[UIApplication sharedApplication].delegate;
    del.shouldRotate = YES;
    
    
    
    [self openCamera:self];
    [self addCameraOverlayBottomView];
    
    [self setFramesForStatusBarOrientation];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

-(void)setFileSize {
        if (_cameraMode == CameraMode_Custom) {
            _fileSize = CGSizeMake(512, 512);
        } else if (_cameraMode == CameraMode_default) {
            if ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight) {
                _fileSize = CGSizeMake(1920., 1080.);
            } else {
                _fileSize = CGSizeMake(1080., 1920.);
            }
            
        } else {
            if ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight) {
                _fileSize = CGSizeMake(2000., ((2000. * self.view.bounds.size.height) / self.view.bounds.size.width));
            } else {
                _fileSize = CGSizeMake(((2000. * self.view.bounds.size.width) / self.view.bounds.size.height), 2000.0);
            }
        }
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

-(AVCaptureVideoOrientation )captureVideoOrientation {
    UIInterfaceOrientation statusBarOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (statusBarOrientation == UIInterfaceOrientationPortrait) {
        return AVCaptureVideoOrientationPortrait;
    } else if (statusBarOrientation == UIInterfaceOrientationLandscapeLeft) {
        return AVCaptureVideoOrientationLandscapeLeft;
    } else if (statusBarOrientation == UIInterfaceOrientationLandscapeRight) {
        return AVCaptureVideoOrientationLandscapeRight;
    } else {
        return AVCaptureVideoOrientationPortraitUpsideDown;
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    if (isExiting == YES) {
        return _parentStatusBarStyle;
    } else {
        if (self.cameraBackgroundColor == [UIColor grayColor]) {
            return UIStatusBarStyleDefault;
        }
        if (photoLibraryStatusBarAppearance == YES) {
            return UIStatusBarStyleDefault;
        } else {
            return UIStatusBarStyleLightContent;
        }
    }
    return _parentStatusBarStyle;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    //    NSLog(@"didReceiveMemoryWarning");
}



-(void)back {
    isExiting = YES;
    [UIApplication sharedApplication].statusBarStyle = _parentStatusBarStyle;
    [self setNeedsStatusBarAppearanceUpdate];
    // If the app supports rotation comment this out
    AppDelegate *del = (AppDelegate *)[UIApplication sharedApplication].delegate;
    del.shouldRotate = NO;
    //    self.navigationController.navigationBar.barStyle = _parentNavBarStyle;
    if (self.navigationController) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self dismissViewControllerAnimated:YES completion:NULL];
    }
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.tabBarController.tabBar setHidden:NO];
    
}

#pragma mark - Create Views and Frames

-(CGRect)frameForFileSize {
    if (_cameraMode == CameraMode_fullScreen) {
        return self.view.frame;
    }
    if (_fileSize.width != 0 && _fileSize.height != 0) {
        CGFloat heightValue = width < height ? (width * _fileSize.height)/_fileSize.width : (height * _fileSize.width)/_fileSize.height;
        CGFloat widthValue = width < height ? ((height) * _fileSize.width)/_fileSize.height : (width * _fileSize.height)/_fileSize.width;
        if (_fileSize.width >= _fileSize.height) {
            return width < height ? CGRectMake(0, self.view.center.y - heightValue/2, width, heightValue) : CGRectMake(self.view.center.x - heightValue/2, 0, heightValue, height);
        } else {
            return CGRectMake(self.view.center.x - widthValue/2, 64, widthValue, height);
        }
        
    } else {
        return CGRectMake(0, 64, width, height - 64);
    }
}

-(IBAction)openCamera:(id)sender {
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] && [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusAuthorized) {
        captureMode = YES;
        [self.view endEditing:YES];
        if (moveAndScaleLabel) {
            [moveAndScaleLabel removeFromSuperview];
        }
        [self animateButtonsForImageCapture];
        [editingCropScrollview removeFromSuperview];
        [clearViewWithWhiteCircle removeFromSuperview];
        [circleFilterView removeFromSuperview];
        for (UIView *view in self.view.subviews) {
            if ([view isKindOfClass:[UIImageView class]]) {
                [view removeFromSuperview];
            }
        }
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
        
        [self.view endEditing:YES];
        self.navigationController.navigationBarHidden = YES;
        if (!_cameraOutput) {
            if ([[[UIDevice currentDevice] systemVersion] floatValue] < 10.0) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
                
                _cameraOutput = [[AVCaptureStillImageOutput alloc] init];
                
#pragma clang diagnostic pop
                
                NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys:
                                                AVVideoCodecJPEG, AVVideoCodecKey,
                                                nil];
                
                [_cameraOutput setOutputSettings:outputSettings];
            } else {
                _cameraOutput = [[AVCapturePhotoOutput alloc] init];
            }
            
        }
        if (!_session) {
            _session = [[AVCaptureSession alloc] init];
            if ([_session canAddOutput:_cameraOutput]) {
                [_session addOutput:_cameraOutput];
            }
            [_session startRunning];
        }
        if (!_device) {
            _device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        }
        NSError *error = nil;
        
        if (!_input) {
            _input = [AVCaptureDeviceInput deviceInputWithDevice:_device error:&error];
            if (_input) {
                [_session addInput:_input];
            } else {
                NSLog(@"Error: %@", error);
            }
        }
        
        if (!_prevLayer) {
            _prevLayer = [AVCaptureVideoPreviewLayer layerWithSession:_session];
            _prevLayer.frame = [self frameForFileSize];
            _prevLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
            [self setFramesForStatusBarOrientation];
            [self.view.layer addSublayer:_prevLayer];
        }
        self.view.layer.backgroundColor = self.cameraBackgroundColor.CGColor;
        
        
        _capturePhotoSettings = [AVCapturePhotoSettings photoSettings];
        
        cameraView = [[UIView alloc] init];
        cameraView.frame = [[UIScreen mainScreen] bounds];
        cameraView.hidden = NO;
        
        flashButton = [UIButton buttonWithType:UIButtonTypeCustom];
        flashButton.isAccessibilityElement = YES;
        flashButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        flashButton.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
        flashFrame = [self frameForFileSize].origin.y > 40. ? CGRectMake(8, [self frameForFileSize].origin.y - 30, 100, 50) : CGRectMake(8, 20, 100, 50);
        //        NSLog(@"flashFrame:%@", NSStringFromCGRect(flashFrame));
        //NSLog(@"imageP.cameraFlashM:%d", imagePicker.cameraFlashMode);
        
        flashButton.frame = flashFrame;
        [flashButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        if (self.cameraBackgroundColor == [UIColor whiteColor]) {
            [flashButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        }
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"flashMode"] isEqualToNumber:@(FlashMode_auto)]) {
            if ([self flashModeIsSupportedForDevice:AVCaptureFlashModeAuto]) {
                [self toggleAVFlashAndTorchWithFlashMode:AVCaptureFlashModeAuto];
            }
        }
        else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"flashMode"] isEqualToNumber:@(FlashMode_off)]) {
            if ([self flashModeIsSupportedForDevice:AVCaptureFlashModeOff]) {
                [self toggleAVFlashAndTorchWithFlashMode:AVCaptureFlashModeOff];
            }
        }
        else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"flashMode"] isEqualToNumber:@(FlashMode_on)]) {
            if ([self flashModeIsSupportedForDevice:AVCaptureFlashModeOn]) {
                [self toggleAVFlashAndTorchWithFlashMode:AVCaptureFlashModeOn];
            }
        } else {
            //User hasn't changed flash state yet, default to auto.
            if ([self flashModeIsSupportedForDevice:AVCaptureFlashModeAuto]) {
                [self toggleAVFlashAndTorchWithFlashMode:AVCaptureFlashModeAuto];
                [[NSUserDefaults standardUserDefaults] setObject:@(FlashMode_auto) forKey:@"flashMode"];
            } else {
                if ([self flashModeIsSupportedForDevice:AVCaptureFlashModeOff]) {
                    [self toggleAVFlashAndTorchWithFlashMode:AVCaptureFlashModeOff];
                    [[NSUserDefaults standardUserDefaults] setObject:@(FlashMode_off) forKey:@"flashMode"];
                }
                
            }
        }
        
        flashButton.accessibilityLabel = [NSString stringWithFormat:@"Flash Button is set to %@", [self titleForFlashMode]];
        [flashButton setTitle:[self titleForFlashMode] forState:UIControlStateNormal];
        [flashButton addTarget:self action:@selector(toggleFlash) forControlEvents:UIControlEventTouchUpInside];
        
        [cameraView addSubview:flashButton];
        
        if (!_device.isFlashAvailable) {
            [flashButton setAlpha:0.0];
        } else {
            flashButton.alpha = _device.position == AVCaptureDevicePositionBack ? 1.0 : 0.0;
        }
        
        if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront]) {
            flipCameraButton = [UIButton buttonWithType:UIButtonTypeCustom];
            flipCameraButton.isAccessibilityElement = YES;
            flipCameraButton.accessibilityLabel = @"Camera Chooser";
            flipCameraButton.accessibilityHint = @"Chooses between the front facing and back facing cameras";
            [flipCameraButton setTitle:@"\u21BB" forState:UIControlStateNormal];
            flipCameraButton.titleLabel.font = [UIFont systemFontOfSize:36.0];
            [flipCameraButton setFrame:CGRectMake(width - 45, flashFrame.origin.y - 12, 50, 50)];
            [flipCameraButton addTarget:self action:@selector(flipCamera) forControlEvents:UIControlEventTouchUpInside];
            flipCameraButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            flipCameraButton.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
            [cameraView addSubview:flipCameraButton];
            if (self.cameraBackgroundColor == [UIColor whiteColor]) {
                [flipCameraButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            }
            //            NSLog(@"flipCameraButton.frame:%@", NSStringFromCGRect(flipCameraButton.frame));
        }
        if (_device.position == AVCaptureDevicePositionBack && ([[NSUserDefaults standardUserDefaults] boolForKey:@"frontCamera"] == YES || self.faceFrontOnLaunch == YES)) {
            [self flipCamera];
            self.faceFrontOnLaunch = NO;
        }
        //        NSLog(@"flashButton.frame:%@", NSStringFromCGRect(flashButton.frame));
        if ((_titleAboveCamera || ![_titleAboveCamera isEqualToString:@""]) && _cameraMode == CameraMode_square) {
            titleAboveCameraLabel = [UILabel new];
            titleAboveCameraLabel.text = _titleAboveCamera;
            titleAboveCameraLabel.textAlignment = NSTextAlignmentCenter;
            titleAboveCameraLabel.font = [UIFont systemFontOfSize:16.666667];
            titleAboveCameraLabel.textColor = [UIColor whiteColor];
            if (self.cameraBackgroundColor == [UIColor whiteColor]) {
                titleAboveCameraLabel.textColor = [UIColor darkGrayColor];
            }
            titleAboveCameraLabel.minimumScaleFactor = 0.5;
            titleAboveCameraLabel.frame = CGRectMake(0, 30, self.view.frame.size.width, 30);
            [cameraView addSubview:titleAboveCameraLabel];
        }
        if (_showWhiteCircleInSquareMode == YES && _cameraMode == CameraMode_square) {
            clearViewWithWhiteCircle = [self clearViewWithWhiteCircle];
            [cameraView addSubview:clearViewWithWhiteCircle];
            NSLog(@"clearViewWithWhiteCircle:%@", [self clearViewWithWhiteCircle]);
        }
        if (![self.view.subviews containsObject:cameraView]) {
            [self.view addSubview:cameraView];
        }
        [self.view bringSubviewToFront:cameraOverlayBottomView];
    } else {
        [self openPhotoLibrary];
    }
}

-(BOOL)flashModeIsSupportedForDevice:(AVCaptureFlashMode)mode {
    BOOL supported = NO;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 10.0) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        
        supported = [_device isFlashModeSupported:mode];
        
#pragma clang diagnostic pop
        
    } else {
        AVCapturePhotoOutput *output = _cameraOutput;
        for (NSUInteger i = 0; i < output.supportedFlashModes.count; i++) {
            NSNumber *modes = output.supportedFlashModes[i];
            if ([modes integerValue] == mode) {
                supported = YES;
                break;
            }
        }
    }
    return supported;
}

-(void)addCameraOverlayBottomView {
    cameraOverlayBottomView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 100, width, 100)];
    cameraOverlayBottomView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:cameraOverlayBottomView];
    
    
    whiteCircleContainerViewForShutterButton = [[UIView alloc] initWithFrame:CGRectMake(0, 0, cameraOverlayBottomView.frame.size.height/1.5, cameraOverlayBottomView.frame.size.height/1.5)];
    whiteCircleContainerViewForShutterButton.center = CGPointMake(cameraOverlayBottomView.center.x, cameraOverlayBottomView.frame.size.height/2);
    whiteCircleContainerViewForShutterButton.backgroundColor = _cameraMode == CameraMode_fullScreen ? [UIColor colorWithWhite:0.9 alpha:0.1] : [UIColor whiteColor];
    if (self.cameraBackgroundColor == [UIColor whiteColor]) {
        whiteCircleContainerViewForShutterButton.backgroundColor = [UIColor darkGrayColor];
    }
    if (_cameraMode == CameraMode_fullScreen) {
        whiteCircleContainerViewForShutterButton.layer.borderColor = [UIColor whiteColor].CGColor;
        whiteCircleContainerViewForShutterButton.layer.borderWidth = 4.;
    }
    whiteCircleContainerViewForShutterButton.layer.cornerRadius = whiteCircleContainerViewForShutterButton.frame.size.height/2;
    whiteCircleContainerViewForShutterButton.clipsToBounds = YES;
    [cameraOverlayBottomView addSubview:whiteCircleContainerViewForShutterButton];
    //    NSLog(@"whiteCircleContainerView:%@", whiteCircleContainerViewForShutterButton);
    circleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    circleButton.isAccessibilityElement = YES;
    circleButton.accessibilityLabel = @"Shutter.";
    circleButton.accessibilityHint = @"Takes the photo.";
    circleButton.backgroundColor = [self backgroundColorForCircleButton];
    if (_cameraMode == CameraMode_fullScreen) {
        circleButton.backgroundColor = [UIColor colorWithWhite:0.9 alpha:0.1];
    } else if (self.cameraBackgroundColor == [UIColor whiteColor]) {
        circleButton.backgroundColor = [UIColor darkGrayColor];
    }
    circleButton.frame = CGRectMake(whiteCircleContainerViewForShutterButton.frame.size.width * 0.05, whiteCircleContainerViewForShutterButton.frame.size.width * 0.05, whiteCircleContainerViewForShutterButton.frame.size.width * 0.9, whiteCircleContainerViewForShutterButton.frame.size.width * 0.9);
    circleButton.layer.cornerRadius = circleButton.frame.size.height/2;
    circleButton.layer.borderWidth = 1;
    circleButton.layer.borderColor = [UIColor darkGrayColor].CGColor;
    if (self.cameraBackgroundColor == [UIColor whiteColor]) {
        circleButton.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    circleButton.clipsToBounds = YES;
    [circleButton addTarget:self action:@selector(highlightShutterButton:) forControlEvents:UIControlEventTouchDown];
    [circleButton addTarget:self action:@selector(takeThePicture:) forControlEvents:UIControlEventTouchUpInside];
    [whiteCircleContainerViewForShutterButton addSubview:circleButton];
    
    cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelButton.isAccessibilityElement = YES;
    cancelButton.accessibilityLabel = @"Cancel Profile Photo";
    [cancelButton setTitle:@"\u2715" forState:UIControlStateNormal];
    cancelButton.titleLabel.font = [UIFont systemFontOfSize:36.0];
    [cancelButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    cancelButton.frame = CGRectMake(0, 0, cameraOverlayBottomView.frame.size.height/2, cameraOverlayBottomView.frame.size.height/2);
    cancelButton.center = CGPointMake(cameraOverlayBottomView.frame.size.width * 0.15, whiteCircleContainerViewForShutterButton.center.y);
    [cameraOverlayBottomView addSubview:cancelButton];
    if (self.cameraBackgroundColor == [UIColor whiteColor]) {
        [cancelButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    }
    
    if ([PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusAuthorized && _showPictureFromCameraRollForChooseFromPhotoLibraryButton) {
        pickImageFromPhotoLibraryButton = [UIButton buttonWithType:UIButtonTypeCustom];
        pickImageFromPhotoLibraryButton.frame = CGRectMake(0, 0, cameraOverlayBottomView.frame.size.height/2, cameraOverlayBottomView.frame.size.height/2);
        pickImageFromPhotoLibraryButton.center = CGPointMake(cameraOverlayBottomView.frame.size.width * 0.85, whiteCircleContainerViewForShutterButton.center.y);
        [pickImageFromPhotoLibraryButton addTarget:self action:@selector(openPhotoLibrary) forControlEvents:UIControlEventTouchUpInside];
        if (self.cameraBackgroundColor == [UIColor whiteColor]) {
            [pickImageFromPhotoLibraryButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        }
        if (!pickImageFromPhotoLibraryButton.currentBackgroundImage && [PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusAuthorized) {
            if (_showPictureFromCameraRollForChooseFromPhotoLibraryButton) {
                [self getLastPhoto:pickImageFromPhotoLibraryButton];
            } else {
                [pickImageFromPhotoLibraryButton setTitle:@"\u1f5bc" forState:UIControlStateNormal];
            }
            
        }
        [cameraOverlayBottomView addSubview:pickImageFromPhotoLibraryButton];
    }
}

-(void)setFramesForStatusBarOrientation {
    if ([_prevLayer.connection isVideoOrientationSupported])
        [_prevLayer.connection setVideoOrientation:[self captureVideoOrientation]];
    
    width = self.view.frame.size.width; height = self.view.frame.size.height;
    [self setFileSize];
    _prevLayer.frame = [self frameForFileSize];
    cameraView.frame = [[UIScreen mainScreen] bounds];
    clearViewWithWhiteCircle.frame = [self frameForFileSize];
    if (width > height) {
        [self setFramesForLandscape];
    } else {
        [self setFramesForPortrait];
    }
    if (captureMode == NO) {
        [editingCropScrollview removeFromSuperview];
        editingCropScrollview = nil;
        editingCropScrollview = [self editOriginalPhotoView];
        [self.view addSubview:editingCropScrollview];
        [self.view bringSubviewToFront:cameraOverlayBottomView];
        [self.view sendSubviewToBack:editingCropScrollview];
    }
    circleFilterView.frame = [self frameForFileSize];
    [self centerScrollViewContents];
}

-(void)setFramesForLandscape {
    flashFrame = [self frameForFileSize].origin.y > 40. ? CGRectMake(8, [self frameForFileSize].origin.y + 8, 100, 50) : CGRectMake(8, 20, 100, 50);
    flashButton.frame = flashFrame;
    
    [flipCameraButton setFrame:CGRectMake(width - 45, flashFrame.origin.y - 8, 50, 50)];
    titleAboveCameraLabel.frame = CGRectMake(0, 30, width, 30);
    titleAboveCameraLabel.alpha = 0.0;
    titleAboveCameraLabel.center = CGPointMake(self.view.center.x, titleAboveCameraLabel.center.y);
    cameraOverlayBottomView.frame = CGRectMake(0, height - 100, width, 100);
    whiteCircleContainerViewForShutterButton.center = [self centerForWhiteCircleView];
    whiteCircleContainerViewForShutterButton.backgroundColor = [UIColor colorWithWhite:0.9 alpha:0.1];
    whiteCircleContainerViewForShutterButton.layer.borderColor = [UIColor whiteColor].CGColor;
    whiteCircleContainerViewForShutterButton.layer.borderWidth = 4.;
    circleButton.backgroundColor = [self backgroundColorForCircleButton];
    cancelButton.center = CGPointMake(cameraOverlayBottomView.frame.size.width * 0.15, whiteCircleContainerViewForShutterButton.center.y);
    pickImageFromPhotoLibraryButton.center = CGPointMake(cameraOverlayBottomView.frame.size.width * 0.85, whiteCircleContainerViewForShutterButton.center.y);
    if (flashButton.alpha == 0.0) {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            chooseAuto.frame = CGRectMake(0, flashFrame.origin.y, 100, 50);
            chooseOn.frame = CGRectMake(chooseAuto.frame.origin.x + chooseAuto.frame.size.width, flashFrame.origin.y, 100, 50);
            chooseOff.frame = CGRectMake(chooseOn.frame.origin.x + chooseOn.frame.size.width, flashFrame.origin.y, 100, 50);
        }
        else
        {
            chooseAuto.frame = CGRectMake(flashFrame.origin.x, flashFrame.origin.y, 90, 30);
            chooseOn.frame = CGRectMake(chooseAuto.frame.origin.x + chooseAuto.frame.size.width, flashFrame.origin.y, 40, 30);
            chooseOff.frame = CGRectMake(chooseOn.frame.origin.x + chooseOn.frame.size.width, flashFrame.origin.y, 40, 30);
        }
    } else {
        chooseAuto.frame = flashFrame;
        chooseOn.frame = flashFrame;
        chooseOff.frame = flashFrame;
    }
    moveAndScaleLabel.alpha = 0.0;
}

-(void)setFramesForPortrait {
    flashFrame = [self frameForFileSize].origin.y > 40. ? CGRectMake(8, [self frameForFileSize].origin.y - 30, 100, 50) : CGRectMake(8, 20, 100, 50);
    flashButton.frame = flashFrame;
    [flipCameraButton setFrame:CGRectMake(width - 45, flashFrame.origin.y - 12, 50, 50)];
    titleAboveCameraLabel.frame = CGRectMake(0, 30, width, 30);
    titleAboveCameraLabel.alpha = 1.0;
    cameraOverlayBottomView.frame = CGRectMake(0, height - 100, width, 100);
    whiteCircleContainerViewForShutterButton.center = [self centerForWhiteCircleView];
    if (![circleButton.titleLabel.text isEqualToString:@"\u2713"]) {
        circleButton.backgroundColor = [self backgroundColorForCircleButton];
        whiteCircleContainerViewForShutterButton.backgroundColor = _cameraMode == CameraMode_fullScreen ? [UIColor colorWithWhite:0.9 alpha:0.1] : [UIColor whiteColor];
    } else {
        circleButton.backgroundColor = [self backgroundColorForCircleButton];
    }
    
    cancelButton.center = CGPointMake(cameraOverlayBottomView.frame.size.width * 0.15, whiteCircleContainerViewForShutterButton.center.y);
    pickImageFromPhotoLibraryButton.center = CGPointMake(cameraOverlayBottomView.frame.size.width * 0.85, whiteCircleContainerViewForShutterButton.center.y);
    if (flashButton.alpha == 0.0) {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            chooseAuto.frame = CGRectMake(0, flashFrame.origin.y, 100, 50);
            chooseOn.frame = CGRectMake(chooseAuto.frame.origin.x + chooseAuto.frame.size.width, flashFrame.origin.y, 100, 50);
            chooseOff.frame = CGRectMake(chooseOn.frame.origin.x + chooseOn.frame.size.width, flashFrame.origin.y, 100, 50);
        }
        else
        {
            chooseAuto.frame = CGRectMake(flashFrame.origin.x, flashFrame.origin.y, 90, 30);
            chooseOn.frame = CGRectMake(chooseAuto.frame.origin.x + chooseAuto.frame.size.width, flashFrame.origin.y, 40, 30);
            chooseOff.frame = CGRectMake(chooseOn.frame.origin.x + chooseOn.frame.size.width, flashFrame.origin.y, 40, 30);
        }
    } else {
        chooseAuto.frame = flashFrame;
        chooseOn.frame = flashFrame;
        chooseOff.frame = flashFrame;
    }
    moveAndScaleLabel.alpha = 1.0;
    moveAndScaleLabel.center = CGPointMake(self.view.center.x, titleAboveCameraLabel.center.y);
}

-(CGPoint)centerForWhiteCircleView {
    if (captureMode == YES) {
        return CGPointMake(whiteCircleContainerViewForShutterButton.superview.center.x, whiteCircleContainerViewForShutterButton.superview.frame.size.height/2);
    } else {
        return whiteCircleContainerViewForShutterButton.center = CGPointMake(whiteCircleContainerViewForShutterButton.superview.frame.size.width * 0.85, whiteCircleContainerViewForShutterButton.center.y);
    }
}

-(void)animateButtonsForPreview {
    pickImageFromPhotoLibraryButton.alpha = 0.0;
    [cancelButton removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
    [cancelButton addTarget:self action:@selector(openCamera:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setTitle:@"\u2715" forState:UIControlStateNormal];
    [UIView animateWithDuration:0.4 animations:^{
        self->whiteCircleContainerViewForShutterButton.center = CGPointMake(self->whiteCircleContainerViewForShutterButton.superview.frame.size.width * 0.85, self->whiteCircleContainerViewForShutterButton.center.y);
        [self->circleButton setTitle:@"\u2713" forState:UIControlStateNormal];
    }];
    [circleButton removeTarget:nil action:NULL forControlEvents:UIControlEventTouchUpInside];
    [circleButton addTarget:self action:@selector(prepareForUpload:) forControlEvents:UIControlEventTouchUpInside];
    circleButton.backgroundColor = [UIColor darkGrayColor];
    circleButton.titleLabel.font = [UIFont systemFontOfSize:30.];
    
}

-(void)animateButtonsForImageCapture {
    
    [cancelButton removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
    [cancelButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setTitle:@"\u2715" forState:UIControlStateNormal];
    [UIView animateWithDuration:0.4 animations:^{
        self->whiteCircleContainerViewForShutterButton.center = CGPointMake(self->whiteCircleContainerViewForShutterButton.superview.center.x, self->whiteCircleContainerViewForShutterButton.superview.frame.size.height/2);
        [self->circleButton setTitle:@"" forState:UIControlStateNormal];
    } completion:^(BOOL finished) {
        if ([PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusAuthorized && self->_showPictureFromCameraRollForChooseFromPhotoLibraryButton) {
            self->pickImageFromPhotoLibraryButton.alpha = 1.0;
        }
    }];
    [circleButton removeTarget:nil action:NULL forControlEvents:UIControlEventTouchUpInside];
    [circleButton addTarget:self action:@selector(takeThePicture:) forControlEvents:UIControlEventTouchUpInside];
    circleButton.backgroundColor = [self backgroundColorForCircleButton];
    
}

- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

-(void)getLastPhoto:(id)sender {
    pickImageFromPhotoLibraryButton = (UIButton *)sender;
    PHImageRequestOptions *options = [[PHImageRequestOptions alloc]init];
    options.synchronous = YES;
    PHFetchOptions *fetchOptions = [[PHFetchOptions alloc]init];
    fetchOptions.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
    
    PHFetchResult *photos = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:fetchOptions];
    if (photos) {
        [[PHImageManager defaultManager] requestImageForAsset:[photos objectAtIndex:0] targetSize:pickImageFromPhotoLibraryButton.frame.size contentMode:PHImageContentModeAspectFit options:options resultHandler:^(UIImage *result, NSDictionary *info) {
            [self->pickImageFromPhotoLibraryButton setBackgroundImage:result forState:UIControlStateNormal];
        }];
    } else {
        [pickImageFromPhotoLibraryButton setBackgroundImage:[UIImage imageNamed:@"photos_icon"] forState:UIControlStateNormal];
    }
}

-(UIColor *)backgroundColorForCircleButton {
    if (captureMode == NO) {
        return [UIColor darkGrayColor];
    }
    if (_cameraMode == CameraMode_fullScreen) {
        return [UIColor colorWithWhite:0.9 alpha:0.1];
    } else if (self.cameraBackgroundColor == [UIColor whiteColor]) {
        return [UIColor darkGrayColor];
    }
    
    return [UIColor whiteColor];
}

-(UIView *)clearViewWithWhiteCircle {
    UIView *view = [[UIView alloc] initWithFrame:[self frameForFileSize]];
    view.clipsToBounds = YES;
    view.backgroundColor = [UIColor clearColor];
    view.layer.cornerRadius = view.frame.size.width/2;
    view.layer.borderColor = [UIColor colorWithWhite:0.96 alpha:1.0].CGColor;
    view.layer.borderWidth = 2.0;
    view.tag = 66;
    view.userInteractionEnabled = NO;
    return view;
}

#pragma mark - Actions

-(void)flipCamera {
    AVCaptureDevice *newCaptureDevice = nil;
    if (_device.position == AVCaptureDevicePositionFront) {
        newCaptureDevice = [self cameraWithPosition:AVCaptureDevicePositionBack];
        if (newCaptureDevice.isFlashAvailable) {
            [UIView animateWithDuration:0.2 animations:^{
                self->flashButton.alpha = 1.0;
            }];
        } else {
            flashButton.alpha = 0.0;
        }
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"frontCamera"];
    }
    else if (_device.position == AVCaptureDevicePositionBack) {
        newCaptureDevice = [self cameraWithPosition:AVCaptureDevicePositionFront];
        [UIView animateWithDuration:0.2 animations:^{
            self->flashButton.alpha = 0.0;
        }];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"frontCamera"];
    }
    NSError *err = nil;
    _device = newCaptureDevice;
    [_session removeInput:_input];
    _input = [[AVCaptureDeviceInput alloc] initWithDevice:_device error:&err];
    if (_input) {
        [_session addInput:_input];
    }
}

- (AVCaptureDevice *) cameraWithPosition:(AVCaptureDevicePosition) position
{

    NSArray *devices;
    NSLog(@"devices:%@", devices);
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 10.0) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        
        devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];

        
#pragma clang diagnostic pop
    } else {
        devices = ((AVCaptureDeviceDiscoverySession *)[AVCaptureDeviceDiscoverySession discoverySessionWithDeviceTypes:@[AVCaptureDeviceTypeBuiltInDualCamera, AVCaptureDeviceTypeBuiltInWideAngleCamera, AVCaptureDeviceTypeBuiltInTelephotoCamera] mediaType:AVMediaTypeVideo position:AVCaptureDevicePositionUnspecified]).devices;

    }
    
    for (AVCaptureDevice *device in devices)
    {
        if ([device position] == position) return device;
    }
    return nil;
}

-(void)highlightShutterButton:(id)sender {
    UIButton *button = (UIButton *)sender;
    button.backgroundColor = [UIColor darkGrayColor];
}



-(void)takeThePicture:(id)sender {
    fromLibrary = NO;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 10.0) {
        [self preiOS10CameraCapture];
    } else {
        AVCapturePhotoOutput *output = _cameraOutput;
            _capturePhotoSettings.flashMode = _device.position == AVCaptureDevicePositionFront ? AVCaptureFlashModeOff : AVCaptureFlashModeAuto;
        [output capturePhotoWithSettings:_capturePhotoSettings delegate:self];
    }
}

-(void)captureOutput:(AVCapturePhotoOutput *)captureOutput didFinishProcessingPhotoSampleBuffer:(CMSampleBufferRef)photoSampleBuffer previewPhotoSampleBuffer:(CMSampleBufferRef)previewPhotoSampleBuffer resolvedSettings:(AVCaptureResolvedPhotoSettings *)resolvedSettings bracketSettings:(AVCaptureBracketedStillImageSettings *)bracketSettings error:(NSError *)error {
    if (error) {
        NSLog(@"error : %@", error.localizedDescription);
    }
    
    if (photoSampleBuffer) {
        NSData *data = [AVCapturePhotoOutput JPEGPhotoDataRepresentationForJPEGSampleBuffer:photoSampleBuffer previewPhotoSampleBuffer:previewPhotoSampleBuffer];
        UIImage *image = [UIImage imageWithData:data];
        [self pictureFinishedTaking:image];
    }
}

-(void)preiOS10CameraCapture {
    _captureConnection = nil;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
    
    AVCaptureStillImageOutput *output = _cameraOutput;
    
#pragma clang diagnostic pop
    
    if (!_captureConnection) {
        for (AVCaptureConnection *connection in output.connections) {
            for (AVCaptureInputPort *port in [connection inputPorts]) {
                if ([[port mediaType] isEqual:AVMediaTypeVideo]) {
                    _captureConnection = connection;
                    break;
                }
            }
            if (_captureConnection) {
                break;
            }
        }
    }
    //    CGRect rect = _cameraOutput.rectOfInterest;
    [_cameraOutput captureStillImageAsynchronouslyFromConnection:_captureConnection completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        
        NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
        
#pragma clang diagnostic pop
        
        UIImage *image = [[UIImage alloc]initWithData:imageData];
        [self pictureFinishedTaking:image];
    }];
}

-(void)toggleFlash {
    
    //    [UIView animateWithDuration:0.3 animations:^{
    flashButton.alpha = 0.0;
    //    }];
    chooseAuto = [UIButton buttonWithType:UIButtonTypeCustom];
    chooseAuto.frame = flashFrame;
    [chooseAuto setTitle:@"\u26a1 Auto" forState:UIControlStateNormal];
    [chooseAuto addTarget:self action:@selector(chooseFlashAuto) forControlEvents:UIControlEventTouchUpInside];
    [cameraView addSubview:chooseAuto];
    if (self.cameraBackgroundColor == [UIColor whiteColor]) {
        [chooseAuto setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    }
    
    chooseOn = [UIButton buttonWithType:UIButtonTypeCustom];
    chooseOn.frame = flashFrame;
    [chooseOn setTitle:@"On" forState:UIControlStateNormal];
    [chooseOn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [chooseOn addTarget:self action:@selector(chooseFlashOn) forControlEvents:UIControlEventTouchUpInside];
    [cameraView addSubview:chooseOn];
    [chooseOn setAlpha:0.0];
    if (self.cameraBackgroundColor == [UIColor whiteColor]) {
        [chooseOn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    }
    
    chooseOff = [UIButton buttonWithType:UIButtonTypeCustom];
    [chooseOff setTitle:@"Off" forState:UIControlStateNormal];
    [chooseOff setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [chooseOff addTarget:self action:@selector(chooseFlashOff) forControlEvents:UIControlEventTouchUpInside];
    [cameraView addSubview:chooseOff];
    [chooseOff setAlpha:0.0];
    chooseOff.frame = flashFrame;
    if (self.cameraBackgroundColor == [UIColor whiteColor]) {
        [chooseOff setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    }
    
    [UIView animateWithDuration:0.2 animations:^{
        [self->chooseAuto setAlpha:1.0];
        [self->chooseOn setAlpha:1.0];
        [self->chooseOff setAlpha:1.0];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            self->chooseAuto.frame = CGRectMake(0, self->flashFrame.origin.y, 100, 50);
            self->chooseOn.frame = CGRectMake(self->chooseAuto.frame.origin.x + self->chooseAuto.frame.size.width, self->flashFrame.origin.y, 100, 50);
            self->chooseOff.frame = CGRectMake(self->chooseOn.frame.origin.x + self->chooseOn.frame.size.width, self->flashFrame.origin.y, 100, 50);
        }
        else
        {
            self->chooseAuto.frame = CGRectMake(self->flashFrame.origin.x, self->flashFrame.origin.y, 90, 30);
            self->chooseOn.frame = CGRectMake(self->chooseAuto.frame.origin.x + self->chooseAuto.frame.size.width, self->flashFrame.origin.y, 40, 30);
            self->chooseOff.frame = CGRectMake(self->chooseOn.frame.origin.x + self->chooseOn.frame.size.width, self->flashFrame.origin.y, 40, 30);
        }
    }];
}

-(void) chooseFlashAuto {
    if ([self flashModeIsSupportedForDevice:AVCaptureFlashModeAuto]) {
        [self toggleAVFlashAndTorchWithFlashMode:AVCaptureFlashModeAuto];
    }
    [self animateFlashButtonsBackToOne];
    [[NSUserDefaults standardUserDefaults] setObject:@(FlashMode_auto) forKey:@"flashMode"];
    [flashButton setTitle:[self titleForFlashMode] forState:UIControlStateNormal];
}

-(void)chooseFlashOn {
    if ([self flashModeIsSupportedForDevice:AVCaptureFlashModeOn]) {
        [self toggleAVFlashAndTorchWithFlashMode:AVCaptureFlashModeOn];
    }
    [self animateFlashButtonsBackToOne];
    [[NSUserDefaults standardUserDefaults] setObject:@(FlashMode_on) forKey:@"flashMode"];
    [flashButton setTitle:[self titleForFlashMode] forState:UIControlStateNormal];
}

-(void) chooseFlashOff {
    if ([self flashModeIsSupportedForDevice:AVCaptureFlashModeOff]) {
        [self toggleAVFlashAndTorchWithFlashMode:AVCaptureFlashModeOff];
    }
    [self animateFlashButtonsBackToOne];
    [[NSUserDefaults standardUserDefaults] setObject:@(FlashMode_off) forKey:@"flashMode"];
    [flashButton setTitle:[self titleForFlashMode] forState:UIControlStateNormal];
}

-(void)toggleAVFlashAndTorchWithFlashMode:(AVCaptureFlashMode)flashMode {
    NSError *error;
    [_device lockForConfiguration:&error];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 10.0) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        
    [_device setFlashMode:flashMode];
        
#pragma clang diagnostic pop
        
    } else {
        _capturePhotoSettings.flashMode = _device.position == AVCaptureDevicePositionFront ? AVCaptureFlashModeOff : AVCaptureFlashModeAuto;

     //   _capturePhotoSettings.flashMode = flashMode;
    
    }
}

-(void)animateFlashButtonsBackToOne {
    [UIView animateWithDuration:0.2 animations:^{
        self->chooseOn.frame = self->flashFrame;
       self->chooseOff.frame = self->flashFrame;
        self->chooseAuto.frame = self->flashFrame;
    }];
    [UIView animateWithDuration:0.1 delay:0.05 options:0 animations:^{
        [self->chooseAuto setAlpha:0.0];
        [self->chooseOn setAlpha:0.0];
        [self->chooseOff setAlpha:0.0];
        [self->flashButton setAlpha:1.0];
    } completion:NULL];
}

-(NSString *)titleForFlashMode {
    NSNumber *flashModeNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"flashMode"];
    if ([flashModeNumber isEqualToNumber:@(FlashMode_off)]) {
        return @"\u26a1 Off";
    } else if ([flashModeNumber isEqualToNumber:@(FlashMode_on)]) {
        return @"\u26a1 On";
    }
    return @"\u26a1 Auto";
}



#pragma mark - Image Picker Delegate

-(void)openPhotoLibrary {
    fromLibrary = YES;
    imagePicker = [UIImagePickerController new];
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.view.frame = CGRectMake(0, 0, width, height);
    imagePicker.isAccessibilityElement = YES;
    imagePicker.delegate = self;
    
    for (UIView *subviews in imagePicker.view.subviews) {
        if ([subviews isKindOfClass:[CircleFilterView class]]) {
            [subviews removeFromSuperview];
        }
    }
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    photoLibraryStatusBarAppearance = YES;
    [self setNeedsStatusBarAppearanceUpdate];
    [self.view addSubview:imagePicker.view];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    if (imagePicker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] && [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusAuthorized) {
            //     imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            fromLibrary = NO;
            photoLibraryStatusBarAppearance = NO;
            [self setNeedsStatusBarAppearanceUpdate];
            [imagePicker.view removeFromSuperview];
            [self openCamera:self];
        } else {
            //            NSLog(@"going back");
            //            self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
            //            [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
            //            [self setNeedsStatusBarAppearanceUpdate];
            [self back];
        }
    }
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    if (picker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary) {
        fromLibrary = YES;
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] && [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusAuthorized) {
            //           imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        }
    }
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    photoLibraryStatusBarAppearance = NO;
    [self setNeedsStatusBarAppearanceUpdate];
    [imagePicker.view removeFromSuperview];
    
    [self pictureFinishedTaking:[info objectForKey:UIImagePickerControllerOriginalImage]];
    
}

-(void)pictureFinishedTaking:(UIImage *)image {
    captureMode = NO;
    picture = image;
    [self transformPicture];
    [self buildPreviewUI];
    [self performSelector:@selector(removeCaptureViews) withObject:nil afterDelay:0.2];
    [self animateButtonsForPreview];
}

-(void)transformPicture {
    if (fromLibrary == NO) {
        if ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft) {
            picture = [UIImage imageWithCGImage:picture.CGImage scale:picture.scale orientation:UIImageOrientationDown];
        } else if ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight) {
            picture = [UIImage imageWithCGImage:picture.CGImage scale:picture.scale orientation:UIImageOrientationUp];
        }
    }
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"frontCamera"] == YES && fromLibrary == NO) {
        if (picture.imageOrientation == 3) {
            picture = [UIImage imageWithCGImage:picture.CGImage scale:picture.scale orientation:UIImageOrientationLeftMirrored];
        } else if (picture.imageOrientation == 0) {
            picture = [UIImage imageWithCGImage:picture.CGImage scale:picture.scale orientation:UIImageOrientationUpMirrored];
        }
        if ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft) {
            
            picture = [UIImage imageWithCGImage:picture.CGImage scale:picture.scale orientation:UIImageOrientationUpMirrored];
        } else if ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight) {
            picture = [UIImage imageWithCGImage:picture.CGImage scale:picture.scale orientation:UIImageOrientationDownMirrored];
        }
    }
}

-(void)removeCaptureViews {
    [_prevLayer removeFromSuperlayer];
    _prevLayer = nil;
    //    previewToolbar.hidden = NO;
    [cameraView removeFromSuperview];
    cameraView = nil;
}

-(void)buildPreviewUI {
    editingCropScrollview = [self editOriginalPhotoView];
    [self.view addSubview:editingCropScrollview];
    [self.view sendSubviewToBack:editingCropScrollview];
    [self centerScrollViewContents];
    if (_cameraMode == CameraMode_square) {
        if (_showWhiteCircleInSquareMode == YES) {
            clearViewWithWhiteCircle = [self clearViewWithWhiteCircle];
            [self.view addSubview:clearViewWithWhiteCircle];
            circleFilterView = [self circleFilterView];
            [self.view addSubview:circleFilterView];
        }
        [self addMoveAndScaleLabel];
        [self saveCroppedImage];
    }
}

-(void)addMoveAndScaleLabel {
    moveAndScaleLabel = [UILabel new];
    moveAndScaleLabel.font = [UIFont systemFontOfSize:16.666667];
    moveAndScaleLabel.frame = CGRectMake(0, 30, self.view.frame.size.width, 30);
    moveAndScaleLabel.text = @"Move and Scale";
    moveAndScaleLabel.textColor = [UIColor whiteColor];
    if (self.cameraBackgroundColor == [UIColor whiteColor]) {
        moveAndScaleLabel.textColor = [UIColor darkGrayColor];
    }
    moveAndScaleLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:moveAndScaleLabel];
    if ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight) {
        moveAndScaleLabel.alpha = 0.0;
    }
}

-(CircleFilterView *)circleFilterView {
    CircleFilterView *view = [[CircleFilterView alloc] init];
    view.frame = [self frameForFileSize];
    [view setBackgroundColor:[UIColor colorWithWhite:0.2 alpha:0.0]];
    [view setFillColor:[UIColor colorWithWhite:0.1f alpha:0.7]];
    [view setFramesToCutOut:@[[NSValue valueWithCGRect:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)]]];
    view.userInteractionEnabled = NO;
    return view;
}

#pragma mark - Photo processing

-(UIScrollView *)editOriginalPhotoView {
    UIScrollView *editView = [[UIScrollView alloc] initWithFrame:[self frameForFileSize]];
    editView.delegate = self;
    editView.center = self.view.center;
    editView.contentSize = CGSizeMake(picture.size.width, picture.size.height);
    editView.backgroundColor = self.cameraBackgroundColor;
//    editView.backgroundColor = [UIColor greenColor];
    editView.userInteractionEnabled = YES;
    editView.scrollEnabled = CameraMode_square ? YES : NO;
    editImageView = [[UIImageView alloc] initWithImage:picture];
    //    NSLog(@"width:%f height:%f", uneditedImage.size.width, uneditedImage.size.height);
    editImageView.frame = CGRectMake(0, 0, picture.size.width, picture.size.height);
    originalImageHeight = picture.size.height;
    originalImageWidth = picture.size.width;
    
    [editView addSubview:editImageView];
    
    CGFloat minScale;
    if (editView.contentSize.width > 0 && editView.contentSize.height > 0) {
        minScale = MIN(width / editView.contentSize.width, height / editView.contentSize.height);
    } else {
        minScale = 1.0;
    }
    
    [editView setMinimumZoomScale:minScale];
    [editView setMaximumZoomScale:1.0f];
    editView.zoomScale = minScale;
    
    if (_cameraMode == CameraMode_square && !fromLibrary) {
        UITapGestureRecognizer *doubleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewDoubleTapped:)];
        doubleTapRecognizer.numberOfTapsRequired = 2;
        doubleTapRecognizer.numberOfTouchesRequired = 1;
        [editView addGestureRecognizer:doubleTapRecognizer];
        
        UITapGestureRecognizer *twoFingerTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewTwoFingerTapped:)];
        twoFingerTapRecognizer.numberOfTapsRequired = 1;
        twoFingerTapRecognizer.numberOfTouchesRequired = 2;
        [editView addGestureRecognizer:twoFingerTapRecognizer];
        
        
    }
    
    if (picture.size.height != _fileSize.height) {
        CGPoint contentOffset = picture.size.width > picture.size.height ? CGPointMake(picture.size.width * ([self frameForFileSize].origin.x/width * minScale), 0) : CGPointMake(0, picture.size.height * ([self frameForFileSize].origin.y/height) * minScale);
        [editView setContentOffset:contentOffset];
    }
    //    [self.view addSubview:blackView];
    
    return editView;
}

-(void)saveCroppedImage {
    
    
    float scale = 1.0f/editingCropScrollview.zoomScale;
    editImageView.image = [self imageWithImage:editImageView.image convertToSize:CGSizeMake(originalImageWidth, originalImageHeight)];
    CGRect visibleRect;
    visibleRect.origin.x = editingCropScrollview.contentOffset.x * scale;
    visibleRect.origin.y = editingCropScrollview.contentOffset.y * scale;
    visibleRect.size.width = editingCropScrollview.frame.size.width * scale;
    visibleRect.size.height = editingCropScrollview.frame.size.height * scale;
    
    picture = [self croppIngimageByImageName:editImageView.image toRect:visibleRect];
    picture = [self imageWithImage:picture convertToSize:CGSizeMake(_fileSize.width, _fileSize.height)];
}

-(void)retakePicture {
    [editingCropScrollview removeFromSuperview];
    [moveAndScaleLabel removeFromSuperview];
    [self openCamera:self];
    //   [self removeFromSuperview];
}

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return editImageView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    // The scroll view has zoomed, so you need to re-center the contents
    [self centerScrollViewContents];
}

- (void)centerScrollViewContents {
    CGSize scrollViewFrameSize = editingCropScrollview.bounds.size;
    CGRect pictureImageViewFrame = editImageView.frame;
    if (pictureImageViewFrame.size.width < scrollViewFrameSize.width) {
        pictureImageViewFrame.origin.x = (scrollViewFrameSize.width - pictureImageViewFrame.size.width) / [UIScreen mainScreen].scale;
    } else {
        pictureImageViewFrame.origin.x = 0.0f;
    }
    
    if (pictureImageViewFrame.size.height < scrollViewFrameSize.height) {
        pictureImageViewFrame.origin.y = (scrollViewFrameSize.height - pictureImageViewFrame.size.height) / [UIScreen mainScreen].scale;
    } else {
        pictureImageViewFrame.origin.y = 0.0f;
    }
    
    editImageView.frame = pictureImageViewFrame;
    
}
- (void)scrollViewDoubleTapped:(UITapGestureRecognizer*)recognizer {
    CGPoint pointInView = [recognizer locationInView:editImageView];
    
    // 2
    CGFloat newZoomScale = editingCropScrollview.zoomScale * 1.5f;
    newZoomScale = MIN(newZoomScale, editingCropScrollview.maximumZoomScale);
    
    // 3
    CGSize scrollViewSize = editingCropScrollview.bounds.size;
    
    CGFloat w = scrollViewSize.width / newZoomScale;
    CGFloat h = scrollViewSize.height / newZoomScale;
    
    CGFloat x = pointInView.x - (w / 2.0f);
    CGFloat y = pointInView.y - (h / 2.0f);
    
    CGRect rectToZoomTo = CGRectMake(x, y, w, h);
    
    // 4
    [editingCropScrollview zoomToRect:rectToZoomTo animated:YES];
}
- (void)scrollViewTwoFingerTapped:(UITapGestureRecognizer*)recognizer {
    // Zoom out slightly, capping at the minimum zoom scale specified by the scroll view
    CGFloat newZoomScale = editingCropScrollview.zoomScale / 1.5f;
    newZoomScale = MAX(newZoomScale, editingCropScrollview.minimumZoomScale);
    [editingCropScrollview setZoomScale:newZoomScale animated:YES];
}

- (UIImage *)croppIngimageByImageName:(UIImage *)imageToCrop toRect:(CGRect)rect
{
    //CGRect CropRect = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height+15);
//    NSLog(@"imageToCrop:%@", imageToCrop);
    CGImageRef imageRef = CGImageCreateWithImageInRect([imageToCrop CGImage], rect);
    UIImage *cropped = [UIImage imageWithCGImage:imageRef scale:imageToCrop.scale orientation:imageToCrop.imageOrientation];
    CGImageRelease(imageRef);
//    NSLog(@"cropped:%@", cropped);
    return cropped;
}

#pragma mark - Start Uploading Methods



-(IBAction)prepareForUpload:(id)sender {
    
    [self saveCroppedImage];
    //    picture = [self fixOrientationWithImage:picture];
    //    UIImage *thumbnailVersion = [self imageWithImage:picture convertToSize:[self thumbnailSize]];
    [self requestCompleted];
}

-(CGSize)thumbnailSize {
    if (_fileSize.width <= _fileSize.height) {
        return CGSizeMake((100 * _fileSize.width) / _fileSize.height, 100);
    } else {
        return CGSizeMake(100, (100 * _fileSize.height)/_fileSize.width);
    }
}



- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

#pragma mark - Finished

-(void) requestCompleted {
    if (_fileType == FileType_jpg) {
        picture = [UIImage imageWithData:UIImageJPEGRepresentation(picture, 1.0)];
    }
    [self back];
    [self.delegate dismissCameraWithImage:picture];
}




@end
