//
//  PushNotification.h
//  OnDemand Timesheets
//
//  Created by Scott Mahonis on 5/11/17.
//  Copyright © 2017 Jed Mahonis Group. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef NS_ENUM(NSUInteger, PushType) {
    PushType_1,
    PushType_2,
    PushType_3,
    PushType_4,
};

@interface PushNotification : NSObject

@property (nonatomic) PushType pushType;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
