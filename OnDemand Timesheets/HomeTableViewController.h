//
//  HomeTableViewController.h
//  OnDemand Timesheets
//
//  Created by Joey Kjornes on 3/16/17.
//  Copyright © 2017 Jed Mahonis Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import "UserSettingsOptionsTableViewController.h"
#import <UserNotifications/UserNotifications.h>
#import <CoreLocation/CoreLocation.h>


@interface HomeTableViewController : UITableViewController <UserSettingsOptionsTableViewControllerDelegate, UIPopoverControllerDelegate, UNUserNotificationCenterDelegate, CLLocationManagerDelegate, UIPopoverPresentationControllerDelegate>

@property (nonatomic, weak) IBOutlet UIButton *findShiftsButton;
@property (nonatomic, weak) IBOutlet UIButton *seeShiftsButton;
@property (nonatomic, weak) IBOutlet UIImageView *agencyImage;
@property (nonatomic, weak) IBOutlet UILabel *helloUserLabel;
@property (nonatomic, strong) User *currentUser;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *settingsBarButtonItem;
@property (nonatomic, strong) CLLocationManager *locManager;




@end
