//
//  ForgotPasswordTableViewController.h
//  OnDemand Timesheets
//
//  Created by Joey Kjornes on 5/10/18.
//  Copyright © 2018 Jed Mahonis Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordTableViewController : UITableViewController  <UITextViewDelegate>

@property(nonatomic, strong) NSString *emailString;

@property (nonatomic, weak) IBOutlet UITextField *emailTextfield;
@property (nonatomic, weak) IBOutlet UIButton *resetButton;
@property (nonatomic , weak) IBOutlet UIToolbar *keyboardToolbar;


@end
