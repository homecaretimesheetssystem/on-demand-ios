//
//  BrowseWorkersTableViewController.h
//  OnDemand Timesheets
//
//  Created by Scott Mahonis on 3/26/18.
//  Copyright © 2018 Jed Mahonis Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrowseWorkersTableViewController : UITableViewController

@property (nonatomic) NSString *errorString;
@property (nonatomic) BOOL isLoading;
@property (nonatomic) NSArray *dataArray;
@property (nonatomic) NSMutableDictionary *sections;
@property (nonatomic) BOOL isSearching;
@property (nonatomic) NSMutableArray *searchArray;

@end
