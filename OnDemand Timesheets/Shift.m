//
//  Shift.m
//  OnDemand Timesheets
//
//  Created by Joey Kjornes on 3/13/17.
//  Copyright © 2017 Jed Mahonis Group. All rights reserved.
//

#import "Shift.h"
#import "Request.h"

@implementation Shift


-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
  //  NSLog(@"dictionary dictionary in shift %@", dictionary);
    if (self == [super init]) {
        
        if ([dictionary objectForKey:@"startShiftTime"] != [NSNull null]) {
            _shiftStartTime = dictionary[@"startShiftTime"];
        }else{
            _shiftStartTime = @"";
        }
        
        if ([dictionary objectForKey:@"endShiftTime"] != [NSNull null]) {
            _shiftEndTime = dictionary[@"endShiftTime"];
        }else{
            _shiftEndTime = @"";
        }
        
        if ([dictionary objectForKey:@"address"] != [NSNull null]) {
            _recipientAddress = dictionary[@"address"];
        }else{
            _recipientAddress = @"";
        }
        
        if ([dictionary objectForKey:@"id"] != [NSNull null]) {
            _rsvpID = dictionary[@"id"];
        }else{
            _rsvpID = [NSNumber numberWithLong:0];
        }
        
        if ([dictionary objectForKey:@"eta"] != [NSNull null]) {
            _etaString = dictionary[@"eta"];
        }else{
            _etaString = @"";
        }
        
        if ([dictionary objectForKey:@"accepted"] != [NSNull null]) {
            _accepted = dictionary[@"accepted"];
        }else{
            _accepted = [NSNumber numberWithLong:0];
        }
        
        if ([dictionary objectForKey:@"declined"] != [NSNull null]) {
            _declined = dictionary[@"declined"];
        }else{
            _declined = [NSNumber numberWithLong:0];
        }
        
        
        
    //    _request = [dictionary valueForKey:@"request"];
     //   NSLog(@"request array %@", reqArray);
       // _request = reqArray[0];
        
//        NSMutableArray *mutArr = [NSMutableArray new];
//        for (NSDictionary *dict in reqArray) {
//            _recipientAddress = dictionary[@"address"];
//            _shiftEndTime = dictionary[@"endShiftTime"];
//            _shiftStartTime = dictionary[@"startShiftTime"];
//            NSLog(@"recip address shifts %@", _recipientAddress);
////            NSLog(@"dictionary in for loop request %@", dict);
      //  for (NSDictionary *reqDict in dictionary[@"request"]) {
              self.request = [[Request alloc] initWithDictionary:dictionary[@"request"]];

        //}
          //  self.request = [[Request alloc] initWithDictionary:dictionary[@"request"]];
     //   _rsvpsArray = dictionary[@"rsvps"];
        for (NSDictionary *dict in dictionary[@"rsvps"]) {
            self.rsvp = [[RSVP alloc] initWithDictionary:dict];

        }
////            [mutArr addObject:request];
//        }
//        _requestsArray = [mutArr mutableCopy];
    }
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self == [super init]) {
        
        _accepted = [aDecoder decodeObjectForKey:@"accepted"];
        _declined = [aDecoder decodeObjectForKey:@"declined"];
        _rsvpID = [aDecoder decodeObjectForKey:@"id"];
        _etaString = [aDecoder decodeObjectForKey:@"eta"];
        _request = [aDecoder decodeObjectForKey:@"request"];
        _rsvp = [aDecoder decodeObjectForKey:@"rsvps"];
        _shiftStartTime = [aDecoder decodeObjectForKey:@"startShiftTime"];
        _shiftEndTime = [aDecoder decodeObjectForKey:@"endShiftTime"];
     //   _rsvpsArray = [aDecoder decodeObjectForKey:@"rsvps"];
    
        
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:_accepted forKey:@"accepted"];
    [aCoder encodeObject:_declined forKey:@"declined"];
    [aCoder encodeObject:_rsvpID forKey:@"id"];
    [aCoder encodeObject:_etaString forKey:@"eta"];
    [aCoder encodeObject:_request forKey:@"request"];
    [aCoder encodeObject:_rsvp forKey:@"rsvps"];
    [aCoder encodeObject:_shiftStartTime forKey:@"startShiftTime"];
    [aCoder encodeObject:_shiftEndTime forKey:@"endShiftTime"];
   // [aCoder encodeObject:_rsvpsArray forKey:@"rsvps"];
    
}

-(NSString *)description {
    return [NSString stringWithFormat:@"RSVP ID:%@\r Accepted:%@\r Declined:%@\r ETA:%@\r Request:%@\r  Start Shift Time:%@\r   End Shift Time:%@\r RSVPS:%@\r", _rsvpID, _accepted, _declined, _etaString, _request, _shiftStartTime, _shiftEndTime, _rsvp];
}

@end
