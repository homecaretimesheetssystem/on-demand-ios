//
//  AppDelegate.h
//  OnDemand Timesheets
//
//  Created by Joey Kjornes on 2/27/17.
//  Copyright © 2017 Jed Mahonis Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>
#import "PushNotification.h"
#import "PushNotificationView.h"
@import Firebase;

@interface AppDelegate : UIResponder <UIApplicationDelegate, UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navCont;
@property (nonatomic) BOOL shouldRotate;

-(void)notificationBannerTouched:(id)sender withPushNotification:(PushNotification *)pushNotification;
//- (void)registerForRemoteNotification;

//-(void)unregisterForPushNotifications;


@end
