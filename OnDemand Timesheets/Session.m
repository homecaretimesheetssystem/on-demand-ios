//
//  Session.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 2/17/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "Session.h"

@implementation Session

//-(instancetype)initWithDictionary:(NSDictionary *)dict andIndex:(long)index andUserType:(UserType)userType {
//    NSLog(@"dict:%@", dict[@"continueTimesheetNumber"]);
//    if (self == [super init]) {
//        
//        
//        
////        NSLog(@"dict[amazonUrl]:%@", dict[@"amazonUrl"]);
//        if (dict[@"amazonUrl"] && ![dict[@"amazonUrl"] isEqual:[NSNull null]]) {
//            _fileURL = dict[@"amazonUrl"];
//        }
//                
//        _verificationIsRequiredForWorkerAndRecipient = [NSNumber numberWithBool:NO];
//        /*   SESSION DATA    */
//        if (dict[@"sessionData"] && [dict[@"sessionData"] isKindOfClass:[NSDictionary class]]) {
//            NSDictionary *sessionDict = dict[@"sessionData"];
////            NSLog(@"sessionData:%@", sessionDict);
//            [self getSessionDataFromDictionary:sessionDict andUserType:userType];
//        } else if (dict[@"sessionData"] && [dict[@"sessionData"] isKindOfClass:[NSArray class]]) {
//            [self getSessionDataFromDictionary:dict[@"sessionData"][index] andUserType:userType];
//            
//        }
////        NSLog(@"_currentTimecard:%@", _currentTimecard);
//    }
//    return self;
//}

//-(void)getSessionDataFromDictionary:(NSDictionary *)sessionDict andUserType:(UserType)userType {
//    if (sessionDict[@"id"]) {
//        if (![sessionDict[@"id"] isEqual:[NSNull null]]) {
//            _idNumber = sessionDict[@"id"];
//        }
//    }
//    
//    //            NSLog(@"sessionDictRatio:%@", sessionDict[@"ratio"]);
//    if (sessionDict[@"ratio"]) {
//        _ratio = [[Ratio alloc] initWithDictionary:sessionDict[@"ratio"]];
//    } else {
//        _ratio = [Ratio new];
//    }
//    
//    if (sessionDict[@"sharedCareLocation"]) {
//        _careLocation = [[CareLocation alloc] initWithDictionary:sessionDict[@"sharedCareLocation"]];
//    } else {
//        _careLocation = [CareLocation new];
//    }
//    
//    NSDictionary *timeInDict = sessionDict[@"timeIn"];
//    if (timeInDict) {
//        _timeIn = [[TimeIn alloc] initWithDictionary:timeInDict];
//    } else {
//        _timeIn = [TimeIn new];
//    }
//    
//    NSDictionary *timeOutDict = sessionDict[@"timeOut"];
//    if (timeOutDict) {
//        _timeOut = [[TimeOut alloc] initWithDictionary:timeOutDict];
//    } else {
//        _timeOut = [TimeOut new];
//    }
//    
//    if (sessionDict[@"currentTimesheetNumber"]) {
//        _currentTimesheetNumber = sessionDict[@"currentTimesheetNumber"];
//    } else {
//        _currentTimesheetNumber = @1;
//    }
//    /*
//     if (sessionDict[@"flagForFurtherInvestigation"]) {
//     _flagForFurtherInvestigation = [NSNumber numberWithBool:[dict[@"flagForFurtherInvestigation"] boolValue]];
//     } else {
//     _flagForFurtherInvestigation = [NSNumber numberWithBool:@NO];
//     }
//     */
//    if (sessionDict[@"continueTimesheetNumber"]) {
//        _continueTimesheetNumber = sessionDict[@"continueTimesheetNumber"];
//    } else {
//        _continueTimesheetNumber = @0;
//    }
//    
//    if (sessionDict[@"currentTimesheetId"]) {
//        _currentTimesheetID = sessionDict[@"currentTimesheetId"];
//    } else {
//        _currentTimesheetID = @0;
//    }
//    
//    if (sessionDict[@"service"]) {
//        _service = [[Service alloc] initWithDictionary:sessionDict[@"service"]];
//    } else {
//        _service = [Service new];
//    }
//    
//    if (sessionDict[@"dateEnding"]) {
//        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//        dateFormatter.dateFormat = kDateFormat;
//        NSDate *endingDate = [dateFormatter dateFromString:sessionDict[@"dateEnding"]];
//        dateFormatter.dateStyle = NSDateFormatterShortStyle;
//        _dateEnding = [dateFormatter stringFromDate:endingDate];
//        NSLog(@"date ending log:%@", _dateEnding);
//    } else {
//        NSDateFormatter *formatter = [NSDateFormatter new];
//        _dateEnding = [formatter stringFromDate:[NSDate date]];
//    }
//    
//    if (sessionDict[@"sessionFiles"]) {
//        _sessionFile = [[SessionFile alloc] initWithDictionary:sessionDict[@"sessionFiles"]];
//    } else {
//        _sessionFile = [SessionFile new];
//    }
//    NSLog(@"sessionDict[pca]:%@", sessionDict[@"pca"]);
//    if (sessionDict[@"pca"] != [NSNull null]) {
//        
//        _pca = [[PCA alloc] initWithDictionary:sessionDict[@"pca"]];
//    }
//    NSLog(@"_pca:%@", _pca);
//    if (sessionDict[@"timesheets"]) {
//        //                NSLog(@"timesheets:%@", dict[@"timesheets"]);
//        NSMutableArray *mutArr = [NSMutableArray new];
//        NSMutableArray *allTimesheetsMutArray = [NSMutableArray new];
//        NSArray *timesheetsArray = sessionDict[@"timesheets"];
////        NSLog(@"timesheetsArray:%@", timesheetsArray);
//        if (timesheetsArray.count == 1) {
//            _currentTimecard = [[Timecard alloc] initWithDictionary:timesheetsArray[0]];
//        } else {
//            for (NSDictionary *timecardDict in timesheetsArray) {
//                //            NSLog(@"currentID:%@, [id]:%@", _currentTimesheetID, timecardDict[@"id"]);
//                if ([_currentTimesheetID isEqualToNumber:[timecardDict objectForKey:@"id"]]) {
//                    //                        NSLog(@"timecardDict:%@", timecardDict);
//                    _currentTimecard = [[Timecard alloc] initWithDictionary:timecardDict];
//                    break;
//                } else if ([_currentTimesheetID isEqualToNumber:@0] && userType == UserType_IsRecipient) {
//                    _currentTimecard = [[Timecard alloc] initWithDictionary:timecardDict];
//                    break;
//                } else {
//                    _currentTimecard = [Timecard new];
//                }
//                Timecard *tc = [[Timecard alloc] initWithDictionary:timecardDict];
//                [allTimesheetsMutArray addObject:tc];
//            }
//        }
//        for (NSDictionary *timecardDict in timesheetsArray) {
//            [mutArr addObject:timecardDict[@"id"]];
//        }
//        _timesheetIDsArray = mutArr;
//        _recipientTimesheetsArray = allTimesheetsMutArray;
//        
//    } else {
//        _currentTimecard = [Timecard new];
//        _recipientTimesheetsArray = [NSMutableArray new];
//    }
//}
//
//-(void)resetLocalValues {
//    self.currentTimecard = [Timecard new];
//    self.service = [Service new];
//    self.continueTimesheetNumber = @1;
//    self.dateEnding = nil;
//    self.ratio = [Ratio new];
//    self.timeIn = [TimeIn new];
//    self.timeOut = [TimeOut new];
//    self.sessionFile = [SessionFile new];
//    self.pdfState = PDFState_Default;
//    self.pca = [PCA new];
////    self.pca = ((Session *)[User currentUser].sessions[0]).pca;
//}

-(id)copyWithZone:(NSZone *)zone {
    Session *copy = [[Session allocWithZone:zone] init];
    copy.idNumber = self.idNumber;
    copy.currentTimesheetNumber = self.currentTimesheetNumber;
    copy.service = self.service;
    copy.continueTimesheetNumber = self.continueTimesheetNumber;
    copy.dateEnding = self.dateEnding;
    copy.datesInHospital = self.datesInHospital;
    copy.currentTimesheetID = _currentTimesheetID;
    copy.verificationID = _verificationID;
    copy.timesheetIDsArray = _timesheetIDsArray;
    copy.verificationIsRequiredForWorkerAndRecipient = _verificationIsRequiredForWorkerAndRecipient;
    copy.fileURL = _fileURL;
    copy.selectedRecipients = _selectedRecipients;
    copy.pca = _pca;
    return copy;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self == [super init]) {
        self.idNumber = [aDecoder decodeObjectForKey:@"idNumber"];
        self.currentTimesheetNumber = [aDecoder decodeObjectForKey:@"currentTimesheetNumber"];
        self.service = [NSKeyedUnarchiver unarchiveObjectWithData:[aDecoder decodeObjectForKey:@"service"]];
        self.continueTimesheetNumber = [aDecoder decodeObjectForKey:@"continueTimesheetNumber"];
        self.dateEnding = [aDecoder decodeObjectForKey:@"dateEnding"];
        self.datesInHospital = [aDecoder decodeObjectForKey:@"datesInHospital"];
        self.currentTimesheetID = [aDecoder decodeObjectForKey:@"currentTimesheetID"];
        self.verificationID = [aDecoder decodeObjectForKey:@"verificationID"];
        self.timesheetIDsArray = [NSKeyedUnarchiver unarchiveObjectWithData:[aDecoder decodeObjectForKey:@"timesheetIDsArray"]];
        self.verificationIsRequiredForWorkerAndRecipient = [aDecoder decodeObjectForKey:@"verificationIsRequiredForWorkerAndRecipient"];
        self.fileURL = [aDecoder decodeObjectForKey:@"fileURL"];
        self.selectedRecipients = [NSKeyedUnarchiver unarchiveObjectWithData:[aDecoder decodeObjectForKey:@"selectedRecipients"]];
        self.pca = [NSKeyedUnarchiver unarchiveObjectWithData:[aDecoder decodeObjectForKey:@"sessionpca"]];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.idNumber forKey:@"idNumber"];
    [aCoder encodeObject:self.currentTimesheetNumber forKey:@"currentTimesheetNumber"];
    [aCoder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:self.service] forKey:@"service"];
    [aCoder encodeObject:self.continueTimesheetNumber forKey:@"continueTimesheetNumber"];
    [aCoder encodeObject:self.dateEnding forKey:@"dateEnding"];
    [aCoder encodeObject:self.datesInHospital forKey:@"datesInHospital"];
    [aCoder encodeObject:self.currentTimesheetID forKey:@"currentTimesheetID"];
    [aCoder encodeObject:self.verificationID forKey:@"verificationID"];
    [aCoder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:self.timesheetIDsArray] forKey:@"timesheetIDsArray"];
    [aCoder encodeObject:self.verificationIsRequiredForWorkerAndRecipient forKey:@"verificationIsRequiredForWorkerAndRecipient"];
    [aCoder encodeObject:self.fileURL forKey:@"fileURL"];
    [aCoder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:self.selectedRecipients] forKey:@"selectedRecipients"];
    [aCoder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:self.pca] forKey:@"sessionpca"];
}

-(NSString *)description {
    return [NSString stringWithFormat:@"Session ID: %@, Service: %@, ContinueTimesheetNumber: %@, Date Ending: %@, VerificationID:%@, VerificationIsRequired:%@, fileURL:%@, selectedRecipients:%@, timesheetIDsArray:%@, pca:%@", self.idNumber, self.service, self.continueTimesheetNumber, self.dateEnding, _verificationID,  _verificationIsRequiredForWorkerAndRecipient, _fileURL, self.selectedRecipients, self.timesheetIDsArray, _pca];
}


@end
