//
//  ViewProfileTableViewController.h
//  OnDemand Timesheets
//
//  Created by Joey Kjornes on 5/22/17.
//  Copyright © 2017 Jed Mahonis Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PCA.h"

@interface ViewProfileTableViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource, UITextViewDelegate>

@property (nonatomic, strong) PCA *pca;
//@property (nonatomic, strong) NSArray *qualificationsArray;
//@property (nonatomic, strong) NSString *pcaNameString;
//@property (nonatomic, strong) NSString *pcaImageUrlString;
//@property (nonatomic, strong) NSString *pcaGenderString;
//@property (nonatomic, strong) NSString *yearsOfExperienceString;
@property (weak, nonatomic) IBOutlet UIImageView *pcaImage;
@property (weak, nonatomic) IBOutlet UILabel *pcaNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *pcaGenderLabel;
@property (weak, nonatomic) IBOutlet UILabel *pcaYOELabel;
@property (weak, nonatomic) IBOutlet UITextView *pcaQualificationsTextView;

@property (nonatomic, weak) IBOutlet UILabel *aboutPCALabel;
@property (nonatomic, weak) IBOutlet UITextView *aboutPCATextView;

@end
