//
//  Counties.h
//  OnDemand Timesheets
//
//  Created by Joey Kjornes on 3/17/17.
//  Copyright © 2017 Jed Mahonis Group. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Counties : NSObject <NSCoding>

@property NSString *countyName;
@property NSNumber *countyID;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;



@end
