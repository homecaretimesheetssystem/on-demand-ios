//
//  RSVP.m
//  Homecare Timesheet Demo
//
//  Created by Joey Kjornes on 2/16/17.
//  Copyright © 2017 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "RSVP.h"

@implementation RSVP

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
    
  //  NSLog(@"dictin ary in rsvp %@", dictionary);
    if (self == [super init]) {
        if (dictionary[@"id"]) {
        _RSVPID = dictionary[@"id"];
        }else{
            _RSVPID = 0;
            
        }
        if (dictionary[@"accepted"]) {

        _accepted = dictionary[@"accepted"];
        }else{
            _accepted = 0;
        }
        if (dictionary[@"declined"]) {
            
        _declined = dictionary[@"declined"];
        }else{
            _declined = 0;
        }
        if (dictionary[@"eta"]) {
            
        
        _eta = dictionary[@"eta"];
            
        }else{
            _eta = @" ";
                    }
        _pca = [[PCA alloc] initWithDictionary:dictionary[@"pca"]];
        
        
        if (dictionary[@"session_data_id"] && dictionary[@"session_data_id"] != [NSNull null]) {
            self.sessionDataID = dictionary[@"session_data_id"];
        }else{
            self.sessionDataID = @"";
        }
    }
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self == [super init]) {
        
        _accepted = [aDecoder decodeObjectForKey:@"accepted"];
        _declined = [aDecoder decodeObjectForKey:@"declined"];
        _RSVPID = [aDecoder decodeObjectForKey:@"id"];
        _eta = [aDecoder decodeObjectForKey:@"eta"];
        _pca = [aDecoder decodeObjectForKey:@"pca"];
        _sessionDataID = [aDecoder decodeObjectForKey:@"session_data_id"];
        
        
        
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:_accepted forKey:@"accepted"];
    [aCoder encodeObject:_declined forKey:@"declined"];
    [aCoder encodeObject:_RSVPID forKey:@"id"];
    [aCoder encodeObject:_eta forKey:@"eta"];
    [aCoder encodeObject:_pca forKey:@"pca"];
    [aCoder encodeObject:_sessionDataID forKey:@"session_data_id"];
    
}

-(NSString *)description {
    return [NSString stringWithFormat:@"ID:%@, Accepted:%@, Declined:%@, ETA:%@\r   PCA:%@\r  SessionDataID:%@", _RSVPID, _accepted, _declined, _eta, _pca, _sessionDataID];
}

@end
