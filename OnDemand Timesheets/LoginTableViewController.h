//
//  LoginTableViewController.h
//  OnDemand Timesheets
//
//  Created by Joey Kjornes on 2/27/17.
//  Copyright © 2017 Jed Mahonis Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetworkingHelper.h"
#import "Constants.h"

@interface LoginTableViewController : UITableViewController <UITextViewDelegate>

@property(nonatomic, strong) NSString *usernameString;
@property (nonatomic, strong) NSString *passwordString;

@property (nonatomic, weak) IBOutlet UITextField *usernameTextfield;
@property (nonatomic, weak) IBOutlet UITextField *passwordTextfield;
@property (nonatomic, weak) IBOutlet UIButton *loginButton;
@property (nonatomic , weak) IBOutlet UIToolbar *keyboardToolbar;

@end
