//
//  PCA.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 1/22/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "PCA.h"
#import "Constants.h"
#import "Service.h"
#import "Counties.h"
#import "Qualifications.h"
#import "Request.h"

@implementation PCA

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self == [super init]) {
        NSLog(@"dictionary:%@", dictionary);
        
        
        if ([dictionary objectForKey:@"id"] != [NSNull null]) {
            _idNumber = dictionary[@"id"];
        }else{
            _idNumber = [NSNumber numberWithLong:0];
        }
        
        
        if ([dictionary objectForKey:@"home_address"] != [NSNull null]) {
            _homeAddress = dictionary[@"home_address"];
        }else{
            _homeAddress = @"";
        }
        
        if (dictionary[@"first_name"]) {
            _firstName = [dictionary[@"first_name"] capitalizedStringWithLocale:[NSLocale localeWithLocaleIdentifier:@"en-US"]];
        } else {
            _firstName = @"";
        }
        if (dictionary[@"last_name"]) {
            _lastName = [dictionary[@"last_name"] capitalizedStringWithLocale:[NSLocale localeWithLocaleIdentifier:@"en-US"]];
        } else {
            _lastName = @"";
        }
//        _firstName = dictionary[@"first_name"];
//        _lastName = dictionary[@"last_name"];
        if (dictionary[@"umpi"]) {
            _umpi = [dictionary[@"umpi"] uppercaseString];
        } else {
            _umpi = @"";
        }
        
        if ([dictionary objectForKey:@"available"] != [NSNull null]) {
            _isAvailable = dictionary[@"available"];
        }else{
            _isAvailable = [NSNumber numberWithLong:0];
        }

        _gender = [dictionary[@"gender"]intValue];
  
        
        if ([dictionary objectForKey:@"counties"] != [NSNull null]) {
            NSArray *countArray = [dictionary valueForKey:@"counties"];
            NSMutableArray *mutCountyArray = [NSMutableArray new];
            for (NSDictionary *dict in countArray) {
                Counties *counties = [[Counties alloc] initWithDictionary:dict];
                [mutCountyArray addObject:counties];
            }
            _countyArray = [mutCountyArray copy];
        }
        if ([dictionary objectForKey:@"qualifications"] != [NSNull null]) {

            NSArray *qualifArray = [dictionary valueForKey:@"qualifications"];
            NSMutableArray *mutQArray = [NSMutableArray new];
            for (NSDictionary *dict in qualifArray) {
                Qualifications *qualifications = [[Qualifications alloc] initWithDictionary:dict];
                [mutQArray addObject:qualifications];
            }
            _qualificationsArray = [mutQArray copy];
        }
        
        
        if ([dictionary objectForKey:@"randomSessionData"] != [NSNull null]) {
            _randomSessionData = dictionary[@"randomSessionData"];
        }else{
            
        }
        
        if ([dictionary objectForKey:@"profilePhoto"] != [NSNull null]) {
            _profileImageURL = dictionary[@"profilePhoto"];

        }else{
            _profileImageURL = @"";
        }
        
        
        if (dictionary[@"years_of_experience"] && dictionary[@"years_of_experience"] != [NSNull null]) {
        _yearsOfExperience = [NSString stringWithFormat:@"%@", dictionary[@"years_of_experience"]];
        }else{
            _yearsOfExperience = @"";
        }
        if (dictionary[@"company_assigned_id"] && ![dictionary[@"company_assigned_id"] isEqual:[NSNull null]]) {
            _companyAssignedID = dictionary[@"company_assigned_id"];
        }
        NSArray *servArr = [dictionary valueForKey:@"services"];
        NSMutableArray *mutArr = [NSMutableArray new];
        for (NSDictionary *dict in servArr) {
            Service *service = [[Service alloc] initWithDictionary:dict];
            [mutArr addObject:service];
        }
        _servicesArray = [mutArr mutableCopy];
        if (dictionary[@"about_me"] != [NSNull null]) {
            _aboutMe = dictionary[@"about_me"];
        } else {
            _aboutMe = @"";
        }
        if (dictionary[@"phoneNumber"] != [NSNull null]) {
            _phoneNumber = dictionary[@"phoneNumber"];
        } else {
            _phoneNumber = @"";
        }
        if (dictionary[@"email"] != [NSNull null]) {
            _email = dictionary[@"email"];
        } else {
            _email = @"";
        }
        _directRequests = [NSMutableArray new];
        if (dictionary[@"direct_requests"] != [NSNull null]) {
            if ([dictionary[@"direct_requests"] isKindOfClass:[NSArray class]]) {
                for (NSDictionary *request in dictionary[@"direct_requests"]) {
                    [_directRequests addObject:[[Request alloc] initWithDictionary:request]];
                }
            }
        }
    }
    return self;
}

-(id)copyWithZone:(NSZone *)zone {
    PCA *copy = [[PCA allocWithZone:zone] init];
    copy.idNumber = _idNumber;
    copy.homeAddress = _homeAddress;
    copy.firstName = _firstName;
    copy.lastName = _lastName;
    copy.umpi = _umpi;
    copy.randomSessionData = _randomSessionData;
    copy.companyAssignedID = _companyAssignedID;
    copy.servicesArray = _servicesArray;
    copy.isAvailable = self.isAvailable;
    copy.gender = _gender;
    copy.profileImageURL = _profileImageURL;
    copy.hasFilledOutProfile = _hasFilledOutProfile;
    copy.cprCertified = _cprCertified;
    copy.experienceWithBehaviors = _experienceWithBehaviors;
    copy.experienceWithChildren = _experienceWithChildren;
    copy.experienceMakingTransfers = _experienceMakingTransfers;
    copy.hasDriversLicense = _hasDriversLicense;
    copy.yearsOfExperience = _yearsOfExperience;
    copy.countyArray = _countyArray;
    copy.qualificationsArray = _qualificationsArray;
    copy.profileImageURL = _profileImageURL;
    copy.available = _available;
    copy.aboutMe = _aboutMe;
    copy.directRequests = _directRequests;
    copy.phoneNumber = _phoneNumber;
    copy.email = _email;
    return copy;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self == [super init]) {
        _idNumber = [aDecoder decodeObjectForKey:@"idNumber"];
        _firstName = [aDecoder decodeObjectForKey:@"firstName"];
        _homeAddress = [aDecoder decodeObjectForKey:@"home_address"];
        _lastName = [aDecoder decodeObjectForKey:@"lastName"];
        _umpi =[aDecoder decodeObjectForKey:@"umpi"];
        _randomSessionData = [aDecoder decodeObjectForKey:@"randomSessionData"];
        _companyAssignedID = [aDecoder decodeObjectForKey:@"companyAssignedID"];
        _servicesArray = [NSKeyedUnarchiver unarchiveObjectWithData:[aDecoder decodeObjectForKey:@"services"]];
        _isAvailable = [aDecoder decodeObjectForKey:@"available"];
        _gender = [aDecoder decodeIntegerForKey:@"gender"];
        _profileImageURL = [aDecoder decodeObjectForKey:@"profilePhoto"];
        _hasFilledOutProfile = [aDecoder decodeBoolForKey:@"hasFilledOutProfile"];
        _cprCertified = [aDecoder decodeBoolForKey:@"cprCertified"];
        _experienceWithBehaviors = [aDecoder decodeBoolForKey:@"experienceWithBehaviors"];
        _experienceWithChildren = [aDecoder decodeBoolForKey:@"experienceWithChildren"];
        _experienceMakingTransfers = [aDecoder decodeBoolForKey:@"experienceMakingTransfers"];
        _hasDriversLicense = [aDecoder decodeBoolForKey:@"hasDriversLicense"];
        _yearsOfExperience = [aDecoder decodeObjectForKey:@"years_of_experience"];
        _countyArray = [aDecoder decodeObjectForKey:@"counties"];
        _qualificationsArray = [aDecoder decodeObjectForKey:@"qualifications"];
        _aboutMe = [aDecoder decodeObjectForKey:@"about_me"];
        _directRequests = [aDecoder decodeObjectForKey:@"direct_request_ids"];
        _email = [aDecoder decodeObjectForKey:@"email"];
        _phoneNumber = [aDecoder decodeObjectForKey:@"phoneNumber"];
     //   _available = [aDecoder decodeObjectForKey:@"available"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_firstName forKey:@"firstName"];
    [aCoder encodeObject:_idNumber forKey:@"idNumber"];
    [aCoder encodeObject:_homeAddress forKey:@"home_address"];
    [aCoder encodeObject:_lastName forKey:@"lastName"];
    [aCoder encodeObject:_umpi forKey:@"umpi"];
    [aCoder encodeObject:_randomSessionData forKey:@"randomSessionData"];
    [aCoder encodeObject:_companyAssignedID forKey:@"companyAssignedID"];
    [aCoder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:_servicesArray] forKey:@"services"];
    [aCoder encodeObject:self.isAvailable forKey:@"available"];
    [aCoder encodeInteger:_gender forKey:@"gender"];
    [aCoder encodeObject:_profileImageURL forKey:@"profilePhoto"];
    [aCoder encodeBool:_hasFilledOutProfile forKey:@"hasFilledOutProfile"];
    [aCoder encodeBool:_cprCertified forKey:@"cprCertified"];
    [aCoder encodeBool:_experienceWithBehaviors forKey:@"experienceWithBehaviors"];
    [aCoder encodeBool:_experienceWithChildren forKey:@"experienceWithChildren"];
    [aCoder encodeBool:_experienceMakingTransfers forKey:@"experienceMakingTransfers"];
    [aCoder encodeBool:_hasDriversLicense forKey:@"hasDriversLicense"];
    [aCoder encodeObject:_yearsOfExperience forKey:@"years_of_experience"];
    [aCoder encodeObject:_countyArray forKey:@"counties"];
    [aCoder encodeObject:_qualificationsArray forKey:@"qualifications"];
    [aCoder encodeObject:_aboutMe forKey:@"about_me"];
    [aCoder encodeObject:_directRequests forKey:@"direct_request_ids"];
    [aCoder encodeObject:_email forKey:@"email"];
    [aCoder encodeObject:_phoneNumber forKey:@"phoneNumber"];
 //   [aCoder encodeObject:_available forKey:@"available"];
}

-(NSString *)description {
    return [NSString stringWithFormat:@"{\r   PCA - idNumber:%@\r firstName:%@\r lastName:%@\r umpi:%@\r companyAssignedID:%@\r email:%@\r phone:%@\r randomSessionData:%@\r  IsAvailable:%@\r  _profileImageURL:%@\r _hasFilledOutProfile:%d\r _cprCertified:%d\r _experienceWithBehaviors:%d\r _experienceWithChildren:%d\r experienceMakingTransfers:%d\r _hasDriversLicense:%d\r _yearsOfExperience:%@\r HomeAddress:%@\r CountyArray:%@\r QualificationArray:%@\r ProfileImageURL:%@\r  Available:%d\r GENDER:%lul\r AboutMe:%@\r DirectRequests:%@\r} ", _idNumber, _firstName, _lastName, _umpi, _companyAssignedID, _email, _phoneNumber, _randomSessionData, _isAvailable, _profileImageURL, _hasFilledOutProfile, _cprCertified, _experienceWithBehaviors, _experienceWithChildren, _experienceMakingTransfers, _hasDriversLicense, _yearsOfExperience, _homeAddress, _countyArray, _qualificationsArray, _profileImageURL, _available, (unsigned long)_gender, _aboutMe, _directRequests];
}

-(NSString *)stringForGender {
    if (_gender == Gender_NotSpecified) {
        return @"Choose Gender";
    } else if (_gender == Gender_Male) {
        return @"Male";
    } else {
        return @"Female";
    }
}
@end
