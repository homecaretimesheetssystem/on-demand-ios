//
//  AppDelegate.m
//  OnDemand Timesheets
//
//  Created by Joey Kjornes on 2/27/17.
//  Copyright © 2017 Jed Mahonis Group. All rights reserved.
//

#import "AppDelegate.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "LoginTableViewController.h"
//#import "PushNotificationView.h"
@import Firebase;
@import FirebaseMessaging;
#import "AppDelegate.h"
#import "Firebase.h"
@import GooglePlaces;
@import GoogleMapsBase;
@import GooglePlacePicker;
@import GoogleMaps;
#import <Google/Analytics.h>
#import "PushNotification.h"
#import "User.h"
#import "NetworkingHelper.h"
#import "DirectRequestDetailViewController.h"

@interface AppDelegate (){
    
    
    NSString *InstanceID;
}
@property (nonatomic, strong) NSString *strUUID;
@property (nonatomic, strong) NSString *strDeviceToken;


@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
        
    [FIRApp configure];
//    Previous GMSPlacesClient in Firebase P-List:
//    AIzaSyDKkxgAUhiL7eNe5zE7oVe4uV-CB5Q-9CY
//    
//    Previous GMSPlacesClient in App Delegate:
//    AIzaSyDjTC4BOnCDwh2PRNi0bRhrispeQIG_7p4
//    [GMSPlacesClient provideAPIKey:@"AIzaSyC2woTXxF_AXsEaW9xCqZPCJa7RwYGKgwk"];
    [GMSPlacesClient provideAPIKey:@"AIzaSyDUZQzqnze1RNLlhy0MpDzfnCT8IXckkSw"];
    [GMSServices provideAPIKey:@"AIzaSyDjTC4BOnCDwh2PRNi0bRhrispeQIG_7p4"];
    
    
    // Configure tracker from GoogleService-Info.plist.
    NSError *configureError;
    [[GGLContext sharedInstance] configureWithError:&configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    // Optional: configure GAI options.
    GAI *gai = [GAI sharedInstance];
    gai.trackUncaughtExceptions = YES;  // report uncaught exceptions
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:) name:kFIRInstanceIDTokenRefreshNotification object:nil];
    

    [FIRMessaging messaging].remoteMessageDelegate = self;
   
    
    
    _navCont = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"mainNavCont"];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.window setRootViewController:_navCont];
    [self.window setBackgroundColor:[UIColor whiteColor]];
    [self.window makeKeyAndVisible];
    
    //    NSMutableDictionary *userIfno = @{@"message":@"Push Notification Test for seeing how it looks so I can get this view built"}.mutableCopy;
    //    [self showPushNotificationViewWithUserInfo:userIfno.copy];
    

    return YES;
}

-(UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (_shouldRotate) {
            return UIInterfaceOrientationMaskAll;
        }
        return UIInterfaceOrientationMaskPortrait;
    }
    else
    {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)showLoginView
{
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to didreinactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    //  NSLog(@"background");
    //    UINavigationController *navCont = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"mainNavCont"];
    NSLog(@"nav:%@", _navCont);
    [_navCont popToRootViewControllerAnimated:NO];
    //    application.keyWindow.rootViewController = _navCont;
    //    [self.window setRootViewController:_navCont];
    
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    /*
     if ([[NSUserDefaults standardUserDefaults] boolForKey:@"locRequest"] == YES) {
     [[NSNotificationCenter defaultCenter] postNotificationName:@"askLocationPermission" object:nil];
     }
     */
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"deviceToken"];
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)registerForRemoteNotification {
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0) {
        [self ios10RegisterForNotificationHandling];
    } else {
        UIUserNotificationType types = UIUserNotificationTypeSound | UIUserNotificationTypeBadge | UIUserNotificationTypeAlert;
        UIUserNotificationSettings *notificationSettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
    }
}


- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
}

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    //  if (![[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"]) {
    
    NSLog(@"didRegisterForRemoteNotificationsSuccess");
    NSString *deviceTokenString = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    deviceTokenString = [deviceTokenString stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"deviceTokenString:%@", deviceTokenString);
    [[NSUserDefaults standardUserDefaults] setObject:deviceTokenString forKey:@"pushToken"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UserAcceptedApplePushPrompt" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UserAcceptedApplePushPromptNotifications" object:nil];
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    
    [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:kDebug ? FIRInstanceIDAPNSTokenTypeSandbox : FIRInstanceIDAPNSTokenTypeProd];
    NSLog(@"device firebase token = %@",refreshedToken);
    //   NSLog(@"InstanceID token: %@", refreshedToken);
    
    
    
    
    //  }else{
    if (refreshedToken) {
        [self saveRefreshedTokenAndSendToServer:refreshedToken];
    } else {
        NSLog(@"no refreshToken in app delegate");
    }
    
    
    //   }
    
}

-(void)saveRefreshedTokenAndSendToServer:(NSString *)refreshedToken {
    if (User.currentUser.idNumber) {
        [[NSUserDefaults standardUserDefaults] setObject:refreshedToken forKey:@"deviceToken1"];
        [NetworkingHelper POSTDeviceToken:NO andIsIOS:YES andIsActive:YES andDeviceToken:refreshedToken andUserID:User.currentUser.idNumber andSuccess:^(id responseObject) {
            NSLog(@"success sending device token in app delegate %@", responseObject);
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            NSLog(@"error in cont %@", error);
            
        }];
    }
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"error registering for remote notifications:%@", error);
}


-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSLog(@"Got remote notification userInfo :%@", userInfo);
    [self application:application didReceiveRemoteNotification:userInfo fetchCompletionHandler:^(UIBackgroundFetchResult result) {
        
    }];
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    NSLog(@"Message ID: %@", userInfo[@"gcm.message_id"]);
    // [[ messaging] appDidReceiveMessage:userInfo];
    NSLog(@"Got remote notification fetchCompletionHandler %@", userInfo);
    // TODO: handle notification and call handler with real result
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0) {
        //            return;
    }
    [self handleNotificationWithUserInfo:userInfo];
    completionHandler(UIBackgroundFetchResultNewData);
    
    //  NSLog(@"userInfo=>%@", userInfo);
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateInactive || state == UIApplicationStateBackground) {
        // go to screen relevant to Notification content
    } else {
        // App is in UIApplicationStateActive (running in foreground)
        // perhaps show an UIAlertView
        if ([userInfo[@"push_type"] isEqualToString:@"1"]) {
            //new shift available
            [[NSNotificationCenter defaultCenter] postNotificationName:@"push_type=1" object:self];
        }
        if ([userInfo[@"push_type"] isEqualToString:@"2"]) {
            //recipients shift has been accepted by a pca
            [[NSNotificationCenter defaultCenter] postNotificationName:@"push_type=2" object:self];
        }
        if ([userInfo[@"push_type"] isEqualToString:@"3"]) {
            //recipient cancelled the shift, notify pca who accepted
            [[NSNotificationCenter defaultCenter] postNotificationName:@"push_type=3" object:self];
        }
        if ([userInfo[@"push_type"] isEqualToString:@"4"]) {
            //worker canled request
            [[NSNotificationCenter defaultCenter] postNotificationName:@"push_type=4" object:self];
            
        }
    }
    
    
    
}
-(void)showPushNotificationViewWithUserInfo:(NSDictionary *)userInfo {
    PushNotificationView *pushNotificationView = [[PushNotificationView alloc] initWithFrame:CGRectMake(12, -80, self.window.frame.size.width - 24, 80)];
    pushNotificationView.messageLabel.text = userInfo[@"aps"][@"alert"][@"body"];
    //    pushNotificationView.messageLabel.text = userInfo[@"message"];
    PushNotification *notification = [[PushNotification alloc] initWithDictionary:userInfo];
    pushNotificationView.pushNotification = notification;
    pushNotificationView.alpha = 1.0;
    [self.window addSubview:pushNotificationView];
    pushNotificationView.alpha = 0.99;
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseOut|UIViewAnimationOptionAllowUserInteraction animations:^{
        pushNotificationView.frame = CGRectMake(12, 20, self.window.frame.size.width - 24, 80);
        pushNotificationView.alpha = 1.0;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.2 delay:3.0 options:UIViewAnimationOptionCurveEaseOut|UIViewAnimationOptionAllowUserInteraction animations:^{
            pushNotificationView.frame = CGRectMake(12, -80, self.window.frame.size.width - 24, 80);
            pushNotificationView.alpha = 0.99;
        } completion:^(BOOL finished) {
            [pushNotificationView removeFromSuperview];
        }];
    }];
}

-(void)handleNotificationWithUserInfo:(NSDictionary *)userInfo {
    NSDictionary *dict = [userInfo objectForKey:@"aps"];
    int badgeCountInt = [[dict objectForKey:@"badge"] intValue];
    NSString *badgeCountString = [NSString stringWithFormat:@"%ld", (long)badgeCountInt];
    [[NSUserDefaults standardUserDefaults] setObject:badgeCountString forKey:@"badgeCount"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"notificationReceivedFromServer" object:nil];
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"receivedNotifWhileNotificaitonsControllerActive" object:nil];
        [self showPushNotificationViewWithUserInfo:userInfo];
    } else if ([UIApplication sharedApplication].applicationState == UIApplicationStateInactive || [UIApplication sharedApplication].applicationState == UIApplicationStateBackground  )
    {
        //opened from a push notification when the app was on background
    }
    
}



//- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
//
//    [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeProd];
//    NSLog(@"deviceToken1 = %@",deviceToken);
//
//}


- (void)tokenRefreshNotification:(NSNotification *)notification {
    
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", refreshedToken);
    
    NSLog(@"instanceId_notification=>%@",[notification object]);
    InstanceID = [NSString stringWithFormat:@"%@",[notification object]];
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken1"]) {
        [self saveRefreshedTokenAndSendToServer:[notification object]];
    }
    [self connectToFcm];
}

-(void)connectToFcm {
    
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Unable to connect to FCM. %@", error);
        } else {
            
//            NSLog(@"InstanceID_connectToFcm = %@", self->InstanceID);
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    //    [self sendDeviceInfo];
                    NSLog(@"instanceId_tokenRefreshNotification22=>%@",[[FIRInstanceID instanceID] token]);
                    
                });
            });
            
            
        }
    }];}

/*********************************  iOS 10 Notification Code  ************************************/

-(void)ios10RegisterForNotificationHandling {
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    center.delegate = self;
    [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
        if( !error ){
            NSLog(@"granted:%d", granted);
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        } else {
            NSLog(@"iOS10 register for notifications error:%@", error);
        }
    }];
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    
    //Called when a notification is delivered to a foreground app.
    
    //    NSLog(@"willPresentNotification Userinfo %@",notification.request.content.userInfo[@"push_type"]);
    //    NSDictionary *userInfo = notification.request.content.userInfo;
    //    if ([notification.request.content.userInfo[@"aps"][@"alert"][@"body"] containsString:@"You have received a pca request"]) {
    //        NSLog(@"push notification one");
    //        [[NSNotificationCenter defaultCenter] postNotificationName:@"push_type=1" object:nil];
    //    }
    //    if ([userInfo[@"push_type"] isEqualToValue:@2]) {
    //        [[NSNotificationCenter defaultCenter] postNotificationName:@"push_type=2" object:nil];
    //    }
    //    if ([notification.request.content.userInfo[@"aps"][@"alert"][@"body"] containsString:@"Recipient has cancelled the listing"]) {
    //        NSLog(@"notification 3");
    //        [[NSNotificationCenter defaultCenter] postNotificationName:@"push_type=3" object:nil];
    //    }
    //    if ([userInfo[@"push_type"] isEqualToValue:@4]) {
    //        [[NSNotificationCenter defaultCenter] postNotificationName:@"push_type=4" object:nil];
    //
    //    }
    NSLog(@"willPresentNotification");
    [self handleNotificationWithUserInfo:notification.request.content.userInfo];
    completionHandler(UNNotificationPresentationOptionSound);
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler{
    
    //Called to let your app know which action was selected by the user for a given notification.
    [self handleNotificationWithUserInfo:response.notification.request.content.userInfo];
    //  NSLog(@"didReceiveNotificationResponse Userinfo %@",response.notification.request.content.userInfo);
    
}

-(void)notificationBannerTouched:(id)sender withPushNotification:(PushNotification *)pushNotification {
    //    NSLog(@"touch recognized:%@", pushNotification);
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([User currentUser]) {
        [NetworkingHelper getSessionDataWithSuccess:^(id responseObject) {
            User *user = [[User alloc] initWithDictionary:responseObject];
            [user save];
            [self navigateToCorrectScreenWithPushNotification:pushNotification andUser:user];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            // Fail Silently
        }];
    }
    if (sender) {
        UIButton *button = (UIButton *)sender;
        UIView *pnv = button.superview;
        [pnv removeFromSuperview];
    }
}

-(void)navigateToCorrectScreenWithPushNotification:(PushNotification *)pushNotification andUser:(User *)user {
    NSMutableArray *viewControllersArray = @[[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"loginTableViewController"]].mutableCopy;
    
    if (pushNotification.pushType == PushType_1) {
        [viewControllersArray addObject:[[UIStoryboard storyboardWithName:@"OnDemandWorker" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeTableViewController"]];
        [viewControllersArray addObject:[[UIStoryboard storyboardWithName:@"OnDemandWorker" bundle:nil] instantiateViewControllerWithIdentifier:@"FindShiftsViewController"]];
    } else if (pushNotification.pushType == PushType_2 || pushNotification.pushType == PushType_4) {
        [viewControllersArray addObject:[[UIStoryboard storyboardWithName:@"OnDemandRecipient" bundle:nil] instantiateViewControllerWithIdentifier:@"RecipientHomeTableViewController"]];
        [viewControllersArray addObject:[[UIStoryboard storyboardWithName:@"OnDemandRecipient" bundle:nil] instantiateViewControllerWithIdentifier:@"SeeRSVPsViewController"]];
        
    } else if (pushNotification.pushType == PushType_3) {
        [viewControllersArray addObject:[[UIStoryboard storyboardWithName:@"OnDemandWorker" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeTableViewController"]];
        [viewControllersArray addObject:[[UIStoryboard storyboardWithName:@"OnDemandWorker" bundle:nil] instantiateViewControllerWithIdentifier:@"UpcomingShiftsViewController"]];
    }
    if (_navCont.viewControllers.count > 1) {
        [_navCont setViewControllers:viewControllersArray.copy animated:YES];
        if (user.pca.directRequests.count > 0) {
            DirectRequestDetailViewController *vc = [[UIStoryboard storyboardWithName:@"DirectRequestDetail" bundle:nil] instantiateViewControllerWithIdentifier:@"DirectRequestDetailViewController"];
            vc.request = user.pca.directRequests[0];
            [_navCont presentViewController:vc animated:NO completion:NULL];
        }
    }
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}


@end
