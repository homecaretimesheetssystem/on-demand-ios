//
//  PushNotificationView.h
//  Kindu
//
//  Created by Scott Mahonis on 1/15/16.
//  Copyright © 2016 The Jed Mahonis Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PushNotification.h"

@interface PushNotificationView : UIView

@property (nonatomic, weak) IBOutlet UILabel *messageLabel;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) PushNotification *pushNotification;

-(IBAction)buttonTouched:(id)sender;

@end
