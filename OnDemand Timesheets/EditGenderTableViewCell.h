//
//  EditGenderTableViewCell.h
//  OnDemand Timesheets
//
//  Created by Joey Kjornes on 3/17/17.
//  Copyright © 2017 Jed Mahonis Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
@protocol EditGenderTableViewCellDelegate <NSObject>

-(void)updateCurrentUserFromPicker:(User *)user;

@end

@interface EditGenderTableViewCell : UITableViewCell <UIPickerViewDelegate, UIPickerViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *genderPreferenceLabel;
@property (weak, nonatomic) IBOutlet UIPickerView *genderPicker;
@property (nonatomic, strong) NSArray *genderArray;
@property (nonatomic) id<EditGenderTableViewCellDelegate>delegate;

@end
