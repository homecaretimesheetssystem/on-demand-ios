//
//  Qualifications.h
//  OnDemand Timesheets
//
//  Created by Joey Kjornes on 3/21/17.
//  Copyright © 2017 Jed Mahonis Group. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Qualifications : NSObject

@property NSString *qualificationName;
@property NSNumber *qualificationID;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;


@end
