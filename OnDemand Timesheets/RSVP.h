//
//  RSVP.h
//  Homecare Timesheet Demo
//
//  Created by Joey Kjornes on 2/16/17.
//  Copyright © 2017 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PCA.h"

@interface RSVP : NSObject <NSCoding>


@property (nonatomic, strong) NSString *eta;
@property (nonatomic, strong) NSNumber *RSVPID;
@property (nonatomic,strong) NSNumber *declined;
@property (nonatomic, strong) NSNumber *accepted;
@property (nonatomic, strong) PCA *pca;

@property (nonatomic, strong) NSString *sessionDataID;


-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
