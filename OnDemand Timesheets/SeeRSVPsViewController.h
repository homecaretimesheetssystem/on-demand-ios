//
//  SeeRSVPsViewController.h
//  OnDemand Timesheets
//
//  Created by Joey Kjornes on 3/16/17.
//  Copyright © 2017 Jed Mahonis Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Request.h"
#import "RSVP.h"
#import <CoreLocation/CoreLocation.h>
#import "User.h"
#import "Shift.h"

typedef NS_ENUM(NSUInteger, UpcomingShiftStatus){
    UpcomingShiftStatus_NoUpcomingShifts,
    UpcomingShiftStatus_AtLeastOneShift,
    UpcomingShiftStatus_Loading,
    
};


@interface SeeRSVPsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) User *currentUser;
@property (nonatomic, strong) NSMutableArray *availableJobsArray;
@property (nonatomic, strong) NSMutableArray *upcomingShiftsArray;
@property (nonatomic, strong) NSMutableArray *allRequestsArray;
@property (nonatomic, strong) NSMutableArray *acceptedJobsArray;
@property (nonatomic, strong) CLLocationManager *locManager;
@property (nonatomic) UpcomingShiftStatus upcomingStatus;
@property (nonatomic, strong) Request *request;
@property (nonatomic, strong) RSVP *rsvp;
@property (nonatomic, strong) Shift *shift;


@end
