//
//  UpcomingShiftsViewController.h
//  OnDemand Timesheets
//
//  Created by Joey Kjornes on 3/16/17.
//  Copyright © 2017 Jed Mahonis Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Request.h"
#import "RSVP.h"
#import <CoreLocation/CoreLocation.h>
#import "User.h"
#import "Shift.h"
#import "UserSettingsOptionsTableViewController.h"

typedef NS_ENUM(NSUInteger, RecipientRequests){
    RecipientRequests_NoRequests,
    RecipientRequests_AtLeastOneRequest,
    
};


@interface UpcomingShiftsViewController : UIViewController  <UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate, UserSettingsOptionsTableViewControllerDelegate, UIPopoverControllerDelegate, UIPopoverPresentationControllerDelegate>


@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) User *currentUser;
@property (nonatomic, strong) NSMutableArray *availableJobsArray;
@property (nonatomic, strong) NSMutableArray *upcomingShiftsArray;
@property (nonatomic, strong) NSMutableArray *allRequestsArray;
@property (nonatomic, strong) NSMutableArray *acceptedJobsArray;
@property (nonatomic, strong) CLLocationManager *locManager;
@property (nonatomic) RecipientRequests recipientRequests;
@property (nonatomic, strong) Request *request;
@property (nonatomic, strong) RSVP *rsvp;
@property (nonatomic, strong) Shift *shift;
@property (nonatomic, strong) NSMutableArray *arrayToPass;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *settingsBarButtonItem;


@end
