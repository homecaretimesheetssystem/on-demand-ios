//
//  CountiesTableViewCell.m
//  OnDemand Timesheets
//
//  Created by Joey Kjornes on 3/17/17.
//  Copyright © 2017 Jed Mahonis Group. All rights reserved.
//

#import "CountiesTableViewCell.h"

@implementation CountiesTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
