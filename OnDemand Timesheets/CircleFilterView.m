//
//  CircleFilterView.m
//  camera
//
//  Created by Scott Mahonis on 7/24/16.
//  Copyright © 2016 JMG. All rights reserved.
//

#import "CircleFilterView.h"

@implementation CircleFilterView

- (void)drawRect:(CGRect)rect
{
    // Drawing code
    [[self fillColor] set];
    UIRectFill(rect);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetBlendMode(context, kCGBlendModeDestinationOut);
    
    for (NSValue *value in self.framesToCutOut) {
        CGRect pathRect = [value CGRectValue];
        UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:pathRect];
        [path fill];
    }
    
    CGContextSetBlendMode(context, kCGBlendModeNormal);
}

@end
