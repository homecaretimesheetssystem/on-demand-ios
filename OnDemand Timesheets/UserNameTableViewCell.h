//
//  UserNameTableViewCell.h
//  OnDemand Timesheets
//
//  Created by Joey Kjornes on 2/28/17.
//  Copyright © 2017 Jed Mahonis Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserNameTableViewCell : UITableViewCell <UITextViewDelegate>

@property (nonatomic, weak) IBOutlet UITextField *usernameTextField;

@end
