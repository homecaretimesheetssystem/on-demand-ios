//
//  Recipient.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 1/22/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "Recipient.h"

@implementation Recipient

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self == [super init]) {
        if ([dictionary objectForKey:@"id"] != [NSNull null]) {
            _idNumber = dictionary[@"id"];
        }else{
            _idNumber = [NSNumber numberWithLong:0];
        }

        if (dictionary[@"first_name"]) {
            _firstName = [dictionary[@"first_name"] capitalizedStringWithLocale:[NSLocale localeWithLocaleIdentifier:@"en-US"]];
        } else {
            _firstName = @"";
        }
        if (dictionary[@"last_name"]) {
            _lastName = [dictionary[@"last_name"] capitalizedStringWithLocale:[NSLocale localeWithLocaleIdentifier:@"en-US"]];
        } else {
            _lastName = @"";
        }
        if (dictionary[@"ma_number"]) {
            _maNumber = [dictionary[@"ma_number"] uppercaseString];
        } else {
            _maNumber = @"";
        }
        if (dictionary[@"company_assigned_id"] && ![dictionary[@"company_assigned_id"] isEqual:[NSNull null]]) {
            _companyAssignedID = dictionary[@"company_assigned_id"];
        }
        BOOL skipGPS = [dictionary[@"skip_gps"] boolValue];
        _shouldRecordLocation = [NSNumber numberWithBool:!skipGPS];
        
        BOOL skipVerification = [dictionary[@"skip_verification"] boolValue];
        _noVerificationNeeded = [NSNumber numberWithBool:!skipVerification];
        
        _genderPreference = dictionary[@"gender_preference"];

    }
    return self;
}

-(id)copyWithZone:(NSZone *)zone {
    Recipient *copy = [[Recipient allocWithZone:zone] init];
    copy.idNumber = _idNumber;
    copy.firstName = _firstName;
    copy.lastName = _lastName;
    copy.maNumber = _maNumber;
    copy.companyAssignedID = _companyAssignedID;
    copy.shouldRecordLocation = _shouldRecordLocation;
    copy.noVerificationNeeded = _noVerificationNeeded;
    copy.genderPreference = _genderPreference;
    
    return copy;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self == [super init]) {
        _idNumber = [aDecoder decodeObjectForKey:@"idNumber"];
        _firstName = [aDecoder decodeObjectForKey:@"firstName"];
        _lastName = [aDecoder decodeObjectForKey:@"lastName"];
        _maNumber =[aDecoder decodeObjectForKey:@"maNumber"];
        _companyAssignedID = [aDecoder decodeObjectForKey:@"companyAssignedID"];
        _shouldRecordLocation = [aDecoder decodeObjectForKey:@"shouldRecordLocation"];
        _noVerificationNeeded = [aDecoder decodeObjectForKey:@"skip_verification"];
        _genderPreference = [aDecoder decodeObjectForKey:@"gender_preference"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_firstName forKey:@"firstName"];
    [aCoder encodeObject:_idNumber forKey:@"idNumber"];
    [aCoder encodeObject:_lastName forKey:@"lastName"];
    [aCoder encodeObject:_maNumber forKey:@"maNumber"];
    [aCoder encodeObject:_companyAssignedID forKey:@"companyAssignedID"];
    [aCoder encodeObject:_shouldRecordLocation forKey:@"shouldRecordLocation"];
    [aCoder encodeObject:_noVerificationNeeded forKey:@"skip_verification"];
    [aCoder encodeObject:_genderPreference forKey:@"gender_preference"];
}

-(NSString *)description {
    return [NSString stringWithFormat:@"Recipient - idNumber:%@, firstName:%@, lastName:%@, maNumber:%@, companyAssignedID:%@, shouldRecordLocation:%@, genderPreference:%@, skipVerification:%@", _idNumber, _firstName, _lastName, _maNumber, _companyAssignedID, _shouldRecordLocation, _genderPreference, _noVerificationNeeded];
}

@end
