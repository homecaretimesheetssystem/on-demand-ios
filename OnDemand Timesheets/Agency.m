//
//  Agency.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 3/16/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "Agency.h"
#import "Service.h"


@implementation Agency

-(instancetype)init {
    if (self == [super init]) {
        _name = @"";
        _phone = @"";
        _IDNumber = [NSNumber numberWithLong:-1];
    }
    return self;
}

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self == [super init]) {
        
        
        if ([dictionary objectForKey:@"agency_name"] != [NSNull null]) {
            _name = [[dictionary valueForKey:@"agency_name"] uppercaseString];
        }else{
            _name = @"";
        }
        
        if ([dictionary objectForKey:@"phone_number"] != [NSNull null]) {
            _phone = [dictionary valueForKey:@"phone_number"];
        }else{
            _phone = @"";
        }
        
        if ([dictionary objectForKey:@"id"] != [NSNull null]) {
            _IDNumber = [dictionary valueForKey:@"id"];
        }else{
            _IDNumber = [NSNumber numberWithLong:-1];
        }
        
        if ([dictionary objectForKey:@"state"] != [NSNull null] && [dictionary objectForKey:@"state_name"] != [NSNull null]) {
            NSString *stateNameString = dictionary[@"state"][@"state_name"];
            _state = [stateNameString uppercaseString];
            
        }else{
            _state = @"";
        }
        
        if ([dictionary objectForKey:@"state"] != [NSNull null] && [dictionary objectForKey:@"id"] != [NSNull null]) {
            _stateIdNumber = (NSNumber*)dictionary[@"state"][@"id"];
        }else{
            _stateIdNumber = [NSNumber numberWithLong:0];
        }
        
        if ([dictionary objectForKey:@"amazonUrl"] != [NSNull null] && [dictionary objectForKey:@"image_name"] != [NSNull null]) {
            NSString *URL = [NSString stringWithFormat:@"%@%@", dictionary[@"amazonUrl"], dictionary[@"image_name"]];
            //        NSString *URL = [NSString stringWithFormat:@"https://s3.amazonaws.com/hc-timesheets-demo/%@", dictionary[@"image_name"]];
            _logoURL = [NSURL URLWithString:URL];
            
        }else{
            _logoURL = [NSURL URLWithString:@""];
        }
        
        
        if ([dictionary objectForKey:@"services"] != [NSNull null]) {
            NSArray *servArr = [dictionary valueForKey:@"services"];
            NSMutableArray *mutArr = [NSMutableArray new];
            for (NSDictionary *dict in servArr) {
                Service *service = [[Service alloc] initWithDictionary:dict];
                [mutArr addObject:service];
            }
            _servicesArray = [mutArr mutableCopy];
        }else{
            _servicesArray = [NSMutableArray new];
        }

    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    if (self == [super init]) {
        _name = [aDecoder decodeObjectForKey:@"name"];
        _phone = [aDecoder decodeObjectForKey:@"phone"];
        _IDNumber = [aDecoder decodeObjectForKey:@"idnumber"];
        _servicesArray = [NSKeyedUnarchiver unarchiveObjectWithData:[aDecoder decodeObjectForKey:@"services"]];
        _state = [aDecoder decodeObjectForKey:@"state_name"];
        _stateIdNumber = [aDecoder decodeObjectForKey:@"state_id"];
        _logoURL = [NSKeyedUnarchiver unarchiveObjectWithData:[aDecoder decodeObjectForKey:@"image_name"]];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_name forKey:@"name"];
    [aCoder encodeObject:_phone forKey:@"phone"];
    [aCoder encodeObject:_IDNumber forKey:@"idnumber"];
    [aCoder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:_servicesArray] forKey:@"services"];
    [aCoder encodeObject:_state forKey:@"state_name"];
    [aCoder encodeObject:_stateIdNumber forKey:@"state_id"];
    [aCoder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:_logoURL] forKey:@"image_name"];
}

-(id)copyWithZone:(NSZone *)zone {
    Agency *copy = [[Agency allocWithZone:zone] init];
    copy.IDNumber = _IDNumber;
    copy.name = _name;
    copy.phone = _phone;
    copy.state = _state;
    copy.stateIdNumber = _stateIdNumber;
    copy.servicesArray = _servicesArray;
    copy.logoURL = _logoURL;
    return copy;
}

-(NSString *)description {
//    NSLog(@"_servicesArray:%@", _servicesArray);
//    NSArray *services = [NSKeyedUnarchiver unarchiveObjectWithData:_servicesArray];
    return [NSString stringWithFormat:@"Agency ID:%@, Name:%@, Phone:%@, State,ID:%@,%@ Services:%@, logoURL: %@", _IDNumber, _name, _phone, _state, _stateIdNumber, _servicesArray, _logoURL];
}





@end
