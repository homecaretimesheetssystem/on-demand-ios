//
//  LoginTableViewController.m
//  OnDemand Timesheets
//
//  Created by Joey Kjornes on 2/27/17.
//  Copyright © 2017 Jed Mahonis Group. All rights reserved.
//

#import "LoginTableViewController.h"
#import "LoadingView.h"
#import "User.h"
#import "UserNameTableViewCell.h"
#import "LoginButtonTableViewCell.h"
#import "PasswordTableViewCell.h"
#import "ErrorAlertController.h"
#import "FindShiftsViewController.h"
#import "PCARequestTableViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "HomeTableViewController.h"
#import "RecipientHomeTableViewController.h"
#import "EditWorkerProfileTableViewController.h"

@interface LoginTableViewController ()

@end

@implementation LoginTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.usernameString = @"eausterman";
//    self.passwordString = @"eausterman";
//    [self loginButtonPressed:nil];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"userNameSaved"]) {
        self.usernameTextfield.text = [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"userNameSaved"]];
    }
    
#if DEBUG
    self.usernameTextfield.text = @"prunyon";
    self.passwordTextfield.text = @"Test1234";
#endif
    
    self.loginButton.layer.borderColor = [UIColor colorWithRed:55.0/255.0 green:99.0/255.0 blue:135.0/255.0 alpha:1.0].CGColor;
    self.loginButton.layer.borderWidth = 1.0f; //make border 1px thick

   // [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
   //  self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    self.passwordTextfield.text = @"";
//    self.usernameTextfield.text = @"eausterman";
//    self.passwordTextfield.text = @"Test1234";
//    [self verifyWithServer];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
}



-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];

}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 360;
    }else if (indexPath.row == 3){
        return 200;
    }
    return 50;
}


-(IBAction)loginButtonPressed:(id)sender{
    [self.view endEditing:YES];
    [self checkForCharacters];
}

-(void)verifyWithServer {
    NSDictionary *params = @{@"username":self.usernameTextfield.text, @"password":self.passwordTextfield.text};
    LoadingView *loadingView = [LoadingView addLoadingViewWithText:@"Loading"];
    [NetworkingHelper loginWithParams:params success:^(id responseObject) {
        [NetworkingHelper setNetworkingHelperOAuthToken:responseObject[@"access_token"]];
        [[NSUserDefaults standardUserDefaults] setObject:responseObject[@"expires_in"] forKey:@"credExpiration"];
        [NetworkingHelper getSessionDataWithSuccess:^(id responseObject) {
          //  NSLog(@"responseObject:%@", responseObject);
            User *user = [[User alloc] initWithDictionary:responseObject];
            [user save];
            NSLog(@"user:%@", user);
            NSString *errorNumber = responseObject[@"error"];
            NSString *errorMessage = responseObject[@"errorMessage"];
            [loadingView hideActivityIndicator];
            if ([errorNumber isEqualToString:@"2"]) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Password is incorrect. Please try again" preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                }]];
                alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
                [self presentViewController:alert animated:YES completion:NULL];
                
            } else if (![errorNumber isEqualToString:@"1"]) {
                [[NSUserDefaults standardUserDefaults] setObject:self.usernameTextfield.text forKey:@"userNameSaved"];
                
                [self nextScreenWithUser:user];
                
            } else if ([errorNumber isEqualToString:@"1"]) {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:[NSString stringWithFormat:@"Error:%@", errorMessage] preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                }]];
                alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
                [self presentViewController:alert animated:YES completion:NULL];
                
            } else if (errorNumber) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Unable to connect to the server. Please check your internet connection and try again." preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                }]];
                alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
                [self presentViewController:alert animated:YES completion:NULL];
            }
        
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            NSLog(@"failed to login because :%@", error);
            
            [loadingView hideActivityIndicator];
            id responseObject = [NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:0 error:nil];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:[NSString stringWithFormat:@"%@", responseObject[@"error"]] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.tableView reloadData];
            }]];
            
            alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
            [self presentViewController:alert animated:YES completion:NULL];
         
        }];
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        id responseObject = [NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:0 error:nil];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:[NSString stringWithFormat:@"%@", responseObject[@"error"]] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.tableView reloadData];
        }]];
        
        alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
        [self presentViewController:alert animated:YES completion:NULL];
        [loadingView hideActivityIndicator];
        
    }];
    
}



-(void)nextScreenWithUser:(User *)currentUser {
    if (currentUser.userType == UserType_IsPCA) {
//        if (currentUser.pca.countyArray.count == 0  || [currentUser.pca.profileImageURL isEqualToString:@""] || !currentUser.pca.firstName || !currentUser.pca.lastName) {
//            EditWorkerProfileTableViewController *vc = [[UIStoryboard storyboardWithName:@"OnDemandWorker" bundle:nil] instantiateViewControllerWithIdentifier:@"EditWorkerProfileTableViewController"];
//            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"stillFromLogin"];
//            HomeTableViewController *home = [[UIStoryboard storyboardWithName:@"OnDemandWorker" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeTableViewController"];
//            [self.navigationController setViewControllers:@[self, home, vc] animated:YES];
//
//
//        }else{
        HomeTableViewController *vc = [[UIStoryboard storyboardWithName:@"OnDemandWorker" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeTableViewController"];
            [self.navigationController showViewController:vc sender:self];
//        }
    }else if (currentUser.userType == UserType_IsRecipient) {
        RecipientHomeTableViewController *vc = [[UIStoryboard storyboardWithName:@"OnDemandRecipient" bundle:nil] instantiateViewControllerWithIdentifier:@"RecipientHomeTableViewController"];
        [self.navigationController showViewController:vc sender:self];

        }
}




#pragma mark - UITextfield Delegate


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    textField.inputAccessoryView = _keyboardToolbar;
    for (UIBarButtonItem *item in _keyboardToolbar.items) {
        item.tag = textField.tag;
    }
    return YES;
}

-(IBAction)nextTextField:(UIBarButtonItem *)sender {
    long tag = sender.tag;
    id nextView = [self.view viewWithTag:tag + 1];
    if (nextView && [nextView respondsToSelector:@selector(becomeFirstResponder)]) {
        [nextView becomeFirstResponder];
    } else {
        [self dismissKeyboard:sender];
    }
}

-(IBAction)prevTextField:(UIBarButtonItem *)sender {
    long tag = sender.tag;
    id previousView = [self.view viewWithTag:tag - 1];
    if (previousView && [previousView respondsToSelector:@selector(becomeFirstResponder)]) {
        [previousView becomeFirstResponder];
    } else {
        [self dismissKeyboard:sender];
    }
}

-(IBAction)dismissKeyboard:(UIBarButtonItem *)sender {
    [self.view endEditing:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    UIBarButtonItem *item = [UIBarButtonItem new];
    item.tag = textField.tag;
    [self nextTextField:item];
    return YES;
}




-(void)endEditing {
    [self.tableView setContentOffset:CGPointMake(0, 20.)];
    [self.view endEditing:YES];
}

-(UIToolbar *)textFieldAccessoryToolbar:(UITextField *)textField {
    
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 44)];
    toolbar.tintColor = [UIColor darkGrayColor];
    UIButton *previousButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [previousButton addTarget:self action:@selector(prevTextField:) forControlEvents:UIControlEventTouchUpInside];
    previousButton.tag = textField.tag;
    [previousButton setBackgroundImage:[UIImage imageNamed:@"back_chevron"] forState:UIControlStateNormal];
    previousButton.frame = CGRectMake(0, 0, 32, 32);
    UIBarButtonItem *prevBBItem = [[UIBarButtonItem alloc] initWithCustomView:previousButton];
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [nextButton addTarget:self action:@selector(nextTextField:) forControlEvents:UIControlEventTouchUpInside];
    [nextButton setBackgroundImage:[UIImage imageNamed:@"chevron"] forState:UIControlStateNormal];
    nextButton.tag = textField.tag;
    nextButton.frame = CGRectMake(0, 0, 32, 32);
    UIBarButtonItem *nextBBItem = [[UIBarButtonItem alloc] initWithCustomView:nextButton];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *fixedSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedSpace.width = -6;
    toolbar.items = @[prevBBItem, nextBBItem, flexibleSpace];
    return toolbar;
}



- (BOOL) containsSymbols: (NSString *) candidate {
    NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"_@.+abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789"] invertedSet];
    
    if ([candidate rangeOfCharacterFromSet:set].location != NSNotFound) {
        return YES;
    }
    return NO;
}

-(BOOL) containsWhitespace: (NSString *) candidate {
    NSRange whiteSpaceRange = [candidate rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
    if (whiteSpaceRange.location != NSNotFound) {
        return YES;
    }
    return NO;
}

-(void)checkForCharacters{
    if ([_usernameTextfield.text isEqualToString:@""]) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"You need to type in your Username" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
        }];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
    }else if ([_passwordTextfield.text isEqualToString:@""]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"You need to type in your password" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
        }];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
    }else if (![_usernameTextfield.text isEqualToString:@""] && ![_passwordTextfield.text isEqualToString:@""]){
        [self verifyWithServer];
    }
}

-(void)userNameErrorWithTextField:(UITextField *)textField {
    [self.tableView reloadData];
    [textField becomeFirstResponder];
    _loginButton.enabled = NO;
    _loginButton.backgroundColor = [UIColor colorWithWhite:0.65 alpha:1.0];
}



@end
