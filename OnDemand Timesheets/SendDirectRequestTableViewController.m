//
//  SendDirectRequestTableViewController.m
//  OnDemand Timesheets
//
//  Created by Scott Mahonis on 3/30/18.
//  Copyright © 2018 Jed Mahonis Group. All rights reserved.
//

#import "SendDirectRequestTableViewController.h"
#import "ShiftDetailViewController.h"
#import "ShiftDetailTableViewCell.h"
#import "AcceptButtonTableViewCell.h"
#import "AcceptedShiftViewController.h"
#import "StartTimeCellTableViewCell.h"
#import "EndTimeTableViewCell.h"
#import "QualificationsTableViewCell.h"
#import <AFNetworking/AFNetworking.h>
#import <AFOAuth2Manager/AFOAuth2Manager.h>
#import "GenderPreferenceTableViewCell.h"
#import "NetworkingHelper.h"
#import "LoadingView.h"
#import "Qualifications.h"
#import "RecipientHomeTableViewController.h"
#import "CountiesTableViewCell.h"

@interface SendDirectRequestTableViewController ()

@end

@implementation SendDirectRequestTableViewController{
    
    BOOL hideStartTimePicker;
    BOOL hideEndTimePicker;
    BOOL hideGenderPicker;
    BOOL blankCell1;
    BOOL blankCell2;
    BOOL isSelected;
    NSDate *inTimeDateString;
    NSDate *outTimeDateString;
    NSString *shiftStartTime;
    NSString *shiftEndTime;
    NSInteger genderPreferenceString;
    NSMutableArray *qualificationsArrayRecip;
    BOOL loadingRecipQuals;
    NSString *pickerintimelabel;
    NSString *pickerouttimelabel;
    BOOL localPickerChangedBOOL;
    float estimatedShiftToCheckForWarning;
    NSInteger yearsOfExperienceString;
    BOOL getCurrentAddressButtonPRessed;
    BOOL genderPreferenceChanged;
    BOOL updateGenderString;
    BOOL shouldLoadLocation;
    BOOL hideAutoCompeleteButton;
    NSString *additionalRequirementsString;
    UITextView *additionalDetailsTextView;
    UITextView *additionalDetailsPlaceholderTextView;

    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Request a Worker";
    self.statusPickerVisible = NO;
    hideEndTimePicker = YES;
    hideStartTimePicker = YES;
    hideGenderPicker = YES;
    updateGenderString = NO;
    blankCell2 = YES;
    blankCell1 = YES;
    getCurrentAddressButtonPRessed = NO;
    isSelected = NO;
    genderPreferenceChanged = NO;
    genderPreferenceString = 0;
    yearsOfExperienceString = 0;
    hideAutoCompeleteButton = NO;
    additionalRequirementsString = @"";
    [NSNotificationCenter.defaultCenter addObserver:self selector:@selector(updateAdditionalRequirementsString:) name:@"updateAdditionalRequirementsString" object:nil];
    _locManager = [CLLocationManager new];
    _locManager.delegate = self;
    
    _currentUser = [User currentUser];
    
    [self.tableView reloadData];
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateAdditionalRequirementsString:(NSNotification *)notification {
    additionalRequirementsString = notification.userInfo[@"additionalRequirements"];
    
    if ([notification.userInfo[@"isEditing"] isEqualToNumber:@YES]) {
        [self.tableView reloadData];
        ShiftDetailTableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
        [cell.additionalRequirementsTextView becomeFirstResponder];
        
    }
}


-(void)loadQualifications {
    [NetworkingHelper getQualificationsWithSuccess:^(id responseObject) {
        NSMutableArray *mutArr = [NSMutableArray new];
        for (NSDictionary *dict in responseObject) {
            Qualifications *qualifications = [[Qualifications alloc] initWithDictionary:dict];
            //       NSLog(@"qualifications %@", qualifications);
            [mutArr addObject:qualifications];
        }
        [self->qualificationsArrayRecip addObjectsFromArray:mutArr];
        // NSLog(@"qualis %@", qualificationsArrayRecip);
        self->loadingRecipQuals = NO;
        [self.tableView reloadData];
        //[self loadCounties];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"failure in loadcounties %@", error);
        id responseObject = [NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:0 error:nil];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:[NSString stringWithFormat:@"%@", responseObject[@"error"]] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.tableView reloadData];
        }]];
        
        alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
        [self presentViewController:alert animated:YES completion:NULL];
        
    }];
    
}

-(void)goToShiftDetail{
    
    AcceptedShiftViewController *vc = [[UIStoryboard storyboardWithName:@"OnDemandWorker" bundle:nil] instantiateViewControllerWithIdentifier:@"AcceptedShiftViewController"];
    [self.navigationController showViewController:vc sender:self];
}



#pragma mark - UITableView Data Source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 4;
    }
    return 1;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return @"What time do you need a worker?";
    } else {
        return nil;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 1) {
        return 0.01;
    }
    return 34.;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

-(void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[UIColor colorNamed:@"ButtonColor"]];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (indexPath.row == 1 && hideStartTimePicker == YES) {
            return self.view.frame.size.height *.07;
        }else if (indexPath.row == 1 && hideStartTimePicker == NO){
            return 261;
            
        }else if (indexPath.row == 2 && hideEndTimePicker == YES) {
            return self.view.frame.size.height *.07;
        }else if (indexPath.row == 2 && hideEndTimePicker == NO){
            return 261;
        }else if (indexPath.row == 3){
            //            return self.view.frame.size.height *.45;
            return [self heightForTextViewForString:additionalRequirementsString] < 63. ? 173. + 63. : (173. + [self heightForTextViewForString:additionalRequirementsString]);
            
        } else {
            return 44.;
        }
    }
    return 200;
}

-(CGFloat)heightForTextViewForString:(NSString *)string {
    UITextView *tv = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width - 16, 1)];
    tv.attributedText = [[NSAttributedString alloc] initWithString:string attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.]}];
    [tv sizeToFit];
    return tv.frame.size.height;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            CountiesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CountiesTableViewCell"];
            cell.countyLabel.text = [NSString stringWithFormat:@"For Worker: %@ %@", _pca.firstName, _pca.lastName];
            return cell;
        }
        if (indexPath.row == 1){
            
            StartTimeCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"StartTimeCellTableViewCell"];
            NSDateFormatter *format = [[NSDateFormatter alloc] init];
            NSDateFormatter *format2 = [[NSDateFormatter alloc] init];
            
            [format2 setDateFormat:@"MMM dd, yyyy hh:mm a"];
            [format setDateFormat:kDateFormat];
            
            NSString *dateStr = [format stringFromDate:cell.startTimePickerView.date];
            NSString *dateStringForLabel = [format2 stringFromDate:cell.startTimePickerView.date];
            if ([[[UIDevice currentDevice] systemVersion] floatValue] < 10.0) {
                [cell.startTimePickerView setDatePickerMode:UIDatePickerModeDate];
                [cell.startTimePickerView setDatePickerMode:UIDatePickerModeDateAndTime];
                
            }
            
            
            inTimeDateString = [format dateFromString:dateStr];
            NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
            NSDate *currentDate = [self roundToNearestQuarterHour:[NSDate date]];
            NSDateComponents *comps = [[NSDateComponents alloc] init];
            [comps setDay:0];
            NSDate *minDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
            [cell.startTimePickerView setMinimumDate:minDate];
            
            if (blankCell1 == YES) {
                cell.downArrowImage.image = [UIImage imageNamed:@"down_chevron"];
                cell.startTimeLabel.text = @"";
            }else if (blankCell1 == NO){
                cell.downArrowImage.image = [UIImage imageNamed:@"up_chevron"];
                cell.startTimeLabel.text = dateStringForLabel;
                dateStringForLabel = pickerintimelabel;
                shiftStartTime = dateStr;
            }
            if (hideStartTimePicker == YES){
                cell.downArrowImage.image = [UIImage imageNamed:@"down_chevron"];
            }
            return cell;
            
        }else if (indexPath.row == 2){
            
            EndTimeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EndTimeTableViewCell"];
            NSDateFormatter *format = [[NSDateFormatter alloc] init];
            NSDateFormatter *format2 = [[NSDateFormatter alloc] init];
            
            [format2 setDateFormat:@"MMM dd, yyyy hh:mm a"];
            [format setDateFormat:kDateFormat];
            
            NSString *dateStr = [format stringFromDate:cell.endTimePickerView.date];
            NSString *dateStringForLabel = [format2 stringFromDate:cell.endTimePickerView.date];
            if ([[[UIDevice currentDevice] systemVersion] floatValue] < 10.0) {
                [cell.endTimePickerView setDatePickerMode:UIDatePickerModeDate];
                [cell.endTimePickerView setDatePickerMode:UIDatePickerModeDateAndTime];
            }
            
            outTimeDateString = [format dateFromString:dateStr];
            NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
            NSDateComponents *comps = [[NSDateComponents alloc] init];
            
            [comps setDay:0];
            NSDate *minDate = [calendar dateByAddingComponents:comps toDate:inTimeDateString options:0];
            [comps setHour:30];
            NSDate *maxDate = [calendar dateByAddingComponents:comps toDate:inTimeDateString options:0];
            
            [cell.endTimePickerView setMaximumDate:maxDate];
            [cell.endTimePickerView setMinimumDate:minDate];
            
            if (blankCell2 == YES) {
                cell.downArrowImage.image = [UIImage imageNamed:@"down_chevron"];
                cell.endTimeLabel.text = @"";
            }else if (blankCell2 == NO){
                cell.downArrowImage.image = [UIImage imageNamed:@"up_chevron"];
                cell.endTimeLabel.text = dateStringForLabel;
                dateStringForLabel = pickerouttimelabel;
                shiftEndTime = dateStr;
            }
            if (hideEndTimePicker == YES){
                cell.downArrowImage.image = [UIImage imageNamed:@"down_chevron"];
            }
            return cell;
        } else if (indexPath.row == 3){
            ShiftDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"shiftDetailTableViewCell"];
            NSTimeInterval distanceBetweenDates = [outTimeDateString timeIntervalSinceDate:inTimeDateString];
            
            if ([[NSUserDefaults standardUserDefaults] objectForKey:@"recipientAddress"]) {
                cell.addressTextField.text = [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"recipientAddress"]];
            }
            
            
            if (hideAutoCompeleteButton == YES) {
                cell.autocompleteAddressButton.hidden = YES;
            }else{
                cell.autocompleteAddressButton.hidden = NO;
            }
            
            if (getCurrentAddressButtonPRessed == NO) {
                self.addressString = cell.addressTextField.text;
            }else {
                cell.addressTextField.text = self.addressString;
            }
            
            if (blankCell1 == YES) {
                cell.jobTitleLabel.text = @"";
            }else if (distanceBetweenDates > 0){
                CGFloat floatingPointNumber = distanceBetweenDates/3600;
                NSInteger integerNumber = floatingPointNumber;
                CGFloat floatingPointNumberWithoutTheInteger = floatingPointNumber - integerNumber;
                CGFloat newMinutesString = 60/floatingPointNumberWithoutTheInteger;
                CGFloat m = 60/newMinutesString;
                CGFloat numberToPass = m *60;
                
                NSString* formattedHours = [NSString stringWithFormat:@"%.f", floor(distanceBetweenDates/3600)];
                NSString *formattedMinutes = [NSString stringWithFormat:@"%.f", ceil(numberToPass)];
                if (floor(distanceBetweenDates/3600) == 0 && numberToPass == 0) {
                    estimatedShiftToCheckForWarning = 0;
                }else{
                    estimatedShiftToCheckForWarning = 1;
                }
                
                if (floor(distanceBetweenDates/3600) > 1 && numberToPass == 0) {
                    cell.jobTitleLabel.text = [NSString stringWithFormat:@"Estimated shift length: %@ hours", formattedHours];
                }else if (floor(distanceBetweenDates/3600) > 1 && ceil(numberToPass) == 60) {
                    cell.jobTitleLabel.text = [NSString stringWithFormat:@"Estimated shift length: %.f hours", (floor(distanceBetweenDates/3600)+1)];
                }else if (floor(distanceBetweenDates/3600) == 1 && numberToPass > 0){
                    cell.jobTitleLabel.text = [NSString stringWithFormat:@"Estimated shift length: %@ hour %@ minutes", formattedHours, formattedMinutes];
                }else if (floor(distanceBetweenDates/3600) == 1 && numberToPass == 0){
                    cell.jobTitleLabel.text = [NSString stringWithFormat:@"Estimated shift length: %@ hour", formattedHours];
                }else if (floor(distanceBetweenDates/3600) == 0 && numberToPass > 0 && !(numberToPass > 57 && numberToPass < 60)){
                    cell.jobTitleLabel.text = [NSString stringWithFormat:@"Estimated shift length: %@ minutes", formattedMinutes];
                }else if (floor(distanceBetweenDates/3600) == 0 && (numberToPass > 55 && numberToPass < 60)){
                    cell.jobTitleLabel.text = [NSString stringWithFormat:@"Estimated shift length: 1 hour"];
                }else{
                    cell.jobTitleLabel.text = [NSString stringWithFormat:@"Estimated shift length: %@ hours %@ minutes", formattedHours, formattedMinutes];
                }
                
//                cell.additionalRequirementsTextView.text = additionalRequirementsString;
                
                cell.additionalRequirementsTextView.delegate = self;
                cell.additionalRequirementsPlaceholderTextView.delegate = self;
                additionalDetailsTextView = cell.additionalRequirementsTextView;
                additionalDetailsPlaceholderTextView = cell.additionalRequirementsPlaceholderTextView;
                additionalDetailsTextView.delegate = self;
                additionalDetailsPlaceholderTextView.delegate = self;

            }
            return cell;
        }
        
    }else if (indexPath.section == 1){
        
        if (indexPath.row == 0) {
            AcceptButtonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"acceptShiftTableViewCell"];
            return cell;
            
        }
    }
    
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 1 && hideStartTimePicker == NO) {
            hideStartTimePicker = YES;
            blankCell1 = NO;
            [self.tableView reloadData];
            
        }else if (indexPath.row == 2 && hideEndTimePicker == NO) {
            hideEndTimePicker = YES;
            blankCell2 = NO;
            [self.tableView reloadData];
            
        }else if (indexPath.row == 1 && hideStartTimePicker == YES) {
            hideStartTimePicker = NO;
            blankCell1 = NO;
            [self.tableView reloadData];
            
        }else if (indexPath.row == 2 && hideEndTimePicker == YES) {
            hideEndTimePicker = NO;
            blankCell2 = NO;
            [self.tableView reloadData];
            
        }
    }
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(IBAction)requestAPCAButtonTapped:(id)sender{
    [self.view endEditing:YES];
    if (!shiftStartTime) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Required" message:@"You need to enter a start time." preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:NULL]];
        alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
        [self presentViewController:alert animated:YES completion:NULL];
    }
    else if (!shiftEndTime) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Required" message:@"You need to enter an end time." preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:NULL]];
        
        alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
        [self presentViewController:alert animated:YES completion:NULL];
        
    }
    else if ([self.addressString isEqualToString:@""]) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Required" message:@"You need to enter an address." preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:NULL]];
        
        alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
        [self presentViewController:alert animated:YES completion:NULL];
    }else if (estimatedShiftToCheckForWarning == 0){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"End Time needs to be later than start time." preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:NULL]];
        
        alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
        [self presentViewController:alert animated:YES completion:NULL];
    }else{
        
        
        [[NSUserDefaults standardUserDefaults] setObject:self.addressString forKey:@"recipientAddress"];
        [self endEditing];
        [self requestAPCA];
    }
    
    
    
}


-(void)requestAPCA{
    LoadingView *loadingView = [LoadingView addLoadingViewWithText:@"Loading"];
    //    NSLog(@"id:%@ address:%@ gender:%@ startTime:%@ endTime:%@ yoe:%@", User.currentUser.recipient.idNumber, self.addressString, genderPreferenceString, shiftStartTime, shiftEndTime, yearsOfExperienceString);
    
    
    [NetworkingHelper requestAPCAWithRecipientID:User.currentUser.recipient.idNumber andAddress:self.addressString andGenderPreference:genderPreferenceString andShiftStartTime:shiftStartTime andShiftEndTime:shiftEndTime andYOE:yearsOfExperienceString andQualificationsArray:[NSMutableArray new] andAdditionalRequirements:additionalRequirementsString andPCAID:_pca.idNumber andSuccess:^(id responseObject) {
        NSLog(@"response object success %@", responseObject);
        [loadingView removeFromSuperview];
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Success" message:@"Your request was successful. You will be notified when a worker accepts your request." preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            //  [self.navigationController dismissViewControllerAnimated:YES completion:nil];
            //            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
            //            RecipientHomeTableViewController *vc = [[UIStoryboard storyboardWithName:@"OnDemandRecipient" bundle:nil] instantiateViewControllerWithIdentifier:@"RecipientHomeTableViewController"];
            //            [self.navigationController showViewController:vc sender:self];
            [self dismiss:nil];
            
        }]];
        
        alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
        [self presentViewController:alert animated:YES completion:NULL];
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [loadingView removeFromSuperview];
        NSLog(@"task %@", task);
        NSLog(@"failure %@", error);
        id responseObject = [NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:0 error:nil];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:[NSString stringWithFormat:@"%@", responseObject[@"error"]] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.tableView reloadData];
        }]];
        
        alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
        [self presentViewController:alert animated:YES completion:NULL];
        
    }];
}


-(IBAction)getCurrentLocationButtonPressed:(id)sender {
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways) {
        shouldLoadLocation = YES;
        [self getLocationPermission];
    } else {
        [self showAutocompleteController];
    }
    
}

// Handle the user's selection.
- (void)viewController:(GMSAutocompleteViewController *)viewController
didAutocompleteWithPlace:(GMSPlace *)place {
    [self dismissViewControllerAnimated:YES completion:nil];
    self.addressString = place.formattedAddress;
    // Do something with the selected place.
    //    NSLog(@"Place name %@", place.name);
    //    NSLog(@"Place address %@", place.formattedAddress);
    //    NSLog(@"Place attributions %@", place.attributions.string);
    getCurrentAddressButtonPRessed = YES;
    [self.tableView reloadData];
}

- (void)viewController:(GMSAutocompleteViewController *)viewController
didFailAutocompleteWithError:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:nil];
    // TODO: handle the error.
    //    ShiftDetailTableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    //    cell.autocompleteAddressButton.alpha = 0.0;
    NSLog(@"Error: %@", [error description]);
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
    ShiftDetailTableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    cell.autocompleteAddressButton.alpha = 0.0;
}

// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)didUpdateAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

-(IBAction)starttimepickerchanged:(id)sender{
    
    [self.tableView reloadData];
}

-(IBAction)endtimepickerchanged:(id)sender{
    
    [self.tableView reloadData];
}

-(IBAction)dismiss:(id)sender{
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isKindOfClass:[RecipientHomeTableViewController class]]) {
            [self.navigationController popToViewController:vc animated:YES];
            break;
        }
    }
}


-(IBAction)genderprefernecetouched:(id)sender{
    [self.tableView setContentOffset:CGPointMake(0, CGFLOAT_MAX)];
    [self.tableView reloadData];
}

#pragma mark picker stuff

- (NSDate *)roundToNearestQuarterHour:(NSDate *)date{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    unsigned unitFlags = NSCalendarUnitYear| NSCalendarUnitMonth | NSCalendarUnitDay |  NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitWeekday | NSCalendarUnitWeekdayOrdinal | NSCalendarUnitWeekOfYear;
    NSDateComponents *components = [calendar components:unitFlags fromDate:date];
    NSInteger roundedToQuarterHour = ceil((components.minute/15.0)) * 15;
    components.minute = roundedToQuarterHour;
    return [calendar dateFromComponents:components];
}

#pragma mark Textfield


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    textField.inputAccessoryView = _keyboardToolbar;
    for (UIBarButtonItem *item in _keyboardToolbar.items) {
        item.tag = textField.tag;
    }
    return YES;
}

-(IBAction)nextTextField:(UIBarButtonItem *)sender {
    long tag = sender.tag;
    id nextView = [self.view viewWithTag:tag + 1];
    if (nextView && [nextView respondsToSelector:@selector(becomeFirstResponder)]) {
        [nextView becomeFirstResponder];
    } else {
        [self dismissKeyboard:sender];
    }
}

-(IBAction)prevTextField:(UIBarButtonItem *)sender {
    long tag = sender.tag;
    id previousView = [self.view viewWithTag:tag - 1];
    if (previousView && [previousView respondsToSelector:@selector(becomeFirstResponder)]) {
        [previousView becomeFirstResponder];
    } else {
        [self dismissKeyboard:sender];
    }
}

-(IBAction)dismissKeyboard:(UIBarButtonItem *)sender {
    [self.tableView reloadData];
    
    [self.view endEditing:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    UIBarButtonItem *item = [UIBarButtonItem new];
    item.tag = textField.tag;
    [self nextTextField:item];
    return YES;
}




-(void)endEditing {
    [self.tableView setContentOffset:CGPointMake(0, 20.)];
    [self.view endEditing:YES];
}

-(UIToolbar *)textFieldAccessoryToolbar:(UITextField *)textField {
    
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 44)];
    toolbar.tintColor = [UIColor darkGrayColor];
    UIButton *previousButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [previousButton addTarget:self action:@selector(prevTextField:) forControlEvents:UIControlEventTouchUpInside];
    previousButton.tag = textField.tag;
    [previousButton setBackgroundImage:[UIImage imageNamed:@"back_chevron"] forState:UIControlStateNormal];
    previousButton.frame = CGRectMake(0, 0, 32, 32);
    UIBarButtonItem *prevBBItem = [[UIBarButtonItem alloc] initWithCustomView:previousButton];
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [nextButton addTarget:self action:@selector(nextTextField:) forControlEvents:UIControlEventTouchUpInside];
    [nextButton setBackgroundImage:[UIImage imageNamed:@"chevron"] forState:UIControlStateNormal];
    nextButton.tag = textField.tag;
    nextButton.frame = CGRectMake(0, 0, 32, 32);
    UIBarButtonItem *nextBBItem = [[UIBarButtonItem alloc] initWithCustomView:nextButton];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *fixedSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedSpace.width = -6;
    toolbar.items = @[prevBBItem, nextBBItem, flexibleSpace];
    return toolbar;
}



- (BOOL) containsSymbols: (NSString *) candidate {
    NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"_@.+abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789"] invertedSet];
    
    if ([candidate rangeOfCharacterFromSet:set].location != NSNotFound) {
        return YES;
    }
    return NO;
}

-(BOOL) containsWhitespace: (NSString *) candidate {
    NSRange whiteSpaceRange = [candidate rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
    if (whiteSpaceRange.location != NSNotFound) {
        return YES;
    }
    return NO;
}

#pragma mark - Get Location Permission

-(void)getLocationPermission {
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways) {
        NSLog(@"startUpdatingLocation");
        [_locManager startUpdatingLocation];
    } else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
        [_locManager requestWhenInUseAuthorization];
    } else {
        /*  ITS OK TO LET THIS BE, WE DON'T NEED TO PROMPT THEM FOR IT EVERY TIME THEY HIT THIS SCREEN. ITS NOT THAT IMPORTANT TO THE APP. WITHOUT LOCATION, ALL THAT IS AFFECTED IS THEY HAVE TO TYPE MORE IN THE ADDRESS AUTOCOMPLETE. */
    }
}

-(void)goToPhoneSettings {
    NSURL *appSettings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    [[UIApplication sharedApplication] openURL:appSettings];
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == kCLAuthorizationStatusAuthorizedAlways || status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        NSLog(@"didChange");
        if (shouldLoadLocation) {
            [self getLocationPermission];
        }
    } else if (status == kCLAuthorizationStatusNotDetermined) {
        [self.locManager requestWhenInUseAuthorization];
    } else if (status == kCLAuthorizationStatusDenied || kCLAuthorizationStatusRestricted) {
        NSString *appName = [[[NSBundle mainBundle] infoDictionary] objectForKey:(id)kCFBundleNameKey];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Location" message:[NSString stringWithFormat:@"To get the most out of %@, it is best to have location enabled. Would you like to change this in your phone settings?", appName] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self goToPhoneSettings];
        }]];
        //        [alert addAction:[UIAlertAction actionWithTitle:@"No thanks" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        //            AddressDetailViewController *vc = [[UIStoryboard storyboardWithName:@"JobDetail" bundle:nil] instantiateViewControllerWithIdentifier:@"AddressDetailViewController"];
        //            vc.office = _assignment.office;
        //            [self.navigationController showViewController:vc sender:self];
        //        }]];
        [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:NULL]];
        //        self.goToGoogleAutoCompleteButton.hidden = YES;
        //        hideAutoCompeleteButton = YES;
        [self.tableView reloadData];
        [self presentViewController:alert animated:YES completion:NULL];
        
        //        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        //            //            AddressDetailViewController *vc = [[UIStoryboard storyboardWithName:@"JobDetail" bundle:nil] instantiateViewControllerWithIdentifier:@"AddressDetailViewController"];
        //            //            vc.office = _assignment.office;
        //            //            [self.navigationController showViewController:vc sender:self];
        //            [self goToPhoneSettings];
        //        }]];
        //        [self presentViewController:alert animated:YES completion:NULL];
    }
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"failed to find location with error:%@", error);
}


-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    [self.locManager stopUpdatingLocation];
    self.locManager = manager;
    if (locations.count > 0) {
        NSLog(@"location found");
        [self showAutocompleteController];
    }
    
}

-(void)showAutocompleteController {
    CLLocation *currentLocation = self.locManager.location;
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    GMSAutocompleteFilter *filter = [[GMSAutocompleteFilter alloc] init];
    filter.type = kGMSPlacesAutocompleteTypeFilterAddress;
    acController.autocompleteFilter = filter;
    acController.delegate = self;
    // We will have to see whether or not creating this bounds causes problems.
    if (currentLocation) {
        CLLocationCoordinate2D northEast = CLLocationCoordinate2DMake(currentLocation.coordinate.latitude + 0.1, currentLocation.coordinate.longitude + 0.1);
        CLLocationCoordinate2D southWest = CLLocationCoordinate2DMake(currentLocation.coordinate.latitude - 0.1, currentLocation.coordinate.longitude - 0.1);
        acController.autocompleteBounds = [[GMSCoordinateBounds alloc] initWithCoordinate:northEast coordinate:southWest];
    }
    
    [self presentViewController:acController animated:YES completion:nil];
}

#pragma mark - UITextViewDelegate

-(void)textViewDidBeginEditing:(UITextView *)textView {
    additionalDetailsPlaceholderTextView.inputAccessoryView = _keyboardToolbar;
    additionalDetailsTextView.inputAccessoryView = _keyboardToolbar;

    if (additionalDetailsTextView.text.length > 0) {
        additionalDetailsPlaceholderTextView.hidden = YES;
    }else{
        additionalDetailsPlaceholderTextView.hidden = NO;
    }
}

-(void)textViewDidChange:(UITextView *)textView {
    additionalDetailsPlaceholderTextView.hidden = additionalDetailsTextView.text.length > 0;
    additionalRequirementsString = additionalDetailsTextView.text;
}

-(void)textViewDidEndEditing:(UITextView *)textView {
    additionalDetailsPlaceholderTextView.hidden = additionalDetailsTextView.text.length > 0;
    additionalRequirementsString = additionalDetailsTextView.text;
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
