//
//  UpcomingShiftsTableViewCell.h
//  OnDemand Timesheets
//
//  Created by Joey Kjornes on 3/13/17.
//  Copyright © 2017 Jed Mahonis Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpcomingShiftsTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *shiftStatusLabel;
@property (nonatomic, weak) IBOutlet UILabel *shiftTimeLabel;
@property (nonatomic, weak) IBOutlet UILabel *shiftTypeLabel;
@property (nonatomic, weak) IBOutlet UILabel *distanceLabel;
@property (nonatomic, weak) IBOutlet UILabel *yourETALabel;
@property (nonatomic, weak) IBOutlet UILabel *noRecentRequestsLabel;
@property (weak, nonatomic) IBOutlet UIImageView *colorStatusImageView;

@property (nonatomic, weak) IBOutlet UIButton *cancelButton;
@property (nonatomic, weak) IBOutlet UIButton *startAnotherShiftButton;
@property (nonatomic, weak) IBOutlet UIButton *viewProfileButton;

@end
