//
//  UserSettingsOptionsTableViewCell.m
//  OnDemand Timesheets
//
//  Created by Joey Kjornes on 4/25/17.
//  Copyright © 2017 Jed Mahonis Group. All rights reserved.
//

#import "UserSettingsOptionsTableViewCell.h"

@implementation UserSettingsOptionsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
