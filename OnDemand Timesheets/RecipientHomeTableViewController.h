//
//  RecipientHomeTableViewController.h
//  OnDemand Timesheets
//
//  Created by Joey Kjornes on 3/16/17.
//  Copyright © 2017 Jed Mahonis Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import <CoreLocation/CoreLocation.h>
#import <UserNotifications/UserNotifications.h>
@interface RecipientHomeTableViewController : UITableViewController <CLLocationManagerDelegate, UNUserNotificationCenterDelegate>


@property (nonatomic, weak) IBOutlet UIButton *requestAPCAButton;
@property (nonatomic, weak) IBOutlet UIButton *seeRequestsButton;
@property (nonatomic, weak) IBOutlet UIButton *browseWorkersButton;
@property (nonatomic, weak) IBOutlet UIImageView *agencyImage;
@property (nonatomic, weak) IBOutlet UILabel *helloUserLabel;
@property (nonatomic, strong) User *currentUser;
@property (nonatomic, strong) CLLocationManager *locManager;


@end
