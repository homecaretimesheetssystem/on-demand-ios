//
//  JMGCameraViewController.h
//  camera
//
//  Created by Scott Mahonis on 7/23/16.
//  Copyright © 2016 JMG. All rights reserved.
//  Version 1.5

#import <UIKit/UIKit.h>

@protocol JMGCameraViewControllerDelegate <NSObject>

-(void)dismissCameraWithImage:(UIImage *)image;

@end

typedef NS_ENUM(NSUInteger, CameraMode) {
    CameraMode_default,
    CameraMode_square,
    CameraMode_fullScreen,
    CameraMode_Custom,
};

typedef NS_ENUM(NSUInteger, FlashMode) {
    FlashMode_auto,
    FlashMode_on,
    FlashMode_off,
};

typedef NS_ENUM(NSUInteger, FileType) {
    FileType_png,
    FileType_jpg,
};

#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>

@interface JMGCameraViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, AVCaptureMetadataOutputObjectsDelegate, UIScrollViewDelegate, AVCapturePhotoCaptureDelegate> {
    BOOL containsImage; CGFloat height; CGFloat width; CGFloat originalImageHeight; CGFloat originalImageWidth; bool fromLibrary;

    //Camera Vars
    CGRect flashFrame; UIButton *flashButton; UIButton *chooseAuto; UIButton *chooseOn; UIButton *chooseOff;

    //Cropping Vars
    UIScrollView *editingCropScrollview; UIImageView *editImageView; UILabel *moveAndScaleLabel;
}

@property (nonatomic, retain) IBOutlet UIImagePickerController *imagePicker;
@property (nonatomic, retain) UIImage *picture;
@property (nonatomic, strong) IBOutlet UIView *cameraView;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *uploadBarButton;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *retakePictureBarButton;
@property (nonatomic, strong) id<JMGCameraViewControllerDelegate>delegate;
@property (nonatomic) UIBarStyle parentNavBarStyle;
@property (nonatomic) UIStatusBarStyle parentStatusBarStyle;
@property (nonatomic, strong) UIColor *cameraBackgroundColor;
@property (nonatomic) CameraMode cameraMode;
@property (nonatomic) FileType fileType;
@property (nonatomic) BOOL showWhiteCircleInSquareMode;
@property (nonatomic) BOOL showPictureFromCameraRollForChooseFromPhotoLibraryButton;
@property (nonatomic) BOOL shouldUploadFromCameraView;
@property (nonatomic) BOOL faceFrontOnLaunch;

@property (nonatomic) NSString *titleAboveCamera;
@property (nonatomic) CGSize fileSize;

@property (nonatomic, retain) AVCaptureSession *session;
@property (nonatomic, strong) AVCaptureDevice *device;
@property (nonatomic, strong) AVCaptureDeviceInput *input;
@property (nonatomic, strong) AVCaptureMetadataOutput *output;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *prevLayer;
@property (nonatomic, retain) id cameraOutput;
@property (nonatomic, retain) AVCapturePhotoSettings *capturePhotoSettings;
@property (nonatomic, strong) AVCaptureConnection *captureConnection;
@property (nonatomic, strong) UIView *highlightView;
@property(nonatomic) AVCaptureFlashMode flashMode;
@property(nonatomic, readonly) NSArray<NSNumber *> *supportedFlashModes;


-(IBAction)prepareForUpload:(id)sender;
-(IBAction)openCamera:(id)sender;


@end
