//
//  CountiesTableViewController.h
//  OnDemand Timesheets
//
//  Created by Joey Kjornes on 4/17/17.
//  Copyright © 2017 Jed Mahonis Group. All rights reserved.
//



@protocol CountiesTableViewControllerDelegate <NSObject>

-(void)finishedSelectingCountiesWithArray:(NSMutableArray *)countiesArrayy;

@end

#import <UIKit/UIKit.h>
#import "EditWorkerProfileTableViewController.h"


@interface CountiesTableViewController : UITableViewController <UISearchBarDelegate>

@property (nonatomic, strong) NSMutableArray *workersActualCountiesArray;
@property (nonatomic, strong) id<CountiesTableViewControllerDelegate>delegate;
@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;




@end
