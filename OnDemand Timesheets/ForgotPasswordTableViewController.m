//
//  ForgotPasswordTableViewController.m
//  OnDemand Timesheets
//
//  Created by Joey Kjornes on 5/10/18.
//  Copyright © 2018 Jed Mahonis Group. All rights reserved.
//

#import "ForgotPasswordTableViewController.h"

@interface ForgotPasswordTableViewController ()

@end

@implementation ForgotPasswordTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createUI];
//    self.resetButton.layer.borderColor = [UIColor colorWithRed:55.0/255.0 green:99.0/255.0 blue:135.0/255.0 alpha:1.0].CGColor;
//    self.resetButton.layer.borderWidth = 1.0f; //make border 1px thick
    
    self.title = @"Reset Password";

    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStylePlain target:nil action:nil];
//    self.navigationItem.hidesBackButton = NO;
    
}

-(void)createUI{
    UIWebView *webview=[[UIWebView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    NSString *url;
    if (kDebug) {
        url = @"https://staging.homecaretimesheetsapp.com/forgot";
    }else{
        url = @"https:dashboard.homecaretimesheetsapp.com/forgot";
    }
    
    NSURL *nsurl=[NSURL URLWithString:url];
    NSURLRequest *nsrequest=[NSURLRequest requestWithURL:nsurl];
    [webview loadRequest:nsrequest];
//    webview.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:webview];
}


#pragma mark - Table view data source

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    return 1;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return 3;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.row == 0) {
//        return 200;
//    }else if (indexPath.row == 3){
//        return 200;
//    }
//    return 80;
//}
//
//
//-(IBAction)resetButtonPressed:(id)sender{
//    [self.view endEditing:YES];
//    [self checkForCharacters];
//}
//
//-(void)verifyWithServer {
//    NSDictionary *params = @{@"username":self.emailTextfield.text};
//    LoadingView *loadingView = [LoadingView addLoadingViewWithText:@"Loading"];
//
//    [NetworkingHelper resetPasswordWithParams:params success:^(id responseObject) {
//        NSString *errorNumber = responseObject[@"error"];
//        NSString *errorMessage = responseObject[@"errorMessage"];
//        NSLog(@"response object is %@", responseObject);
//        [loadingView hideActivityIndicator];
//        if ([errorNumber isEqualToString:@"2"]) {
//
//            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"E-mail is incorrect. Please try again" preferredStyle:UIAlertControllerStyleAlert];
//            [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//            }]];
//            alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
//            [self presentViewController:alert animated:YES completion:NULL];
//
//        } else if (![errorNumber isEqualToString:@"1"]) {
//            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Success" message:@"We sent you an email containing the information you requested." preferredStyle:UIAlertControllerStyleAlert];
//            [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                [self.navigationController popToRootViewControllerAnimated:YES];
//
//            }]];
//            alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
//            [self presentViewController:alert animated:YES completion:NULL];
//
//
//        } else if ([errorNumber isEqualToString:@"1"]) {
//            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:[NSString stringWithFormat:@"Error:%@", errorMessage] preferredStyle:UIAlertControllerStyleAlert];
//            [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//            }]];
//            alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
//            [self presentViewController:alert animated:YES completion:NULL];
//
//        } else if (errorNumber) {
//
//            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Unable to connect to the server. Please check your internet connection and try again." preferredStyle:UIAlertControllerStyleAlert];
//            [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//            }]];
//            alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
//            [self presentViewController:alert animated:YES completion:NULL];
//        }
//
//    } failure:^(NSURLSessionDataTask *task, NSError *error) {
//        NSLog(@"failurrrre error is %@", error);
//        id responseObject = [NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:0 error:nil];
//        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:[NSString stringWithFormat:@"%@", responseObject[@"error"]] preferredStyle:UIAlertControllerStyleAlert];
//        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//            [self.tableView reloadData];
//        }]];
//
//        alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
//        [self presentViewController:alert animated:YES completion:NULL];
//        [loadingView hideActivityIndicator];
//
//    }];
//
//}
//
//
//
//#pragma mark - UITextfield Delegate
//
//
//-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
//    textField.inputAccessoryView = _keyboardToolbar;
//    for (UIBarButtonItem *item in _keyboardToolbar.items) {
//        item.tag = textField.tag;
//    }
//    return YES;
//}
//
////-(IBAction)nextTextField:(UIBarButtonItem *)sender {
////    long tag = sender.tag;
////    id nextView = [self.view viewWithTag:tag + 1];
////    if (nextView && [nextView respondsToSelector:@selector(becomeFirstResponder)]) {
////        [nextView becomeFirstResponder];
////    } else {
////        [self dismissKeyboard:sender];
////    }
////}
////
////-(IBAction)prevTextField:(UIBarButtonItem *)sender {
////    long tag = sender.tag;
////    id previousView = [self.view viewWithTag:tag - 1];
////    if (previousView && [previousView respondsToSelector:@selector(becomeFirstResponder)]) {
////        [previousView becomeFirstResponder];
////    } else {
////        [self dismissKeyboard:sender];
////    }
////}
//
//-(IBAction)dismissKeyboard:(UIBarButtonItem *)sender {
//    [self.view endEditing:YES];
//}
//
//-(BOOL)textFieldShouldReturn:(UITextField *)textField {
//    UIBarButtonItem *item = [UIBarButtonItem new];
//    item.tag = textField.tag;
//    return YES;
//}
//
//
////
////
////-(void)endEditing {
////    [self.tableView setContentOffset:CGPointMake(0, 20.)];
////    [self.view endEditing:YES];
////}
////
////-(UIToolbar *)textFieldAccessoryToolbar:(UITextField *)textField {
////
////    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 44)];
////    toolbar.tintColor = [UIColor darkGrayColor];
////    UIButton *previousButton = [UIButton buttonWithType:UIButtonTypeCustom];
////    [previousButton addTarget:self action:@selector(prevTextField:) forControlEvents:UIControlEventTouchUpInside];
////    previousButton.tag = textField.tag;
////    [previousButton setBackgroundImage:[UIImage imageNamed:@"back_chevron"] forState:UIControlStateNormal];
////    previousButton.frame = CGRectMake(0, 0, 32, 32);
////    UIBarButtonItem *prevBBItem = [[UIBarButtonItem alloc] initWithCustomView:previousButton];
////    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
////    [nextButton addTarget:self action:@selector(nextTextField:) forControlEvents:UIControlEventTouchUpInside];
////    [nextButton setBackgroundImage:[UIImage imageNamed:@"chevron"] forState:UIControlStateNormal];
////    nextButton.tag = textField.tag;
////    nextButton.frame = CGRectMake(0, 0, 32, 32);
////    UIBarButtonItem *nextBBItem = [[UIBarButtonItem alloc] initWithCustomView:nextButton];
////    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
////    UIBarButtonItem *fixedSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
////    fixedSpace.width = -6;
////    toolbar.items = @[prevBBItem, nextBBItem, flexibleSpace];
////    return toolbar;
////}
//
//
//
//- (BOOL) containsSymbols: (NSString *) candidate {
//    NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"_@.+abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789"] invertedSet];
//
//    if ([candidate rangeOfCharacterFromSet:set].location != NSNotFound) {
//        return YES;
//    }
//    return NO;
//}
//
//-(BOOL) containsWhitespace: (NSString *) candidate {
//    NSRange whiteSpaceRange = [candidate rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
//    if (whiteSpaceRange.location != NSNotFound) {
//        return YES;
//    }
//    return NO;
//}
//
//-(void)checkForCharacters{
//    if ([_emailTextfield.text isEqualToString:@""]) {
//        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"You need to enter in your Email address" preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//
//        }];
//        [alert addAction:okAction];
//        [self presentViewController:alert animated:YES completion:nil];
//
//    }else{
//        [self verifyWithServer];
//    }
//}
//
//-(void)userNameErrorWithTextField:(UITextField *)textField {
//    [self.tableView reloadData];
//    [textField becomeFirstResponder];
//    _resetButton.enabled = NO;
//    _resetButton.backgroundColor = [UIColor colorWithWhite:0.65 alpha:1.0];
//}


@end
