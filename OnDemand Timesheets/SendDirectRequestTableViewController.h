//
//  SendDirectRequestTableViewController.h
//  OnDemand Timesheets
//
//  Created by Scott Mahonis on 3/30/18.
//  Copyright © 2018 Jed Mahonis Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PCARequestTableViewController.h"

@interface SendDirectRequestTableViewController : UITableViewController <UITextFieldDelegate, UITextViewDelegate, CLLocationManagerDelegate, GMSAutocompleteViewControllerDelegate, UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIPickerView *statusPicker;
@property (weak, nonatomic) IBOutlet UIPickerView *statusPicker2;
@property BOOL statusPickerVisible;
@property (nonatomic, strong) User *currentUser;
@property (nonatomic, strong) NSString *addressString;
@property (nonatomic , weak) IBOutlet UIToolbar *keyboardToolbar;
@property (nonatomic, strong) CLLocationManager *locManager;
@property (nonatomic, strong) PCA *pca;

@property (nonatomic, weak) IBOutlet UIButton *goToGoogleAutoCompleteButton;

@end
