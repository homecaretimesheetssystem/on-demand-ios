//
//  Session.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 2/17/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Service.h"
#import "PCA.h"
#import "Recipient.h"
#import "Agency.h"
#import "User.h"

@class Agency, Service, User, Recipient;




@interface Session : NSObject <NSCopying, NSCoding>


//Variables
@property (strong, nonatomic) Service *service;
@property (strong, nonatomic) PCA *pca;
@property (strong, nonatomic) NSMutableArray *recipientTimesheetsArray;
@property (strong, nonatomic) NSMutableArray *timesheetIDsArray;



//Other Session Data
@property (strong, nonatomic) NSNumber *idNumber;
@property (strong, nonatomic) NSNumber *continueTimesheetNumber;
@property (strong, nonatomic) NSNumber *currentTimesheetNumber;
@property (strong, nonatomic) NSNumber *currentTimesheetID;
@property (strong, nonatomic) NSNumber *verificationID;
@property (strong, nonatomic) NSNumber *flagForFurtherInvestigation;
@property (strong, nonatomic) NSNumber *verificationIsRequiredForWorkerAndRecipient;
@property (strong, nonatomic) NSString *fileURL;
@property (strong, nonatomic) NSString *dateEnding;
@property (strong, nonatomic) NSString *datesInHospital;

@property (strong, nonatomic) NSMutableArray *arrayOfSessions;

@property (nonatomic, strong) NSMutableArray *selectedRecipients;

//-(instancetype)initWithDictionary:(NSDictionary *)dict andIndex:(long)index andUserType:(UserType)userType;

//-(void)resetLocalValues;


@end
