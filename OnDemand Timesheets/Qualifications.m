//
//  Qualifications.m
//  OnDemand Timesheets
//
//  Created by Joey Kjornes on 3/21/17.
//  Copyright © 2017 Jed Mahonis Group. All rights reserved.
//

#import "Qualifications.h"

@implementation Qualifications

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
//    NSLog(@"dictionary in qualifications %@", dictionary);
    if (self == [super init]) {
        
        if ([dictionary objectForKey:@"id"] != [NSNull null]) {
            _qualificationID = dictionary[@"id"];
        }else{
            _qualificationID = [NSNumber numberWithLong:0];
        }
        
        if ([dictionary objectForKey:@"qualification"] != [NSNull null]) {
            _qualificationName = dictionary[@"qualification"];
        }else{
            _qualificationName = @"";
        }
    }
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self == [super init]) {
        
        _qualificationName = [aDecoder decodeObjectForKey:@"qualification"];
        _qualificationID = [aDecoder decodeObjectForKey:@"id"];
        
        
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:_qualificationName forKey:@"qualification"];
    [aCoder encodeObject:_qualificationID forKey:@"id"];
    
}

-(NSString *)description {
    return [NSString stringWithFormat:@"Qualification ID:%@, Qualification Name:%@", _qualificationID, _qualificationName];
}

@end
