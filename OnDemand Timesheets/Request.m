//
//  Request.m
//  Homecare Timesheet Demo
//
//  Created by Joey Kjornes on 2/14/17.
//  Copyright © 2017 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "Request.h"

@implementation Request


-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
    NSLog(@"dictionary in request %@", dictionary);
    if (self == [super init]) {
        
        if ([dictionary objectForKey:@"startShiftTime"] != [NSNull null]) {
            _shiftStartTime = dictionary[@"startShiftTime"];
        }else{
            _shiftStartTime = @"";
        }
        
        if ([dictionary objectForKey:@"endShiftTime"] != [NSNull null]) {
            _shiftEndTime = dictionary[@"endShiftTime"];
        }else{
            _shiftEndTime = @"";
        }
        
        if ([dictionary objectForKey:@"address"] != [NSNull null]) {
            _recipientAddress = dictionary[@"address"];
        }else{
            _recipientAddress = @"";
        }
        
        if ([dictionary objectForKey:@"id"] != [NSNull null]) {
            _requestID = dictionary[@"id"];
        }else{
            _requestID = [NSNumber numberWithLong:0];
        }
        
        if ([dictionary objectForKey:@"gender_preference"] != [NSNull null]) {
            _genderPreference = dictionary[@"gender_preference"];
        }else{
            _genderPreference = [NSNumber numberWithLong:0];
        }
        
        if ([dictionary objectForKey:@"recipient"] != [NSNull null]) {
            _recipient = [[Recipient alloc] initWithDictionary:dictionary[@"recipient"]];

        }
        
//        if (dictionary[@"rsvps"]) {
       //_rsvp = [[RSVP alloc] initWithDictionary:dictionary[@"rsvps"]];
        for (NSDictionary *dict in dictionary[@"rsvps"]) {
            _rsvp = [[RSVP alloc] initWithDictionary:dict];
        }
        if (dictionary[@"additional_requirements"] && dictionary[@"additional_requirements"] != [NSNull null]) {
            _additionalRequirements = dictionary[@"additional_requirements"];
        } else {
            _additionalRequirements = @" ";
        }
//        }
     //   NSMutableArray *mutArray = [NSMutableArray new];
//        for (NSDictionary *rsvpDict in dictionary[@"rsvps"]){
//            
//            _rsvp = [[RSVP alloc] initWithDictionary:rsvpDict];
//            [_bigAssArray addObject:_rsvp];
//        }
     /*
        NSArray *rsvpArr = [dictionary valueForKey:@"rsvps"];
        NSMutableArray *allRSVPArray = [NSMutableArray new];
        NSMutableArray *availableShiftsArray = [NSMutableArray new];
        NSMutableArray *takenShiftsArray = [NSMutableArray new];
        for (NSDictionary *dict in rsvpArr) {
            RSVP *rsvp = [[RSVP alloc] initWithDictionary:dict];
            [allRSVPArray addObject:rsvp];
            NSLog(@"all rsvp array %@", allRSVPArray);
            if ([rsvp.accepted isEqualToNumber:@1]) {
                [takenShiftsArray addObject:rsvp];
            }else if ([rsvp.declined isEqualToNumber:@1]){
                [availableShiftsArray addObject:rsvp];
            }
        }
      */
//        
//        
//        _rsvpArray = [allRSVPArray mutableCopy];
//        _takenArray = [takenShiftsArray mutableCopy];
//        _availableArray = [availableShiftsArray mutableCopy];

    }
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self == [super init]) {
        _shiftStartTime = [aDecoder decodeObjectForKey:@"startShiftTime"];
        _shiftEndTime = [aDecoder decodeObjectForKey:@"endShiftTime"];
        _recipientAddress = [aDecoder decodeObjectForKey:@"address"];
        _requestID = [aDecoder decodeObjectForKey:@"id"];
        _genderPreference = [aDecoder decodeObjectForKey:@"gender_preference"];
        _additionalRequirements = [aDecoder decodeObjectForKey:@"additional_requirements"];
        _recipient = [aDecoder decodeObjectForKey:@"recipient"];
        _rsvp = [aDecoder decodeObjectForKey:@"rsvp"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_shiftStartTime forKey:@"startShiftTime"];
    [aCoder encodeObject:_shiftEndTime forKey:@"endShiftTime"];
    [aCoder encodeObject:_requestID forKey:@"id"];
    [aCoder encodeObject:_recipientAddress forKey:@"address"];
    [aCoder encodeObject:_genderPreference forKey:@"gender_preference"];
    [aCoder encodeObject:_additionalRequirements forKey:@"additional_requirements"];
    [aCoder encodeObject:_recipient forKey:@"recipient"];
    [aCoder encodeObject:_rsvp forKey:@"rsvp"];
}

-(NSString *)description {
    return [NSString stringWithFormat:@"ID:%@, Address:%@, Shift Start Time:%@, Shift End Time:%@, Gender Preference:%@, AdditionalRequirements:%@\r   Recipient:%@", _requestID, _recipientAddress, _shiftStartTime, _shiftEndTime, _genderPreference, _additionalRequirements, _recipient];
}

@end
