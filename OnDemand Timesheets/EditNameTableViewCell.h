//
//  EditNameTableViewCell.h
//  OnDemand Timesheets
//
//  Created by Joey Kjornes on 3/17/17.
//  Copyright © 2017 Jed Mahonis Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditNameTableViewCell : UITableViewCell <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UILabel *firstNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *lastNameLabel;
@property (nonatomic, weak) IBOutlet UITextField *firstNameTextfield;
@property (nonatomic, weak) IBOutlet UITextField *lastNameTextField;

@property (nonatomic, weak) IBOutlet UITextView *aboutMeTextView;
@property (nonatomic, weak) IBOutlet UITextView *placeholderTextView;

@end
