//
//  AcceptedShiftViewController.h
//  Homecare Timesheet Demo
//
//  Created by Joey Kjornes on 2/8/17.
//  Copyright © 2017 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import "SubmitETABeginShiftTableViewCell.h"
#import <CoreLocation/CoreLocation.h>
#import "ErrorAlertController.h"

@interface AcceptedShiftViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, CLLocationManagerDelegate>

@property (nonatomic, retain) NSArray *acceptedJobArray;
@property (nonatomic) long currentIndex;
@property (nonatomic, strong) User *currentUser;
@property (nonatomic, strong) NSString *etaString;
@property (nonatomic, strong) NSNumber *requestIDNumber;
@property (nonatomic, strong) NSNumber *RSVPIDNumber;
@property (nonatomic, strong) CLLocationManager *locManager;
@property (nonatomic, strong) NSString *latitudeCoord;
@property (nonatomic, strong) NSString *longitudeCoord;
@property (nonatomic, strong) NSString *cancelationReasonString;
@property (nonatomic) BOOL *isFromUpcomingShift;
@property (nonatomic, strong) NSMutableArray *rsvpArray;
@property (nonatomic) BOOL isFromShiftDetailScreen;

-(void)backButtonPressed;


@end
