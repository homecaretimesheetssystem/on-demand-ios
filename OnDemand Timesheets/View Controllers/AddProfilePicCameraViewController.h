//
//  AddProfilePicCameraViewController.h
//  MJB
//
//  Created by Scott Mahonis on 1/24/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "UIImage + fixOrientation.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "UIImage+Resize.h"
#import "AppDelegate.h"
#import <Photos/Photos.h>
#import "Session.h"
#import "User.h"
#import <CoreLocation/CoreLocation.h>
@class Session;

@protocol AddProfilePicCameraViewControllerDelegate <NSObject>

@end

@interface AddProfilePicCameraViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UIAlertViewDelegate, UIGestureRecognizerDelegate,  AVCaptureMetadataOutputObjectsDelegate, UIScrollViewDelegate, CLLocationManagerDelegate> {
    BOOL containsImage; float yOrg; CGFloat height; CGFloat width;  bool four;
    
    //Camera Vars
    CGRect flashFrame;  UIButton *flashButton; NSString *flashButtonTitle; UIButton *chooseAuto; UIButton *chooseOn; UIButton *chooseOff; UIView *circleView; UIView *whiteView;
    
}
@property (nonatomic, weak) CLLocationManager *locManager;

@property (nonatomic, retain) IBOutlet UIView *cameraOverlayBottomView;
@property (nonatomic, retain) IBOutlet UIImagePickerController *imagePicker;
@property (nonatomic, strong) IBOutlet UIView *cameraView;
@property (nonatomic, strong) Session *currentSession;
@property (nonatomic, strong) User *currentUser;
@property (nonatomic, strong) id<AddProfilePicCameraViewControllerDelegate>delegate;
//@property (nonatomic) NSString *address;
@property (nonatomic) NSNumber *lat;
@property (nonatomic) NSNumber *lng;

-(IBAction)openCamera:(id)sender;

@end
