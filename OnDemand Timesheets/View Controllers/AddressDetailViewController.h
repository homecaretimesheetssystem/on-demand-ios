//
//  AddressDetailViewController.h
//  Mimi
//
//  Created by Scott Mahonis on 2/16/16.
//  Copyright © 2016 The Jed Mahonis Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@import CoreLocation;

@interface AddressDetailViewController : UIViewController <MKMapViewDelegate, CLLocationManagerDelegate>

@property (nonatomic, weak) IBOutlet UILabel *addressTopLabel;
@property (nonatomic, weak) IBOutlet UILabel *addressBottomLabel;
@property (nonatomic, weak) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) CLLocationManager *locManager;
@property (nonatomic, strong) NSString *userLatitude;
@property (nonatomic, strong) NSString *userLongitude;
@property (nonatomic, strong) NSString *recipientLatitude;
@property (nonatomic, strong) NSString *recipientLongitude;



@end
