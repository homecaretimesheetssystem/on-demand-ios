//
//  ShiftDetailViewController.m
//  Homecare Timesheet Demo
//
//  Created by Joey Kjornes on 2/8/17.
//  Copyright © 2017 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "ShiftDetailViewController.h"
#import "ShiftDetailTableViewCell.h"
#import "AcceptButtonTableViewCell.h"
#import "AcceptedShiftViewController.h"
#import "Request.h"
#import "ChooseETATableViewCell.h"
#import "NetworkingHelper.h"
#import "RSVP.h"

@interface ShiftDetailViewController ()

@end

@implementation ShiftDetailViewController {
    
    BOOL shiftAccepted;
    BOOL etaSuccessfullySubmitted;
    BOOL hideETAPicker;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Shift";
    _locManager = [[CLLocationManager alloc] init];
    _locManager.delegate = self;
    _locManager.distanceFilter = kCLDistanceFilterNone;
    _locManager.desiredAccuracy = kCLLocationAccuracyBest;
        [self.locManager requestWhenInUseAuthorization];
    [_locManager startUpdatingLocation];
    shiftAccepted = NO;
    hideETAPicker = YES;
    etaSuccessfullySubmitted = NO;
    


    
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



//ShiftDetailViewController

#pragma mark - Data


#pragma mark - UITableView Data Source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }
    else if (section == 1 && shiftAccepted == NO) {
    return 1;
    }return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.0f;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0) {
        Request *request = self.selectedJobArray[_currentIndex];
        return 306 + [self heightForTextViewForString:request.additionalRequirements];
    }else if (indexPath.section == 1 && indexPath.row == 0 && shiftAccepted == NO){
    return self.view.frame.size.height * 0.40;
    }else if (indexPath.section == 1 && indexPath.row == 0 && shiftAccepted == YES){
        return 0.01;
    }else if (indexPath.section == 1 && indexPath.row == 1 && shiftAccepted==YES && hideETAPicker == YES){
        return 60;
    }else if (indexPath.section == 1 && indexPath.row == 1 && shiftAccepted==YES && hideETAPicker == NO){
        return 300;
    }
    return 200;
}

-(CGFloat)heightForTextViewForString:(NSString *)string {
    UITextView *tv = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width - 16, 1)];
    tv.attributedText = [[NSAttributedString alloc] initWithString:string attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.]}];
    [tv sizeToFit];
    return tv.frame.size.height;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 && indexPath.row == 0) {
        ShiftDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"shiftDetailTableViewCell"];
        Request *request = _selectedJobArray[self.currentIndex];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:kDateFormat];
        NSDate *date = [formatter dateFromString:request.shiftStartTime];
        NSDate *date2 = [formatter dateFromString:request.shiftEndTime];
        [formatter setDateFormat:kDisplayDateFormat];
        NSString *output = [formatter stringFromDate:date];
        NSString *output2 = [formatter stringFromDate:date2];
        self.requestIDNumber = request.requestID;
        cell.shiftTimesLabel.text = [NSString stringWithFormat:@"%@ - %@", output, output2];
        cell.jobTitleLabel.text = @"Personal Care Service";
        cell.qualificationsTextView.text = request.additionalRequirements;
        CLLocation *currentLoc = self.locManager.location;
        
        CLGeocoder* geocoder = [[CLGeocoder alloc] init];
        [geocoder geocodeAddressString:request.recipientAddress
                     completionHandler:^(NSArray* placemarks, NSError* error){
                         for (CLPlacemark* aPlacemark in placemarks)
                         {
                             double latString = aPlacemark.location.coordinate.latitude;
                             double longString = aPlacemark.location.coordinate.longitude;
                             CLLocation *recipLoc = [[CLLocation alloc] initWithLatitude:latString longitude:longString];
                             if (currentLoc.coordinate.latitude != 0.0 && currentLoc.coordinate.longitude != 0.0) {
                                 CLLocationDistance meters = [recipLoc distanceFromLocation:currentLoc];
                                 NSLog(@"meters away %.2f", meters);
                                 if (meters == -1 || meters/1760 == -0) {
                                     cell.distanceLabel.text = @"?? miles away";
                                 }else{
                                     
                                     
                                     cell.distanceLabel.text = [NSString stringWithFormat:@"%.2f miles away", meters/1760];
                                 }
                             } else {
                                 [geocoder reverseGeocodeLocation:recipLoc completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
                                     if (!error) {
                                         if (placemarks.count > 0) {
                                             CLPlacemark *placeMark = placemarks[0];
                                             cell.distanceLabel.text = [NSString stringWithFormat:@"%@, %@", [placeMark locality], [placeMark administrativeArea]];
                                         } else {
                                             cell.distanceLabel.text = @"?? miles away";
                                         }
                                     } else {
                                         cell.distanceLabel.text = @"?? miles away";
                                     }
                                 }];
                             }
                         }
                     }];
        if (shiftAccepted == NO) {
            cell.acceptedButton.hidden = YES;
            cell.availableButton.hidden = NO;
        }else if (shiftAccepted == YES){
            cell.acceptedButton.hidden = NO;
            cell.availableButton.hidden = YES;
            
        }
     //   cell.delegate = self;
        return cell;
    } else if (indexPath.section == 1 && indexPath.row == 0) {
        AcceptButtonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"acceptShiftTableViewCell"];
        return cell;
    }else if (indexPath.section == 1 && indexPath.row == 1){
        ChooseETATableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"submitETATableViewCellShift"];
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setDateFormat:kDateFormat];

        NSString *dateStr = [format stringFromDate:cell.submitETADatePicker.date];
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDate *currentDate = [self roundToNearestQuarterHour:[NSDate date]];
        NSDateComponents *comps = [[NSDateComponents alloc] init];
        [comps setDay:0];
        NSDate *minDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
        [comps setHour:30];
        NSDate *maxDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
        [cell.submitETADatePicker setMaximumDate:maxDate];
        [cell.submitETADatePicker setMinimumDate:minDate];
        self.etaString = dateStr;
        
        [[UITableView appearanceWhenContainedIn:[UIDatePicker class], nil] setBackgroundColor:nil]; // for iOS 8
        if (hideETAPicker == YES) {
            cell.submitETAButton.hidden = YES;
            cell.chooseETAButton.hidden = NO;
        }else if (hideETAPicker == NO){
            cell.submitETAButton.hidden = NO;
            cell.chooseETAButton.hidden = YES;
        }
        return cell;
    }
    
    
    return nil;
}

-(IBAction)acceptShiftButtonPressed:(id)sender {
    
 //   UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Confirm" message:@"Do you want to accept this shift? Choosing Yes will require you to enter your ETA." preferredStyle:UIAlertControllerStyleAlert];
 //   [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:NULL]];
  //  [alert addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        shiftAccepted = YES;
   //     hideETAPicker = NO;
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Confirm" message:@"By accepting this Shift, you agree to arrive at the stated time and provide services according to recipient's request, and in accordance with all rules and laws rules related to services." preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:NULL]];
    [alert addAction:[UIAlertAction actionWithTitle:@"I Agree" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSDate *currentDate = [NSDate date];
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setDateFormat:kDateFormat];
        NSString *dateStr = [format stringFromDate:currentDate];

        [NetworkingHelper postAnRSVPWithPCAID:User.currentUser.pca.idNumber andRequestID:self.requestIDNumber andETA:dateStr andCancellationReason:@"" andAcceptedBool:YES andDeclinedBool:NO andSuccess:^(id responseObject) {
            //  NSLog(@"success rsvp %@", responseObject);
            //       NSMutableArray *mutArr = [NSMutableArray new];
            //     for (NSDictionary *dict in responseObject) {
            //       RSVP *rsvp = [[RSVP alloc] initWithDictionary:dict];
            //        [mutArr addObject:rsvp];
            //    }
            //    NSLog(@"mut array %@", mutArr);
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Success" message:@"Your RSVP has been sent to the recipient. You will be notified if they cancel your RSVP." preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self performSegueWithIdentifier:@"acceptShiftSegue" sender:self];
            }]];
            alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
            [self presentViewController:alert animated:YES completion:NULL];
            
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            NSLog(@"failure %@", error);
            id responseObject = [NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:0 error:nil];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:[NSString stringWithFormat:@"%@", responseObject[@"error"]] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.tableView reloadData];
            }]];
            
            alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
            [self presentViewController:alert animated:YES completion:NULL];

        }];
        
    }]];
    alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
    [self presentViewController:alert animated:YES completion:NULL];
    
 //   }]];
  //  alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
  //  [self presentViewController:alert animated:YES completion:NULL];

    
    
}

-(IBAction)submitETAButtonPressed:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Confirm" message:@"By accepting this Shift, you agree to arrive at the stated time and provide services according to recipient's request, and in accordance with all rules and laws rules related to services." preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:NULL]];
    [alert addAction:[UIAlertAction actionWithTitle:@"I Agree" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [NetworkingHelper postAnRSVPWithPCAID:User.currentUser.pca.idNumber andRequestID:self.requestIDNumber andETA:self.etaString andCancellationReason:@"" andAcceptedBool:YES andDeclinedBool:NO andSuccess:^(id responseObject) {
          //  NSLog(@"success rsvp %@", responseObject);
     //       NSMutableArray *mutArr = [NSMutableArray new];
       //     for (NSDictionary *dict in responseObject) {
        //       RSVP *rsvp = [[RSVP alloc] initWithDictionary:dict];
        //        [mutArr addObject:rsvp];
        //    }
        //    NSLog(@"mut array %@", mutArr);

            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Success" message:@"Your RSVP has been sent to the recipient. You will be notified if they cancel your RSVP." preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self performSegueWithIdentifier:@"acceptShiftSegue" sender:self];
            }]];
            alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
            [self presentViewController:alert animated:YES completion:NULL];

            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            NSLog(@"failure %@", error);
            id responseObject = [NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:0 error:nil];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:[NSString stringWithFormat:@"%@", responseObject[@"error"]] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.tableView reloadData];
            }]];
            
            alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
            [self presentViewController:alert animated:YES completion:NULL];

        }];

    }]];
    alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
    [self presentViewController:alert animated:YES completion:NULL];
    
    
}
#pragma mark picker stuff

- (NSDate *)roundToNearestQuarterHour:(NSDate *)date{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    unsigned unitFlags = NSCalendarUnitYear| NSCalendarUnitMonth | NSCalendarUnitDay |  NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitWeekday | NSCalendarUnitWeekdayOrdinal | NSCalendarUnitWeekOfYear;
    NSDateComponents *components = [calendar components:unitFlags fromDate:date];
    NSInteger roundedToQuarterHour = ceil((components.minute/15.0)) * 15;
    components.minute = roundedToQuarterHour;
    return [calendar dateFromComponents:components];
}

-(IBAction)chooseETAButtonPressed:(id)sender{
    if (hideETAPicker == YES) {
        hideETAPicker = NO;
        [self.tableView reloadData];
    }else if (hideETAPicker == NO){
        hideETAPicker = YES;
        [self.tableView reloadData];
    }
}


-(IBAction)pickerTouched:(id)sender{
    
    [self.tableView reloadData];
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"acceptShiftSegue"]) {
        
        // Get destination view
        AcceptedShiftViewController *vc = [segue destinationViewController];
        vc.acceptedJobArray = _selectedJobArray;
        vc.requestIDNumber = self.requestIDNumber;
        vc.currentIndex = self.currentIndex;
        vc.isFromShiftDetailScreen = YES;

        
        
    }
}


-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    [self.locManager stopUpdatingLocation];
    self.locManager = manager;
    if (locations.count == 1) {
        NSLog(@"self.locManager.location:%@", self.locManager.location);
    }
    
    
}

//-(UITableViewCell *)tableView:(UITableView *)tableView loadingCellForRowAtIndexPath:(NSIndexPath *)indexPath andText:(NSString *)text {
//    LoadingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"loadingCell"];
//    if (cell.activityIndicator.isAnimating) {
//        [cell.activityIndicator stopAnimating];
//    }
//    cell.statusLabel.text = text;
//    return cell;
//}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.section == 1) {
//        [self goToShiftDetail];
//        //        if (_status == DataStatus_Error) {
//        //            [self loadJobs];
//        //        }
//    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
