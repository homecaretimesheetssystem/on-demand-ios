//
//  PCARequestTableViewController.h
//  Homecare Timesheet Demo
//
//  Created by Joey Kjornes on 2/9/17.
//  Copyright © 2017 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import <CoreLocation/CoreLocation.h>
#import <GooglePlaces/GooglePlaces.h>

@interface PCARequestTableViewController : UITableViewController <UITextFieldDelegate, UITextViewDelegate, CLLocationManagerDelegate, GMSAutocompleteViewControllerDelegate, UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIPickerView *statusPicker;
@property (weak, nonatomic) IBOutlet UIPickerView *statusPicker2;
@property BOOL statusPickerVisible;
@property (nonatomic, strong) User *currentUser;
@property (nonatomic, strong) NSString *addressString;
@property (nonatomic , weak) IBOutlet UIToolbar *keyboardToolbar;
@property (nonatomic, strong) CLLocationManager *locManager;

@property (nonatomic, weak) IBOutlet UIButton *goToGoogleAutoCompleteButton;


@end
