//
//  ShiftDetailViewController.h
//  Homecare Timesheet Demo
//
//  Created by Joey Kjornes on 2/8/17.
//  Copyright © 2017 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "User.h"

@interface ShiftDetailViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate>


//@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (strong, nonatomic) UILabel *shiftTimesLabel;
@property (strong, nonatomic) UILabel *jobTitleLabel;
@property (strong, nonatomic) UILabel *distanceLabel;
@property (strong, nonatomic) UITextView *qualificationsTextView;
@property (nonatomic, retain) NSArray *selectedJobArray;
@property (nonatomic) long currentIndex;
@property (nonatomic, strong) CLLocationManager *locManager;
@property (nonatomic, strong) User *currentUser;
@property (nonatomic, strong) NSString *etaString;
@property (nonatomic, strong) NSNumber *requestIDNumber;




@end
