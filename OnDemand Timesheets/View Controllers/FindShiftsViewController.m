//
//  FindShiftsViewController.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 10/17/16.
//  Copyright © 2016 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "FindShiftsViewController.h"
#import "LoadingTableViewCell.h"
#import "JobTableViewCell.h"
#import "EditWorkerProfileTableViewController.h"
#import "ShiftDetailViewController.h"
#import "AvailabilityTableViewCell.h"
#import "NetworkingHelper.h"
#import "UpcomingShiftsTableViewCell.h"
#import "AcceptedShiftViewController.h"
#import "Qualifications.h"
#import "Counties.h"
#import "HomeTableViewController.h"
#import "LoginTableViewController.h"

@interface FindShiftsViewController ()

@end

@implementation FindShiftsViewController
    


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Find Jobs";
    _currentUser = [User currentUser];
    NSLog(@"currentUser.yearsOfExperience:%@", _currentUser.pca.yearsOfExperience);
    _availableJobsArray = [NSMutableArray new];
    _allRequestsArray = [NSMutableArray new];
    _upcomingShiftsArray = [NSMutableArray new];
    _acceptedJobsArray = [NSMutableArray new];
//    [self checkSwitch];
    _locManager = [[CLLocationManager alloc] init];
    _locManager.delegate = self;
    _locManager.distanceFilter = kCLDistanceFilterNone;
    _locManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    //This does work, but I found its not good coding practice to write an if statement without any {}, it could mess you up if add anything in between those lines.
    
    [self.locManager requestWhenInUseAuthorization];
    [_locManager startUpdatingLocation];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshJobsList:) name:@"push_type=1" object:nil];
    
}


-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    [self checkSwitch];
    [self.tableView reloadData];
    
    _currentUser = [User currentUser];

    
    if (_currentUser.pca.qualificationsArray.count == 0) {
           UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Update Profile" message:@"You are much more likely to see shifts here if you update your qualifications." preferredStyle:UIAlertControllerStyleAlert];
           [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:NULL]];
           [alert addAction:[UIAlertAction actionWithTitle:@"Go To Profile" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
               EditWorkerProfileTableViewController *vc = [[UIStoryboard storyboardWithName:@"OnDemandWorker" bundle:nil] instantiateViewControllerWithIdentifier:@"EditWorkerProfileTableViewController"];
               [self.navigationController showViewController:vc sender:self];
           }]];
           
           alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
           [self presentViewController:alert animated:YES completion:NULL];
    }
    
}


-(void)checkSwitch {
    
    
    _status = [_currentUser.pca.isAvailable isEqualToNumber:@1] ? DataStatus_Loading : DataStatus_NotAvailable;
    if (_status == DataStatus_Loading) {
        [self loadJobs];
    }
    
    [self.tableView reloadData];
}


- (void)refreshJobsList:(NSNotification *)notification{
    
    [self loadJobs];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)dismissPopover {
    [self dismissViewControllerAnimated:YES completion:NULL];
}


-(IBAction)settingButtonPressed:(UIButton *)button {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [[actionSheet popoverPresentationController] setSourceView:self.view];
    [[actionSheet popoverPresentationController] setSourceRect:CGRectMake((self.view.frame.size.width) - 50,-200,100,200)];
    [[actionSheet popoverPresentationController] setPermittedArrowDirections:UIPopoverArrowDirectionUp];

    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Edit Profile" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self editProfileTouched];
    }]];
    
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Log Out" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self logoutTouched];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:YES completion:^{

        }];
    }]];

    [self presentViewController:actionSheet animated:YES completion:nil];
    
}


-(void)editProfileTouched{
    EditWorkerProfileTableViewController *vc = [[UIStoryboard storyboardWithName:@"OnDemandWorker" bundle:nil] instantiateViewControllerWithIdentifier:@"EditWorkerProfileTableViewController"];
    [self.navigationController showViewController:vc sender:self];
}

-(void)backToHome{
    [self.navigationController popViewControllerAnimated:YES];

    
}

-(void)logoutTouched {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Confirm" message:@"Are you sure you want to logout?" preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:NULL]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [UIApplication sharedApplication].keyWindow.rootViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"mainNavCont"];
    }]];
    
    alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
    [self presentViewController:alert animated:YES completion:NULL];
}

-(void)goToShiftDetail{
    
//    ShiftDetailViewController *vc = [[UIStoryboard storyboardWithName:@"OnDemandWorker" bundle:nil] instantiateViewControllerWithIdentifier:@"ShiftDetailViewController"];
//    [vc.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
//    vc.navigationController.navigationBar.shadowImage = nil;
//    
//    [self.navigationController pushViewController:vc animated:YES];
}
//ShiftDetailViewController

#pragma mark - Data

-(void)loadJobs {
    // Make the call to the server here to get a list of available jobs
    _status = DataStatus_Loading;
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationNone];
    [NetworkingHelper findAvailableShiftsPCA:User.currentUser.pca.idNumber andSuccess:^(id responseObject) {
        NSLog(@"success response object in view controller %@", responseObject);
        NSMutableArray *mutArr = [NSMutableArray new];
        for (NSDictionary *dict in responseObject) {
            Request *request = [[Request alloc] initWithDictionary:dict];
           // if (request.rsvpArray.count == 0 || request.availableArray.count > 0) {
                NSDate *currentDate = [[NSDate alloc] init];
                NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:kDateFormat];
                NSDate *etaDate = [dateFormatter dateFromString:request.shiftEndTime];
                NSComparisonResult result;
                result = [currentDate compare:etaDate]; // comparing two date
                if(result==NSOrderedAscending)
                    NSLog(@"today is less");
                else if(result==NSOrderedDescending)
                    NSLog(@"newDate is less");
                else if (result == NSOrderedSame) {
                    NSLog(@"Both dates are same");
                }
                
               // if (result == NSOrderedSame || result == NSOrderedAscending) {
            
            if (!request.rsvp || [request.rsvp.declined isEqual:@1]) {
                NSLog(@"request rsvparray count %lu", (unsigned long)request.rsvpArray.count);
                NSLog(@"req rsvp declined %@", request.rsvp.declined);
                
            [mutArr addObject:request];
            }
                }
        //    }
      //  }
        self->_availableJobsArray = [mutArr mutableCopy];
        if (self->_availableJobsArray.count > 0) {
        self->_status = DataStatus_AtLeastOneJobIsAvailable;
        }else if (self->_availableJobsArray.count == 0){
            self->_status = DataStatus_None;
        }
        
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationNone];
//        NSLog(@"available jobs array %@", _availableJobsArray);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"failure view controller error %@", error);
        self->_status = DataStatus_Error;
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationNone];
    }];
    
}

#pragma mark - UITableView Data Source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 1 && _status == DataStatus_AtLeastOneJobIsAvailable) {
      //  NSLog(@"available job count %lu", (unsigned long)_availableJobsArray.count);
        return _availableJobsArray.count;
//    }else if (section == 2 && _upcomingStatus == UpcomingShiftStatus_AtLeastOneShift){
//        return _upcomingShiftsArray.count;
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 100;
    }
    return 100;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 1) {
        return @"Available Shifts";
    }
    return nil;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        AvailabilityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"availibilityCell"];
        cell.iAmAvailableSwitch.on = ([_currentUser.pca.isAvailable isEqualToNumber:@YES]);
        cell.borderOutlineView.layer.borderColor = [UIColor grayColor].CGColor;
        cell.borderOutlineView.layer.borderWidth = 1.0f; //make border 1px thick
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

     //   cell.delegate = self;
        return cell;
    } else if (indexPath.section == 1) {
        if (_status == DataStatus_Error) {
            return [self tableView:tableView loadingCellForRowAtIndexPath:indexPath andText:@"Loading Error. Tap to retry."];
        } else if (_status == DataStatus_None) {
            return [self tableView:tableView loadingCellForRowAtIndexPath:indexPath andText:@"There are no jobs currently available. You will be notified as jobs become available."];
        } else if (_status == DataStatus_NotAvailable) {
            return [self tableView:tableView loadingCellForRowAtIndexPath:indexPath andText:@"Make yourself available to view jobs."];
        } else if (_status == DataStatus_Loading) {
            LoadingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"loadingCell"];
            [cell.activityIndicator startAnimating];
            return cell;
        } else if (_status == DataStatus_AtLeastOneJobIsAvailable && indexPath.row >=0) {
            JobTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"jobCell"];
            // Job stuff here
            Request *request = _availableJobsArray[indexPath.row];
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:kDateFormat];
            NSDate *date = [formatter dateFromString:request.shiftStartTime];
            NSDate *date2 = [formatter dateFromString:request.shiftEndTime];
            [formatter setDateFormat:kDisplayDateFormat];
            NSString *output = [formatter stringFromDate:date];
            NSString *output2 = [formatter stringFromDate:date2];
            [tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];

            cell.startTimeLabel.text = [NSString stringWithFormat:@"%@ - %@", output, output2];
          //  cell.shiftTypeLabel.text = @"PCA";
            CLLocation *currentLoc = self.locManager.location;
            
            CLGeocoder* geocoder = [[CLGeocoder alloc] init];
            [geocoder geocodeAddressString:request.recipientAddress
                         completionHandler:^(NSArray* placemarks, NSError* error){
                             for (CLPlacemark* aPlacemark in placemarks)
                             {
                                 double latString = aPlacemark.location.coordinate.latitude;
                                 double longString = aPlacemark.location.coordinate.longitude;
                                 CLLocation *recipLoc = [[CLLocation alloc] initWithLatitude:latString longitude:longString];
                                 if (currentLoc.coordinate.latitude != 0.0 && currentLoc.coordinate.longitude != 0.0) {
                                     CLLocationDistance meters = [recipLoc distanceFromLocation:currentLoc];
                                     NSLog(@"meters away %.2f", meters);
                                     if (meters == -1 || meters/1760 == -0) {
                                         cell.shiftDistanceLabel.text = @"?? miles away";
                                     }else{
                                         
                                         
                                         cell.shiftDistanceLabel.text = [NSString stringWithFormat:@"%.2f miles away", meters/1760];
                                     }
                                 } else {
                                     [geocoder reverseGeocodeLocation:recipLoc completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
                                         if (!error) {
                                             if (placemarks.count > 0) {
                                                 CLPlacemark *placeMark = placemarks[0];
                                                 cell.shiftDistanceLabel.text = [NSString stringWithFormat:@"%@, %@", [placeMark locality], [placeMark administrativeArea]];
                                             } else {
                                                 cell.shiftDistanceLabel.text = @"?? miles away";
                                             }
                                         } else {
                                             cell.shiftDistanceLabel.text = @"?? miles away";
                                         }
                                     }];
                                 }
                             }
                         }];
            

            cell.tag = indexPath.row;
            return cell;
        } else {
            return nil;
        }

    }
    return nil;
}

-(UITableViewCell *)tableView:(UITableView *)tableView loadingCellForRowAtIndexPath:(NSIndexPath *)indexPath andText:(NSString *)text {
    LoadingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"loadingCell"];
    if (cell.activityIndicator.isAnimating) {
        [cell.activityIndicator stopAnimating];
    }
    cell.statusLabel.text = text;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        NSLog(@"_status:%lu", (unsigned long)_status);
        if (_status == DataStatus_Error) {
            [self loadJobs];
        } 
    }
}


-(UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller {
    return UIModalPresentationNone;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showShiftDetail"]) {
        
        // Get destination view
        ShiftDetailViewController *vc = [segue destinationViewController];
        JobTableViewCell *cell = (JobTableViewCell *)sender;
        vc.selectedJobArray = _availableJobsArray;
        vc.currentIndex = cell.tag;
    }
   else if ([segue.identifier isEqualToString:@"optionPopoverSegue"]) {
        UserSettingsOptionsTableViewController *vc = [segue destinationViewController];
        vc.modalPresentationStyle = UIModalPresentationPopover;
        vc.popoverPresentationController.delegate = self;
        vc.delegate = self;
    }

        
}

-(IBAction)switchChanged:(id)sender {
    NSMutableArray *qualificationsArrayToPass = [NSMutableArray new];
    for (Qualifications *q in _currentUser.pca.qualificationsArray) {
        [qualificationsArrayToPass addObject:q.qualificationID];
    }
    NSMutableArray *countiesArrayToPass = [NSMutableArray new];
    for (Counties *c in _currentUser.pca.countyArray) {
        [countiesArrayToPass addObject:c.countyID];
    }
    
  //  _currentUser.pca.isAvailable = [NSNumber numberWithBool:![_currentUser.pca.isAvailable boolValue]];
    if ([_currentUser.pca.isAvailable isEqualToNumber:@0] || !_currentUser.pca.isAvailable || [_currentUser.pca.isAvailable isEqual:[NSNull null]]) {
        _currentUser.pca.isAvailable =@1;
    }else{
        _currentUser.pca.isAvailable =@0;
    }
   // = ![_currentUser.pca.isAvailable boolValue];
    [NetworkingHelper updatePCAProfileWithPCAID:_currentUser.pca.idNumber andFirstName:_currentUser.pca.firstName andLastName:_currentUser.pca.lastName andAddressString:_currentUser.pca.homeAddress andYearsOfExperience:_currentUser.pca.yearsOfExperience andGenderInt:_currentUser.pca.gender
        andQualificationsArray:qualificationsArrayToPass andCountiesArray:countiesArrayToPass andIsAvailable:_currentUser.pca.isAvailable andAboutMe:_currentUser.pca.aboutMe andSuccess:^(id responseObject) {
        [self->_currentUser save];
        [self checkSwitch];

    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        id responseObject = [NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:0 error:nil];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:[NSString stringWithFormat:@"%@", responseObject[@"error"]] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.tableView reloadData];
        }]];
        
        alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
        [self presentViewController:alert animated:YES completion:NULL];

    }];
    [self.tableView reloadData];
}


-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    [self.locManager stopUpdatingLocation];
    self.locManager = manager;
    if (locations.count > 0) {
        NSLog(@"self.locManager.location:%@", self.locManager.location);
    }

    
}
@end
