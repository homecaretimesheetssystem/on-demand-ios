//
//  AvailabilityTableViewCell.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 10/24/16.
//  Copyright © 2016 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AvailabilityTableViewCellDelegate <NSObject>

-(void)switchChanged:(id)sender;

@end

@interface AvailabilityTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UISwitch *iAmAvailableSwitch;
@property (nonatomic) id<AvailabilityTableViewCellDelegate>delegate;
@property (nonatomic, weak) IBOutlet UIView *borderOutlineView;

-(IBAction)switchChanged:(id)sender;

@end
