//
//  AddProfilePicCameraViewController.m
//  MJB
//
//  Created by Scott Mahonis on 1/24/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#define DEGREES_TO_RADIANS(x) (M_PI * x / 180.0)
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#import "AddProfilePicCameraViewController.h"
#import "LoadingView.h"
#import "NetworkingHelper.h"

@interface AddProfilePicCameraViewController () {
    NSData *picData;
}

@end

@implementation AddProfilePicCameraViewController

@synthesize  cameraOverlayBottomView, imagePicker, cameraView;
#pragma mark - View Delegate
- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"hideToolbar"];
    // Check if a user's launched this app before.
    [[NSUserDefaults standardUserDefaults] setValue:@".jpg" forKey:@"fileType"];
    
    
    
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    _currentUser = [User currentUser];
    _currentSession = _currentUser.currentSession;
    UIInterfaceOrientation deviceOrientation = [UIApplication sharedApplication].statusBarOrientation;
    if (UIInterfaceOrientationIsLandscape(deviceOrientation)) {
        height = self.view.bounds.size.width;
        width = self.view.bounds.size.height;
    } else {
        height = self.view.bounds.size.height;
        width = self.view.bounds.size.width;
    }
    four = CGSizeEqualToSize([[UIScreen mainScreen] preferredMode].size, CGSizeMake(640, 960));
    yOrg = [UIApplication sharedApplication].statusBarFrame.size.height + self.navigationController.navigationBar.frame.size.height;
    self.view.backgroundColor = [UIColor blackColor];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    [self setNeedsStatusBarAppearanceUpdate];
    self.title = @"Name Image";
//    if ([_currentUser.currentSession.currentTimecard.recipient.shouldRecordLocation isEqualToNumber:@YES]) {
//        AppDelegate *del = (AppDelegate *)[UIApplication sharedApplication].delegate;
//        _locManager = del.appLocationManager;
//        _locManager.delegate = self;
//        _locManager.desiredAccuracy = kCLLocationAccuracyBest;
//        [_locManager startUpdatingLocation];
//    }
    
    [self openCamera:self];
    
    //    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    NSLog(@"didReceiveMemoryWarning");
}


-(void)goToPhoneSettings {
    NSURL *appSettings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    [[UIApplication sharedApplication] openURL:appSettings];
}

#pragma mark - Camera Methods

-(void)getCameraPermissions {
//    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] == NO) {
//        // Get Photo Library Permission
//    } else {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] && [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusAuthorized) {
            [self openCamera:self];
        } else if ([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusNotDetermined) {
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                if (granted) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self openCamera:self];
                    });
                } else {
                    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Camera Access Denied" message:[NSString stringWithFormat:@"We can't access your camera. To give us access, go to Settings -> %@ and allow camera permission to use this feature.", [[NSBundle mainBundle] infoDictionary][@"CFBundleDisplayName"]] preferredStyle:UIAlertControllerStyleAlert];
                    [controller addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}]];
                    [controller addAction:[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self goToPhoneSettings];
                    }]];
                    [self presentViewController:controller animated:YES completion:NULL];
                }
                
            }];
        } else {
            UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Camera Access Denied" message:[NSString stringWithFormat:@"We can't access your camera. To give us access, go to Settings -> %@ and allow camera permission to use this feature.", [[NSBundle mainBundle] infoDictionary][@"CFBundleDisplayName"]] preferredStyle:UIAlertControllerStyleAlert];
            [controller addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}]];
            [controller addAction:[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self goToPhoneSettings];
            }]];
            [self presentViewController:controller animated:YES completion:NULL];
        }
//    }
    
}

-(IBAction)openCamera:(id)sender {
    [self.view endEditing:YES];
    self.navigationController.navigationBarHidden = YES;
    if (!imagePicker) {
        imagePicker = [UIImagePickerController new];
//        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
        imagePicker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
        imagePicker.showsCameraControls = NO;
        imagePicker.toolbarHidden = YES;
        imagePicker.isAccessibilityElement = YES;
        imagePicker.allowsEditing = YES;
        imagePicker.delegate = self;
    }
//    imagePicker.view.frame = CGRectMake(0, 64, width, height - 64);
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        imagePicker.view.frame = CGRectMake(0, 64, 500, 500);
    } else {
        imagePicker.view.frame = CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.width);
    }
    imagePicker.view.center = self.view.center;
    //    imagePicker.view.frame  = CGRectMake(0, 0, width, height + 64);
    //    imagePicker.view.transform = CGAffineTransformMakeScale(1.0, 1.55);
    if (IS_IPAD) {
        imagePicker.modalPresentationStyle = 0;
    }
    if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront]) {
        imagePicker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
    } else {
        imagePicker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
    }
    
    CGRect bottomToolBarFrame;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        bottomToolBarFrame = CGRectMake(0, self.imagePicker.view.frame.origin.y + self.imagePicker.view.frame.size.height, height, self.view.frame.size.height - (self.imagePicker.view.frame.origin.y + self.imagePicker.view.frame.size.height));
    } else {
        bottomToolBarFrame = CGRectMake(0, self.imagePicker.view.frame.origin.y + self.imagePicker.view.frame.size.height, width, self.view.frame.size.height - (self.imagePicker.view.frame.origin.y + self.imagePicker.view.frame.size.height));
    }
    
    for (UIView *subview in self.view.subviews) {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        if (subview.frame.size.height == screenRect.size.height && subview.frame.size.width == screenRect.size.width && subview.frame.origin.y == screenRect.origin.y && subview.frame.origin.x == screenRect.origin.x) {
            [subview removeFromSuperview];
        }
    }
    cameraView = [[UIView alloc] init];
    cameraView.frame = [[UIScreen mainScreen] bounds];
    cameraView.hidden = NO;
    cameraOverlayBottomView = [[UIView alloc] initWithFrame:bottomToolBarFrame];
    cameraOverlayBottomView.backgroundColor = [UIColor blackColor];
    circleView = [[UIView alloc] init];
    
    circleView.frame = CGRectMake(cameraOverlayBottomView.frame.size.width/2 - cameraOverlayBottomView.frame.size.height/2, 10, cameraOverlayBottomView.frame.size.height/2, cameraOverlayBottomView.frame.size.height/2);
    circleView.backgroundColor = [UIColor whiteColor];
    circleView.layer.cornerRadius = circleView.frame.size.height/2;
    circleView.layer.borderColor = [UIColor blackColor].CGColor;
    circleView.center = CGPointMake(cameraOverlayBottomView.center.x, cameraOverlayBottomView.frame.size.height/2);
    UIButton *circleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    circleButton.isAccessibilityElement = YES;
    circleButton.accessibilityLabel = @"Shutter.";
    circleButton.accessibilityHint = @"Takes the photo.";
    
    circleButton.frame = CGRectMake((circleView.frame.size.height * 0.8) * 0.125, (circleView.frame.size.height * 0.8) * 0.125, (circleView.frame.size.height * 0.8), (circleView.frame.size.height * 0.8));
    circleButton.layer.cornerRadius = circleButton.frame.size.height/2;
    circleButton.layer.borderWidth = circleButton.frame.origin.x/3;
    
    [circleButton addTarget:self action:@selector(takeThePicture:) forControlEvents:UIControlEventTouchUpInside];
    
    circleButton.layer.borderColor = [UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1.0].CGColor;
    
    [circleButton setBackgroundImage:[UIImage imageNamed:@"camera-shutter@2x.png"]  forState:UIControlStateNormal];
    [circleButton setBackgroundImage:[UIImage imageNamed:@"camera-shutter-selected"] forState:UIControlStateHighlighted];
    [circleButton setTintColor:[UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1.0]];
    [circleView addSubview:circleButton];
    [cameraOverlayBottomView addSubview:circleView];
    
    [cameraView addSubview:cameraOverlayBottomView];
    flashButton = [UIButton buttonWithType:UIButtonTypeCustom];
    flashButton.isAccessibilityElement = YES;
    if (IS_IPAD) {
        flashFrame = CGRectMake(0, 0, 100, 50);
    }
    else {
        flashFrame = CGRectMake(-20, 30, 100, 30);
    }
    //NSLog(@"imageP.cameraFlashM:%d", imagePicker.cameraFlashMode);
    
    flashButton.frame = flashFrame;
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"Auto"]) {
        flashButtonTitle = @"Auto";
        [imagePicker setCameraFlashMode: UIImagePickerControllerCameraFlashModeAuto];
        [flashButton setTitleColor:[UIColor colorWithRed:1 green:204.0/255.0f blue:0 alpha:1] forState:UIControlStateNormal];
    }
    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"Off"]) {
        flashButtonTitle = @"Off";
        [imagePicker setCameraFlashMode: UIImagePickerControllerCameraFlashModeOff];
        [flashButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"On"]) {
        flashButtonTitle = @"On";
        [imagePicker setCameraFlashMode:UIImagePickerControllerCameraFlashModeOn];
        [flashButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    } else {
        flashButtonTitle = @"Auto";
        [imagePicker setCameraFlashMode: UIImagePickerControllerCameraFlashModeAuto];
        [flashButton setTitleColor:[UIColor colorWithRed:1 green:204.0/255.0f blue:0 alpha:1] forState:UIControlStateNormal];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"Auto"];
    }
    flashButton.accessibilityLabel = [NSString stringWithFormat:@"Flash Button %@", flashButtonTitle];
    [flashButton setTitle:[NSString stringWithFormat:@"\u26A1%@", flashButtonTitle] forState:UIControlStateNormal];
    [flashButton addTarget:self action:@selector(toggleFlash) forControlEvents:UIControlEventTouchUpInside];
    [cameraView addSubview:flashButton];
    
    if (![UIImagePickerController isFlashAvailableForCameraDevice:imagePicker.cameraDevice]) {
        [flashButton setAlpha:0.0];
    }
    
    UILabel *label = [UILabel new];
    label.text = @"Verification Picture";
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont systemFontOfSize:18.0];
    label.textColor = [UIColor whiteColor];
    label.minimumScaleFactor = 0.5;
    label.frame = CGRectMake(0, 30, self.view.frame.size.width, 30);
    [cameraView addSubview:label];
    UILabel *subLabel = [UILabel new];
    subLabel.text = @"Required: PCA and Recipient";
    subLabel.textAlignment = NSTextAlignmentCenter;
    subLabel.font = label.font;
    subLabel.textColor = label.textColor;
    subLabel.minimumScaleFactor = 0.5;
    subLabel.frame = CGRectMake(0, 60, self.view.frame.size.width, 30);
    [cameraView addSubview:subLabel];
    UILabel *overlayLabel = [self labelOnImage:nil];
    overlayLabel.frame = CGRectMake(0, self.imagePicker.view.frame.size.height - 30, self.imagePicker.view.frame.size.width, 30);
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        overlayLabel.frame = CGRectMake(0, cameraOverlayBottomView.frame.origin.y - 30, self.view.frame.size.width, 30);
        overlayLabel.center = CGPointMake(self.imagePicker.view.center.x, overlayLabel.center.y);
    }
    overlayLabel.text = [self setLabelText];
    [self.imagePicker.view addSubview:overlayLabel];
 
    if (![self.view.subviews containsObject:imagePicker.view]) {
        [self.view addSubview:imagePicker.view];
    }
    //    NSLog(@"self.view.subviews:%@", self.view.subviews);
    if (![self.view.subviews containsObject:cameraView]) {
        [self.view addSubview:cameraView];
    }
    //    NSLog(@"self.view.subviews:%@", self.view.subviews);
}

-(void)takeThePicture:(id)sender {
    [imagePicker takePicture];
    circleView.hidden = YES;
    [UIView animateWithDuration:0.8 animations:^{
        self->whiteView = [[UIView alloc] initWithFrame:self.imagePicker.view.bounds];
        self->whiteView.backgroundColor = [UIColor whiteColor];
        [self.imagePicker.view addSubview:self->whiteView];
        [self->whiteView setAlpha:1.0];
    }completion:NULL];
    
    [UIView animateWithDuration:0.8 animations:^{
        [self->whiteView setAlpha:0.0];
    } completion:NULL];
}

-(void)toggleFlash {
    
    [UIView animateWithDuration:0.3 animations:^{
        self->flashButton.alpha = 0.0;
    }];
    chooseOn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    chooseOn.frame = self->flashFrame;
    [chooseOn setTitle:@"On" forState:UIControlStateNormal];
    [chooseOn setTitleColor:[UIColor colorWithRed:1 green:204.0/255.0f blue:0 alpha:1] forState:UIControlStateNormal];
    [chooseOn addTarget:self action:@selector(chooseFlashOn) forControlEvents:UIControlEventTouchUpInside];
    [cameraView addSubview:chooseOn];
    [chooseOn setAlpha:0.0];
    if (IS_IPAD) {
        chooseOn.frame = CGRectMake(0, 0, 100, 50);
    }
    else {
        chooseOn.frame = CGRectMake(10, 30, 60, 30);
    }
    chooseOff = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [chooseOff setTitle:@"Off" forState:UIControlStateNormal];
    [chooseOff setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [chooseOff addTarget:self action:@selector(chooseFlashOff) forControlEvents:UIControlEventTouchUpInside];
    [cameraView addSubview:chooseOff];
    [chooseOff setAlpha:0.0];
    if (IS_IPAD) {
        chooseOff.frame = CGRectMake(0, 0, 100, 50);
    }
    else {
        chooseOff.frame = CGRectMake(10, 30, 60, 30);
    }
    chooseAuto = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [chooseAuto setTitle:@"\u26A1Auto" forState:UIControlStateNormal];
    [chooseAuto setTitleColor:[UIColor colorWithRed:1 green:204.0/255.0f blue:0 alpha:1] forState:UIControlStateNormal];
    [chooseAuto addTarget:self action:@selector(chooseFlashAuto) forControlEvents:UIControlEventTouchUpInside];
    [cameraView addSubview:chooseAuto];
    [chooseAuto setAlpha:0.0];
    if (IS_IPAD) {
        chooseAuto.frame = CGRectMake(0, 0, 100, 50);
    }
    else {
        chooseAuto.frame = CGRectMake(10, 30, 60, 30);
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        [self->chooseAuto setAlpha:1.0];
        [self->chooseOn setAlpha:1.0];
        [self->chooseOff setAlpha:1.0];
        if (IS_IPAD) {
            self->chooseAuto.frame = CGRectMake(0, 0, 100, 50);
        }
        else {
            self->chooseAuto.frame = CGRectMake(10, 30, 60, 30);
        }
        if (IS_IPAD) {
            self->chooseOff.frame = CGRectMake(200, 0, 100, 50);
        }
        else {
            self->chooseOff.frame = CGRectMake(110, 30, 50, 30);
        }
        if (IS_IPAD) {
            self->chooseOn.frame = CGRectMake(100, 0, 100, 50);
        }
        else {
            self->chooseOn.frame = CGRectMake(70, 30, 50, 30);
        }
    }];
    
    
}

-(void)chooseFlashOn {
    [imagePicker setCameraFlashMode:UIImagePickerControllerCameraFlashModeOn];
    flashButtonTitle = @"On";
    [UIView animateWithDuration:0.3 animations:^{
        self->chooseOn.frame = self->flashFrame;
        self->chooseOff.frame = self->flashFrame;
        self->chooseAuto.frame = self->flashFrame;
        [self->chooseAuto setAlpha:0.0];
        [self->chooseOn setAlpha:0.0];
        [self->chooseOff setAlpha:0.0];
        [self->flashButton setAlpha:1.0];
    }];
    [self->flashButton setTitle:[NSString stringWithFormat:@"\u26A1%@", flashButtonTitle] forState:UIControlStateNormal];
    [self->flashButton setTitleColor:[UIColor colorWithRed:1 green:204.0/255.0f blue:0 alpha:1] forState:UIControlStateNormal];
    self->flashButton.titleLabel.textAlignment = NSTextAlignmentLeft;
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"On"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"Off"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"Auto"];
}

-(void) chooseFlashOff {
    [imagePicker setCameraFlashMode:UIImagePickerControllerCameraFlashModeOff];
    flashButtonTitle = @"Off";
    [UIView animateWithDuration:0.3 animations:^{
        self->chooseOn.frame = self->flashFrame;
        self->chooseOff.frame = self->flashFrame;
        self->chooseAuto.frame = self->flashFrame;
        [self->chooseAuto setAlpha:0.0];
        [self->chooseOn setAlpha:0.0];
        [self->chooseOff setAlpha:0.0];
        [self->flashButton setAlpha:1.0];
    }];
    [self->flashButton setTitle:[NSString stringWithFormat:@"\u26A1%@", flashButtonTitle] forState:UIControlStateNormal];
    [self->flashButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self->flashButton.titleLabel.textAlignment = NSTextAlignmentLeft;
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"On"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"Off"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"Auto"];
}

-(void) chooseFlashAuto {
    [imagePicker setCameraFlashMode:UIImagePickerControllerCameraFlashModeAuto];
    flashButtonTitle = @"Auto";
    [UIView animateWithDuration:0.3 animations:^{
        self->chooseOn.frame = self->flashFrame;
        self->chooseOff.frame = self->flashFrame;
       self->chooseAuto.frame = self->flashFrame;
        [self->chooseAuto setAlpha:0.0];
        [self->chooseOn setAlpha:0.0];
        [self->chooseOff setAlpha:0.0];
        [self->flashButton setAlpha:1.0];
    }];
    [self->flashButton setTitle:[NSString stringWithFormat:@"\u26A1%@", flashButtonTitle] forState:UIControlStateNormal];
    [self->flashButton setTitleColor:[UIColor colorWithRed:1 green:204.0/255.0f blue:0 alpha:1] forState:UIControlStateNormal];
    self->flashButton.titleLabel.textAlignment = NSTextAlignmentLeft;
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"On"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"Off"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"Auto"];
}

#pragma mark - Image Picker Delegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *picture = [info objectForKey:UIImagePickerControllerOriginalImage];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setTimeStyle:NSDateFormatterMediumStyle];
    picture = picture.fixOrientation;
    picture = [picture resizedImageByMagick:@"200x200#"];
    UILabel *overlayLabel = [self labelOnImage:picture];
    overlayLabel.text = [self setLabelText];
    picture = [self addLabel:overlayLabel toImage:picture];
    picData = UIImageJPEGRepresentation(picture, 0.8);
   // [self geocodeCurrentLocation];
}

//-(void)saveVerificationPhoto {
//    LoadingView *lv = [[LoadingView alloc] initWithFrame:CGRectMake(0, 0, 150, 100) andText:@"Saving"];
//    lv.center = self.view.center;
//    [self.view addSubview:lv];
//    [NetworkingHelper saveVerificationPhotoWithVerificationID:_currentSession.verificationID andParams:@{@"file":@"verificationPhoto", @"fileName":@"verificationPhoto.jpg", @"fileData":picData, @"mimeType":@"image/jpeg", @"sessionDataId":_currentSession.idNumber} success:^(id responseObject) {
//        NSLog(@"saved photo, now need to geocode");
//        [lv hideActivityIndicator];
//        self.navigationController.navigationBarHidden = NO;
//        _currentSession.verificationIsRequiredForWorkerAndRecipient = @NO;
//        _currentUser.currentSession = _currentSession;
//        [_currentUser save];
//         if ([_currentSession.currentTimesheetNumber isEqualToNumber:@1]) {
//             [self back];
//         }  else {
//             [self.navigationController setViewControllers:[self viewControllersArray] animated:YES];
//         }
//        [NetworkingHelper addVerificatioinIDToTimesheetWithID:_currentSession.currentTimecard.idNumber andVerificationID:_currentSession.verificationID andSuccess:^(id responseObject) {
//            NSLog(@"success sending verifcation to timesheet!:%@", responseObject);
//        } failure:^(NSURLSessionDataTask *task, NSError *error) {
//            NSLog(@"failure sending verification to timesheet:%@", error);
//        }];
//    } failure:^(NSURLSessionDataTask *task, NSError *error) {
//        [lv hideActivityIndicator];
//        NSLog(@"save photo failed:%@", error);
//    }];
//}

-(void)back {
    
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    _currentUser.currentSession = _currentSession;
    [_currentUser save];
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - Image Processing

-(UILabel *)labelOnImage:(UIImage *)image {
    UILabel *lab = [UILabel new];
    lab.frame = CGRectMake(0, image.size.height - 30, image.size.width, 30);
    lab.backgroundColor = [UIColor lightGrayColor];
    lab.textColor = [UIColor whiteColor];
    lab.alpha = 0.75;
    lab.font = [UIFont systemFontOfSize:18.0];
    lab.textAlignment = NSTextAlignmentCenter;
    return lab;
}

-(NSString *)setLabelText {
    return @"Verification";
}

- (UIImage *)croppIngimageByImageName:(UIImage *)imageToCrop toRect:(CGRect)rect
{
    //CGRect CropRect = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height+15);
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([imageToCrop CGImage], rect);
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return cropped;
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

-(UIImage *)addLabel:(UILabel *)label toImage:(UIImage *)image {
    UIImage *watermarkedImage = nil;
    
    UIGraphicsBeginImageContext(image.size);
    [image drawAtPoint: CGPointZero];
    CGRect rectangle = CGRectMake(0.0, image.size.height - 30.0, image.size.width, 30.0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(context, 0.66, 0.66, 0.66, 0.75);
    CGContextSetRGBStrokeColor(context, 0.66, 0.66, 0.66, 0.75);
    CGContextFillRect(context, rectangle);
    [label drawTextInRect:CGRectMake(0.0, image.size.height - 30.0, image.size.width, 30.0)];
    watermarkedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return watermarkedImage;
}
             
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    [self.locManager stopUpdatingLocation];
}
//             
//-(void)geocodeCurrentLocation {
//    if ([_currentSession.currentTimecard.recipient.shouldRecordLocation isEqualToNumber:@YES]) {
//        CLGeocoder *geoCoder = [CLGeocoder new];
//        LoadingView *lv = [[LoadingView alloc] initWithFrame:CGRectMake(0, 0, 150, 100) andText:@"Saving"];
//        lv.center = self.view.center;
//        [self.view addSubview:lv];
//        if (self.locManager.location) {
//            //        NSLog(@"self.locManager.location:%@", self.locManager.location);
//            [geoCoder reverseGeocodeLocation:self.locManager.location completionHandler:^(NSArray *placemarks, NSError *error) {
//                CLPlacemark *placeMark = placemarks[0];
//                _currentSession.timeIn.address = [NSString stringWithFormat:@"%@ %@ %@ %@ %@", [placeMark subThoroughfare], [placeMark thoroughfare], [placeMark locality], [placeMark administrativeArea], [placeMark postalCode]];
//                _lat = [NSNumber numberWithFloat:self.locManager.location.coordinate.latitude];
//                _lng = [NSNumber numberWithFloat:self.locManager.location.coordinate.longitude];
//                [lv removeFromSuperview];
//                NSLog(@"geoCoding done, now should set verification");
//                [self setVerification];
//            }];
//        } else {
//            [self setNoAddressForTimecard];
//        }
//    } else {
//        [self setNoAddressForTimecard];
//    }
//}



-(NSDateFormatter *)dateFormat {
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    return formatter;
}

//-(void)setVerification {
//    LoadingView *lv = [[LoadingView alloc] initWithFrame:CGRectMake(0, 0, 150, 100) andText:@"Saving"];
//    lv.center = self.view.center;
//    [self.view addSubview:lv];
//    [NetworkingHelper setVerificationForCameraWithID:_currentSession.verificationID andParams:@{@"time":[[self dateFormat] stringFromDate:[NSDate date]], @"address":_currentSession.timeIn.address, @"latitude":_lat, @"longitude":_lng} success:^(id responseObject) {
//        [lv hideActivityIndicator];
//        NSLog(@"should be setting the new viewcontrollers array");
//        [self saveVerificationPhoto];
//    } failure:^(NSURLSessionDataTask *task, NSError *error) {
//        [lv hideActivityIndicator];
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[NSString stringWithFormat:@"We were unable to save the photo to the server. Please try again Error Message:%@.", error.localizedDescription] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry", nil];
//        alert.tag = 44;
//        [alert show];
//        NSLog(@"settingverification:%@", error.localizedDescription);
//    }];
//}
//
//-(NSArray *)viewControllersArray {
//    ActivityInfoViewController *vc = [ActivityInfoViewController new];
//    _currentSession.verificationIsRequiredForWorkerAndRecipient = @NO;
//    self.navigationController.navigationBarHidden = NO;
//    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
//    _currentUser.currentSession = _currentSession;
//    [_currentUser save];
//    vc.currentSession = _currentSession;
//    NSArray *array = [NSArray arrayWithObjects:@"Personal Care Service", @"Respite", @"Consumer Support", @"Personal Assistance", @"Treatment and Training", nil];
//    for (NSString *string in array) {
//        if ([self string:_currentSession.service.serviceName containsCaseInsensitiveString:string]) {
//            return [NSArray arrayWithObjects:[LoginViewController new], [HomeViewController new], [SelectServiceViewController new], [RatioSharedCareViewController new], [ChooseUserViewController new], vc, nil];
//        } else {
//            return [NSArray arrayWithObjects:[LoginViewController new], [HomeViewController new], [SelectServiceViewController new], [ChooseUserViewController new], vc, nil];
//        }
//    }
//    return self.navigationController.viewControllers;
//}

-(BOOL)string:(NSString *)urlSchemeString containsCaseInsensitiveString:(NSString *)string {
    if ([urlSchemeString rangeOfString:string options:NSCaseInsensitiveSearch].location == NSNotFound)
    {
        return NO;
    }
    return YES;
}

@end
