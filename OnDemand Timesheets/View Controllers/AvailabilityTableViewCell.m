//
//  AvailabilityTableViewCell.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 10/24/16.
//  Copyright © 2016 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "AvailabilityTableViewCell.h"

@implementation AvailabilityTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(IBAction)switchChanged:(id)sender {
    [self.delegate switchChanged:sender];
}

@end
