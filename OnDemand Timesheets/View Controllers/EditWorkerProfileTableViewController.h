//
//  EditWorkerProfileTableViewController.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 11/12/16.
//  Copyright © 2016 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "JMGCameraViewController.h"
#import "User.h"
#import "EditGenderTableViewCell.h"
#import "CountiesTableViewController.h"



@interface EditWorkerProfileTableViewController : UITableViewController <UITextFieldDelegate, JMGCameraViewControllerDelegate, UITableViewDelegate, UITableViewDataSource, EditGenderTableViewCellDelegate, UITextViewDelegate, CountiesTableViewControllerDelegate>

@property (nonatomic, weak) UITextField *firstNameTextField;
@property (nonatomic, weak) UITextField *lastNameTextField;
@property (nonatomic, weak) UITextField *genderTextField;
@property (nonatomic, weak) UIButton *profileImageButton;
@property (nonatomic, weak) UISwitch *cprCertifiedSwitch;
@property (nonatomic, weak) UISwitch *behaviorSwitch;
@property (nonatomic, weak) UISwitch *childrenSwitch;
@property (nonatomic, weak) UISwitch *transfersSwitch;
@property (nonatomic, weak) UISwitch *driversLicenseSwitch;
@property (nonatomic, weak) IBOutlet UITextField *yearsOfExperienceTextField;
//@property (nonatomic, strong) NSArray *qualificationsArray;
@property (nonatomic, strong) User *currentUser;
@property (nonatomic, strong) UIImage *tempImageToShow;
@property (nonatomic, strong) UIImageView *tempImageToSend;
@property (nonatomic) BOOL isFromLoginScreen;
@property (nonatomic , weak) IBOutlet UIToolbar *keyboardToolbar;
@property(nonatomic, strong) NSMutableArray *countiesArrayPassedToEditWorker;





@property (nonatomic, strong) NSString *firstNameString;
@property (nonatomic, strong) NSString *lastNameString;
@property (nonatomic, strong) NSString *addressString;
@property (nonatomic, strong) NSString *profilePicString;
@property (nonatomic, strong) NSString *qualificationsarray;
//@property (nonatomic, strong) NSArray *countiesArray;





@end
