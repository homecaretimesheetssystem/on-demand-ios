//
//  EditWorkerProfileTableViewController.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 11/12/16.
//  Copyright © 2016 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "EditWorkerProfileTableViewController.h"
#import "ProfilePictureTableViewCell.h"
#import <AFNetworking/AFNetworking.h>
#import <AFOAuth2Manager/AFOAuth2Manager.h>
#import "NetworkingHelper.h"
#import "QualificationsTableViewCell.h"
#import "YearsOfExperienceTableViewCell.h"
#import "EditNameTableViewCell.h"
#import "EditAddressTableViewCell.h"
#import "GenderPreferenceTableViewCell.h"
#import "Counties.h"
#import "CountiesTableViewCell.h"
#import "Qualifications.h"
#import "User.h"
#import "LoadingView.h"
#import "HomeTableViewController.h"
#import <AFNetworking/UIImageView+AFNetworking.h>


@interface EditWorkerProfileTableViewController ()

@end

@implementation EditWorkerProfileTableViewController {
    
    NSMutableArray *selectedQualificationsArray;
    BOOL hideGenderPickerEdit;
    BOOL loadingCounties;
    BOOL loadingQuals;
    NSMutableArray *countiesArray;
    NSMutableArray *selectedCountiesArray;
    NSMutableArray *qualificationsArray;
    BOOL profilePictureChanged;
    BOOL changesMadeToProfile;
    BOOL genderChanged;
    BOOL yearsOfExperienceChanged;
    NSMutableArray *localCountiesArray;
    NSMutableArray *localQualificationsArray;
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Edit Profile";
    
    
  //  _qualificationsArray = @[@"CPR Certified", @"Experience with Behaviors", @"Experience working with Children", @"Experience making Transfers", @"Has a driver's license"];
    selectedQualificationsArray = [NSMutableArray new];
    hideGenderPickerEdit = YES;
    yearsOfExperienceChanged = NO;
    genderChanged = NO;
    countiesArray = [NSMutableArray new];
    selectedCountiesArray = [NSMutableArray new];
    qualificationsArray = [NSMutableArray new];
    localQualificationsArray = [NSMutableArray new];
    localCountiesArray = [NSMutableArray new];
    loadingQuals = YES;
    _currentUser = [User currentUser];
    NSLog(@"_current user is %@", _currentUser);
    loadingCounties = YES;
    [self loadPCAProfile];
  //  [self loadQualifications];
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"stillFromLogin"] isEqual:@YES]) {
        _isFromLoginScreen = YES;
        self.navigationItem.hidesBackButton = YES;
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"stillFromLogin"];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Complete Your Profile" message:@"In order to use OnDemand services, you need to complete your profile." preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

        }]];
        alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
        [self presentViewController:alert animated:YES completion:NULL];
        
    }
    if (_isFromLoginScreen == YES) {
        self.navigationItem.hidesBackButton = YES;
    }else{
        self.navigationItem.hidesBackButton = NO;
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
        UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self action:@selector(home:)];
        self.navigationItem.leftBarButtonItem=newBackButton;
    }
    
    
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(saveTapped)];
    
    self.navigationItem.rightBarButtonItem = saveButton;
    
//    [self.tableView registerClass:[ProfilePictureTableViewCell class] forCellReuseIdentifier:@"profilePictureCell"];
//    [self.tableView registerClass:[EditNameTableViewCell class] forCellReuseIdentifier:@"nameCell"];
//    [self.tableView registerClass:[GenderPreferenceTableViewCell class] forCellReuseIdentifier:@"genderCell"];
//    [self.tableView registerClass:[EditAddressTableViewCell class] forCellReuseIdentifier:@"addressCell"];
//    [self.tableView registerClass:[YearsOfExperienceTableViewCell class] forCellReuseIdentifier:@"yearsOfExperienceTableViewCell"];
//    [self.tableView registerClass:[QualificationsTableViewCell class] forCellReuseIdentifier:@"PCAQualificationsTableViewCell"];
//


}



-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];

    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
    
}


-(void)home:(UIBarButtonItem *)sender
{
    
    
    if (changesMadeToProfile == YES) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Save Changes" message:@"Do you want to save changes to your profile?" preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            self->changesMadeToProfile = NO;
            [self saveTapped];
            
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            self->changesMadeToProfile = NO;
            [self.navigationController popViewControllerAnimated:YES];
          
        }]];
        [self presentViewController:alert animated:YES completion:NULL];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    
}


-(void)loadPCAProfile {
 //   LoadingView *loadingView = [LoadingView addLoadingViewWithText:@"Loading"];
  //  [NetworkingHelper getPCAWithID:User.currentUser.pca.idNumber andSuccess:^(id responseObject) {
        
   //     NSLog(@"current pca %@", User.currentUser.pca);
       [localCountiesArray addObjectsFromArray:_currentUser.pca.countyArray];
        [localQualificationsArray addObjectsFromArray:_currentUser.pca.qualificationsArray];
        _countiesArrayPassedToEditWorker = localCountiesArray;
        if (localCountiesArray.count > 0) {
        [selectedCountiesArray addObjectsFromArray:_currentUser.pca.countyArray];
        //[_countiesArrayPassedToEditWorker addObjectsFromArray:User.currentUser.pca.countyArray];
        }
        if (localQualificationsArray.count > 0) {
        [selectedQualificationsArray addObjectsFromArray:_currentUser.pca.qualificationsArray];
        }
        [self loadQualifications];

//    } failure:^(NSURLSessionDataTask *task, NSError *error) {
 //       NSLog(@"get pca error %@", error);
 //   }];
    
}


-(void)loadQualifications {
//    NSLog(@"in qualificaion");
    [NetworkingHelper getQualificationsWithSuccess:^(id responseObject) {
        NSMutableArray *mutArr = [NSMutableArray new];
//        NSLog(@"response object %@", responseObject);
        for (NSDictionary *dict in responseObject) {
//            NSLog(@"dictionary is %@", dict);
            Qualifications *qualifications = [[Qualifications alloc] initWithDictionary:dict];
//        NSLog(@"qualifications %@", qualifications);
            [mutArr addObject:qualifications];
        }
        [self->qualificationsArray addObjectsFromArray:mutArr];
        self->loadingQuals = NO;
        [self.tableView reloadData];
    //    [self loadCounties];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"failure in load quals %@", error);
        id responseObject = [NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:0 error:nil];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:[NSString stringWithFormat:@"%@", responseObject[@"error"]] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.tableView reloadData];
        }]];
        
        alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
        [self presentViewController:alert animated:YES completion:NULL];

    }];
    
}



-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  //  NSLog(@"3 sections");
    return 4;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }
   else if (section == 1) {
        return 3;
    }else if (section == 2 && loadingQuals == YES){
        return 1;
    }else if (section == 2 && loadingQuals == NO){
        return qualificationsArray.count+1;
    }else if (section == 3){
        return 1;
    }
    return 1;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return @"Profile Picture";
    }else if (section == 1){
                return @"Personal Information";
    }else if (section == 2){
        return @"Qualifications";
    }else if (section == 3){
        return @"Counties";
    }
    return nil;
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
        ProfilePictureTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"profilePictureCell"];
            cell.profilePicture.layer.cornerRadius = cell.profilePicture.frame.size.height /2;
            cell.profilePicture.clipsToBounds = YES;

            if (_tempImageToShow) {
                [cell.profilePicture setImage:_tempImageToShow];
                self.profilePicString = @"YES";
            }else if ([[NSUserDefaults standardUserDefaults] objectForKey:@"profilePicture"]){
                NSString *logoURL = [NSString stringWithFormat:@"https://s3.amazonaws.com/hc-timesheets-demo/%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"profilePicture"]];
                [cell.profilePicture setImageWithURL:[NSURL URLWithString:logoURL] placeholderImage:[UIImage imageNamed:@"Icon"]];
            }
            else{
            NSString *logoURLString = [NSString stringWithFormat:@"https://s3.amazonaws.com/hc-timesheets-demo/%@", _currentUser.pca.profileImageURL];
            self.profilePicString = logoURLString;
            [cell.profilePicture setImageWithURL:[NSURL URLWithString:logoURLString] placeholderImage:[UIImage imageNamed:@"Icon"]];

            }
        return cell;
        }
    } else if (indexPath.section == 1) {
         if (indexPath.row == 0){
             EditNameTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"nameCell"];
             cell.firstNameTextfield.text = _currentUser.pca.firstName;
             cell.lastNameTextField.text = _currentUser.pca.lastName;
             self.firstNameString = cell.firstNameTextfield.text;
             self.lastNameString = cell.lastNameTextField.text;
             return cell;
            
         } else if (indexPath.row == 1) {
             EditGenderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"genderCell"];
             cell.delegate = self;
             if (genderChanged == NO) {
                 cell.genderPreferenceLabel.text = [_currentUser.pca stringForGender];
                 [cell.genderPicker selectRow:_currentUser.pca.gender - 1 inComponent:0 animated:NO];
             }
             return cell;
             
         } else if (indexPath.row == 2) {
             EditNameTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"aboutMeCell"];
             if (User.currentUser.pca.aboutMe) {
                 cell.aboutMeTextView.text = _currentUser.pca.aboutMe;
                 cell.placeholderTextView.alpha = _currentUser.pca.aboutMe.length > 0 ? 0.0 : 1.0;
                 //   self.addressString = User.currentUser.pca.homeAddress;
             }
             //    self.addressString = cell.addressTextfield.text;
             return cell;

        } else if (indexPath.row == 3) {
            EditAddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"addressCell"];
            if (User.currentUser.pca.homeAddress) {
                cell.addressTextfield.text = _currentUser.pca.homeAddress;
             //   self.addressString = User.currentUser.pca.homeAddress;
            }else{
                cell.addressTextfield.placeholder = @"Full address";
                
            }
        //    self.addressString = cell.addressTextfield.text;
            return cell;
        }
    }else if (indexPath.section == 2){
        if (indexPath.row == 0) {
         //   NSLog(@"check 5");
            YearsOfExperienceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"yearsOfExperienceTableViewCell"];
            if (yearsOfExperienceChanged == NO) {
                    cell.yearsOfExperiecneTextField.text = _currentUser.pca.yearsOfExperience;
            }
            return cell;
            
        }else if (indexPath.row >=1){
            QualificationsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PCAQualificationsTableViewCell"];
            Qualifications *qual = qualificationsArray[indexPath.row-1];

            
            cell.accessoryType = [self qualificationIsSelected:qual] ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
            cell.tintColor = [UIColor colorWithRed:55.0/255.0 green:99.0/255.0 blue:135.0/255.0 alpha:1.0];
            
            
            cell.titleLabel.text = qual.qualificationName;
            return cell;
        }
    }else if (indexPath.section == 3){
      //  if (indexPath.row ==0) {
        CountiesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CountiesTableViewCell"];
         //   cell.accessoryType = [self countyFromProfile:counties] ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
          //  cell.accessoryType = [self countyIsSelected:counties] ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
            if (_countiesArrayPassedToEditWorker.count > 1) {
                cell.countyLabel.text = [NSString stringWithFormat:@"%lu Counties Selected", (unsigned long)_countiesArrayPassedToEditWorker.count];
            }else if (_countiesArrayPassedToEditWorker.count == 1){
                Counties *counties = _countiesArrayPassedToEditWorker[indexPath.row];
            cell.countyLabel.text = counties.countyName;
        }else if (_countiesArrayPassedToEditWorker.count == 0){
            cell.countyLabel.text = @"No Counties Selected";
        }
            return cell;
    //    }
    }
    
    return nil;
}

-(BOOL)countyFromProfile:(Counties *)county {
    BOOL found = NO;
    for (int i = 0; i < localCountiesArray.count; i++) {
        Counties *counties = localCountiesArray[i];
        if ([counties.countyID isEqualToNumber:county.countyID]) {
            found = YES;
            break;
        }
    }
    return found;
}

-(BOOL)qualificationIsSelected:(Qualifications *)qualification {
    BOOL found = NO;
    for (int i = 0; i < selectedQualificationsArray.count; i++) {
        Qualifications *qualifications = selectedQualificationsArray[i];
        if ([qualifications.qualificationID isEqualToNumber:qualification.qualificationID]) {
            found = YES;
            break;
        }
    }
    return found;
}


-(BOOL)countyIsSelected:(Counties *)county {
    BOOL found = NO;
    for (int i = 0; i < selectedCountiesArray.count; i++) {
        Counties *counties = selectedCountiesArray[i];
        if ([counties.countyID isEqualToNumber:county.countyID]) {
            found = YES;
            break;
        }
    }
    return found;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
            return 80;
        
    }else if (indexPath.section == 1){
        if (indexPath.row == 1 && hideGenderPickerEdit == YES) {
            return 50;
        }else if (indexPath.row == 1 && hideGenderPickerEdit == NO){
            return 220;
        }else if (indexPath.row == 2){
            return 120;
        }else{
            return 50;
        }
    } else if (indexPath.section == 2){
        return 50;
    }
    return 50;
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    // Text Color
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    if (@available(iOS 11.0, *)) {
        [header.textLabel setTextColor:[UIColor colorNamed:@"ButtonColor"]];
    } else {
        // Fallback on earlier versions
    }
}

-(void)updateCurrentUserFromPicker:(User *)user {
    _currentUser = user;
}

- (void)saveTapped {
    [self.view endEditing:YES];
    NSLog(@"temp image to send %@", _tempImageToSend.image);
    NSLog(@"self.profile pic string %@", _currentUser.pca.profileImageURL);
    NSLog(@"nsuser defaults for profile pic %@", [[NSUserDefaults standardUserDefaults] objectForKey:@"profilePicture"]);
    NSString *yoeString = ((YearsOfExperienceTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]]).yearsOfExperiecneTextField.text;
    if (!yoeString) {
        yoeString = @"1";
    }
    if ([self.firstNameString isEqualToString:@""]) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Required" message:@"You must enter your first name." preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }]];
        [self presentViewController:alert animated:YES completion:NULL];
    }
   else if ([self.lastNameString isEqualToString:@""]) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Required" message:@"You must enter your last name." preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }]];
        [self presentViewController:alert animated:YES completion:NULL];
        
    }
   else if ((!_currentUser.pca.profileImageURL && _isFromLoginScreen == NO) || (!_tempImageToShow && _isFromLoginScreen == YES)) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Required" message:@"You must add a profile picture." preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }]];
        [self presentViewController:alert animated:YES completion:NULL];
    }
   else if (selectedCountiesArray.count == 0 && User.currentUser.pca.countyArray.count == 0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Required" message:@"Choose which counties you're licensed in." preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }]];
        [self presentViewController:alert animated:YES completion:NULL];
    
   } else {
    
    LoadingView *loadingView = [LoadingView addLoadingViewWithText:@"Saving"];
    
    NSMutableArray *qualificationsArrayToPass = [NSMutableArray new];
    for (Qualifications *q in selectedQualificationsArray) {
        [qualificationsArrayToPass addObject:q.qualificationID];
    }
    NSMutableArray *countiesArrayToPass = [NSMutableArray new];
    for (Counties *c in selectedCountiesArray) {
        [countiesArrayToPass addObject:c.countyID];
    }
       if (_isFromLoginScreen == YES) {
           _currentUser.pca.isAvailable = @0;
       }
       [NetworkingHelper updatePCAProfileWithPCAID:User.currentUser.pca.idNumber andFirstName:self.firstNameString andLastName:self.lastNameString andAddressString:@"HI" andYearsOfExperience:yoeString andGenderInt:_currentUser.pca.gender andQualificationsArray:qualificationsArrayToPass andCountiesArray:countiesArrayToPass andIsAvailable:_currentUser.pca.isAvailable andAboutMe:_currentUser.pca.aboutMe andSuccess:^(id responseObject) {
        [loadingView removeFromSuperview];
           [self checkForImageUploadWithLoadingView:loadingView withYearsString:yoeString];
      //  HomeTableViewController *vc = [[UIStoryboard storyboardWithName:@"OnDemandWorker" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeTableViewController"];
       // [self.navigationController showViewController:vc sender:self];
      //  [self presentViewController:vc animated:YES completion:nil];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [loadingView removeFromSuperview];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"There was an error trying to save your profile. Press Retry to try again."  preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self saveTapped];
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:NULL]];
        [self presentViewController:alert animated:YES completion:NULL];
        
    }];
    
        
    
       
    }
}

-(void)checkForImageUploadWithLoadingView:(LoadingView*)loadingView withYearsString:(NSString *)yoeString {
    NSData *imageData = UIImageJPEGRepresentation(self.tempImageToShow, 0.2);
    if (imageData) {
        [NetworkingHelper postPCAProfilePictureWithImageData:imageData andPCA:User.currentUser.pca andSuccess:^(id responseObject) {
            User.currentUser.pca.profileImageURL = responseObject[@"uploadedFileLocation"];
            self.currentUser.pca.profileImageURL = responseObject[@"uploadedFileLocation"];
            [[NSUserDefaults standardUserDefaults] setObject:responseObject[@"uploadedFileLocation"] forKey:@"profilePicture"];
            [self saveCurrentUserAndDismissWithYearsString:yoeString];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"There was an error trying to save your profile. Press Ok to try again."  preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self saveTapped];
            }]];
            [self presentViewController:alert animated:YES completion:NULL];
            
            
            NSLog(@"error in save profile picture %@", error);
            
        }];
    } else {
        [loadingView removeFromSuperview];
        [self saveCurrentUserAndDismissWithYearsString:yoeString];
    }
}

-(void)saveCurrentUserAndDismissWithYearsString:(NSString *)yeoString {
    self.currentUser.pca.firstName = self.firstNameString;
    self.currentUser.pca.lastName = self.lastNameString;
    self.currentUser.pca.yearsOfExperience = yeoString;
    self.currentUser.pca.qualificationsArray = selectedQualificationsArray;
    self.currentUser.pca.countyArray = selectedCountiesArray;
    [self.currentUser save];
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)dismissProfile:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        changesMadeToProfile = YES;
        [self getCameraPermissions:self];

    }else if (indexPath.section == 1){
        if (indexPath.row == 1) {
            genderChanged = YES;
            changesMadeToProfile = YES;
            if (hideGenderPickerEdit == YES) {
                hideGenderPickerEdit = NO;
                [self.tableView beginUpdates];
                [self.tableView endUpdates];
//            [self.tableView reloadData];

            }else{
            hideGenderPickerEdit = YES;
                [self.tableView beginUpdates];
                [self.tableView endUpdates];
//            [self.tableView reloadData];
            }
        }
    }else if (indexPath.section == 2){

         if (indexPath.row >= 1){
            changesMadeToProfile = YES;
            [self selectOrDeselectQualificationsWithIndexPath:indexPath andTableView:tableView];
            
        }
    }else if (indexPath.section == 3){
        if (indexPath.row >= 0) {
        changesMadeToProfile = YES;
        }
    }
}


-(void)selectOrDeselectQualificationsWithIndexPath:(NSIndexPath *)indexPath andTableView:(UITableView *)tableView {
    Qualifications *q = qualificationsArray[indexPath.row-1];
    Qualifications *qualToRemove = nil;
    BOOL shouldRemoveFromArray = NO;
    for (Qualifications *qual in selectedQualificationsArray.copy) {
        if ([qual.qualificationID isEqualToNumber:q.qualificationID]) {
            qualToRemove = qual;
            shouldRemoveFromArray = YES;
            break;
        }
    }
    
    if (qualToRemove) {
        [selectedQualificationsArray removeObject:qualToRemove];
    }else{
        [selectedQualificationsArray addObject:q];
    }
    
    [self.tableView reloadData];
}

//-(void)selectOrDeselectQualificationsWithIndexPath:(NSIndexPath *)indexPath andTableView:(UITableView *)tableView {
//    Qualifications *q = qualificationsArray[indexPath.row-1];
//    BOOL shouldRemoveFromArray = NO;
//    for (Qualifications *qual in selectedQualificationsArray) {
//        if (qual.qualificationID == q.qualificationID) {
//            shouldRemoveFromArray = YES;
//            break;
//        }
//    }
//    Qualifications *qualToRemove = nil;
//    if (shouldRemoveFromArray == YES) {
//        for (Qualifications *qual in selectedQualificationsArray) {
//            if (q.qualificationID == qual.qualificationID) {
//                qualToRemove = qual;
//            }
//        }
//    } else {
//        [selectedQualificationsArray addObject:q];
//    }
//    if (qualToRemove) {
//        [selectedQualificationsArray removeObject:qualToRemove];
//    }
//    [self.tableView reloadData];
//}

-(void)selectOrDeselectCountiesWithIndexPath:(NSIndexPath *)indexPath andTableView:(UITableView *)tableView {
    Counties *c = countiesArray[indexPath.row];
    Counties *countyToRemove = nil;
    BOOL shouldRemoveFromArray = NO;
    for (Counties *counties in selectedCountiesArray) {
        if ([counties.countyID isEqualToNumber:c.countyID]) {
            countyToRemove = counties;
            shouldRemoveFromArray = YES;
            break;
        }
    }

    if (countyToRemove) {
        [selectedCountiesArray removeObject:countyToRemove];
    } else {
        [selectedCountiesArray addObject:c];
    }

    [self.tableView reloadData];
}


#pragma mark - Camera Methods

-(void)openProfilePictureCamera {
    
    JMGCameraViewController *camera = [JMGCameraViewController new];
    camera.delegate = self;
    camera.parentStatusBarStyle = [UIApplication sharedApplication].statusBarStyle;
    camera.parentNavBarStyle = self.navigationController.navigationBar.barStyle;
    camera.cameraMode = CameraMode_Custom;
    camera.fileType = FileType_jpg;
    camera.showWhiteCircleInSquareMode = YES;
    camera.showPictureFromCameraRollForChooseFromPhotoLibraryButton = YES;
    // Add some text if you want to add a title above the square crop camera. If nil, no label will be added.
    camera.titleAboveCamera = @"Add Profile Pic";
    camera.faceFrontOnLaunch = YES;
    [self.navigationController showViewController:camera sender:self];
}

-(void)getCameraPermissions:(id)sender {
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] && [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusAuthorized) {
        //        [self openProfilePictureCamera];
        dispatch_async(dispatch_get_main_queue(), ^{
            //                    [self openProfilePictureCamera];
            [self getPhotoLibraryPermissions];
        });
    } else if ([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusNotDetermined && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if (granted) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    //                    [self openProfilePictureCamera];
                    [self getPhotoLibraryPermissions];
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self getPhotoLibraryPermissions];
                });
                
            }
            
        }];
    } else if ([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusNotDetermined) {
        if  (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            [self getPhotoLibraryPermissions];
        }
    } else if ([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusDenied || [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusRestricted) {
        [self getPhotoLibraryPermissions];
    } else {
        UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Camera Access Denied" message:[NSString stringWithFormat:@"We can't access your camera. To give us access, go to Settings -> %@ and allow camera permission to use this feature.", [[NSBundle mainBundle] infoDictionary][@"CFBundleDisplayName"]] preferredStyle:UIAlertControllerStyleAlert];
        [controller addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}]];
        [controller addAction:[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self goToPhoneSettings];
        }]];
        [self presentViewController:controller animated:YES completion:NULL];
    }
    
}

-(void)getPhotoLibraryPermissions {
    
    NSUInteger status = [PHPhotoLibrary authorizationStatus];
    if (status == PHAuthorizationStatusAuthorized) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self openProfilePictureCamera];
        });
    } else if (status == PHAuthorizationStatusNotDetermined) {
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            if (status == PHAuthorizationStatusAuthorized) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self openProfilePictureCamera];
                });
            } else if ([self cantAccessCameraOrPhotoLibrary]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self showAccessAlert];
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self openProfilePictureCamera];
                });
            }
        }];
    } else if ([self cantAccessCameraOrPhotoLibrary]) {
        [self showAccessAlert];
    }
}

-(BOOL)cantAccessCameraOrPhotoLibrary {
    NSUInteger status = [PHPhotoLibrary authorizationStatus];
    if (([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusDenied || [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusRestricted) && (status == PHAuthorizationStatusDenied || status == PHAuthorizationStatusRestricted)) {
        return YES;
    }
    return false;
}

-(void)showAccessAlert {
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Photo Library Access Denied" message:[NSString stringWithFormat:@"We can't access your camera or photo library. To give us access, go to Settings -> %@ and allow access to camera or photos to use this feature.", [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleNameKey]] preferredStyle:UIAlertControllerStyleAlert];
    [controller addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}]];
    [controller addAction:[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self goToPhoneSettings];
    }]];
    [self presentViewController:controller animated:YES completion:NULL];
}

-(void)goToPhoneSettings {
    NSURL *appSettings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    [[UIApplication sharedApplication] openURL:appSettings];
}

-(void)dismissCameraWithImage:(UIImage *)image {
    profilePictureChanged = YES;
    _tempImageToSend.image = image;
    _tempImageToShow = image;
    NSLog(@"temp image to send %@", _tempImageToSend.image);
    NSLog(@"temp image to show %@", _tempImageToShow);
    [self.tableView reloadData];

}

#pragma mark UITextview

-(void)textViewDidChange:(UITextView *)textView {
    if (textView.tag == 13) {
        EditNameTableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:1]];
        cell.placeholderTextView.alpha = textView.text.length > 0 ? 0.0 : 1.0;
        _currentUser.pca.aboutMe = textView.text;
    }
}

#pragma mark UITextfield


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {

    textField.inputAccessoryView = _keyboardToolbar;
    for (UIBarButtonItem *item in _keyboardToolbar.items) {
        item.tag = textField.tag;
    }
    return YES;
}

-(IBAction)nextTextField:(UIBarButtonItem *)sender {
    long tag = sender.tag;
    id nextView = [self.view viewWithTag:tag + 1];
    if (nextView && [nextView respondsToSelector:@selector(becomeFirstResponder)]) {
        [nextView becomeFirstResponder];
    } else {
        [self dismissKeyboard:sender];
    }
}

-(IBAction)prevTextField:(UIBarButtonItem *)sender {
    long tag = sender.tag;
    id previousView = [self.view viewWithTag:tag - 1];
    if (previousView && [previousView respondsToSelector:@selector(becomeFirstResponder)]) {
        [previousView becomeFirstResponder];
    } else {
        [self dismissKeyboard:sender];
    }
}

-(IBAction)dismissKeyboard:(UIBarButtonItem *)sender {
    
    [self.view endEditing:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    UIBarButtonItem *item = [UIBarButtonItem new];
    item.tag = textField.tag;
    [self nextTextField:item];
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    changesMadeToProfile = YES;
    NSString * searchStr = [textField.text stringByReplacingCharactersInRange:range withString:string];

    if (textField.tag == 10) {
        self.firstNameString = searchStr;
    }else if (textField.tag == 11){
        self.lastNameString = searchStr;
    }else if (textField.tag == 12){
        self.addressString = searchStr;
    } else if (textField.tag == 14) {
        yearsOfExperienceChanged = YES;
    }
    return YES;
    
}




-(void)endEditing {
    [self.tableView setContentOffset:CGPointMake(0, 20.)];
    [self.view endEditing:YES];
    [self.tableView reloadData];
}

-(UIToolbar *)textFieldAccessoryToolbar:(UITextField *)textField {
    
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 44)];
    toolbar.tintColor = [UIColor darkGrayColor];
    UIButton *previousButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [previousButton addTarget:self action:@selector(prevTextField:) forControlEvents:UIControlEventTouchUpInside];
    previousButton.tag = textField.tag;
    [previousButton setBackgroundImage:[UIImage imageNamed:@"back_chevron"] forState:UIControlStateNormal];
    previousButton.frame = CGRectMake(0, 0, 32, 32);
    UIBarButtonItem *prevBBItem = [[UIBarButtonItem alloc] initWithCustomView:previousButton];
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [nextButton addTarget:self action:@selector(nextTextField:) forControlEvents:UIControlEventTouchUpInside];
    [nextButton setBackgroundImage:[UIImage imageNamed:@"chevron"] forState:UIControlStateNormal];
    nextButton.tag = textField.tag;
    nextButton.frame = CGRectMake(0, 0, 32, 32);
    UIBarButtonItem *nextBBItem = [[UIBarButtonItem alloc] initWithCustomView:nextButton];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *fixedSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedSpace.width = -6;
    toolbar.items = @[prevBBItem, nextBBItem, flexibleSpace];
    return toolbar;
}



- (BOOL) containsSymbols: (NSString *) candidate {
    NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"_@.+abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789"] invertedSet];
    
    if ([candidate rangeOfCharacterFromSet:set].location != NSNotFound) {
        return YES;
    }
    return NO;
}

-(BOOL) containsWhitespace: (NSString *) candidate {
    NSRange whiteSpaceRange = [candidate rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
    if (whiteSpaceRange.location != NSNotFound) {
        return YES;
    }
    return NO;
}

-(void)finishedSelectingCountiesWithArray:(NSMutableArray *)countiesArrayy{
    selectedCountiesArray = countiesArrayy;
    [self.tableView reloadData];
    
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"userWantsToChooseCountiesSegue"]) {
        CountiesTableViewController *vc = [segue destinationViewController];
        vc.workersActualCountiesArray = _countiesArrayPassedToEditWorker;
        vc.delegate = self;
    }
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
