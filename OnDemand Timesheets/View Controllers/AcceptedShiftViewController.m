//
//  AcceptedShiftViewController.m
//  Homecare Timesheet Demo
//
//  Created by Joey Kjornes on 2/8/17.
//  Copyright © 2017 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "AcceptedShiftViewController.h"
#import "ShiftConfirmationTableViewCell.h"
#import "ShiftAddressTableViewCell.h"
#import "SubmitETABeginShiftTableViewCell.h"
#import "Request.h"
#import "Shift.h"
#import "NetworkingHelper.h"
#import "BeginShiftTableViewCell.h"
#import "CancelTableViewCell.h"
#import "AddressDetailViewController.h"
#import "FindShiftsViewController.h"
#import "HomeTableViewController.h"

@interface AcceptedShiftViewController ()


@end

@implementation AcceptedShiftViewController {

    BOOL etaSuccessfullySubmitted;
    BOOL hideETAPicker;
    BOOL hideCancelReasonTextfield;
    BOOL shouldShowConfirmCancelButton;
    NSString *sessionDataIDString;
    
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Shift";
    hideETAPicker = YES;
    hideCancelReasonTextfield = YES;
    etaSuccessfullySubmitted = NO;
    shouldShowConfirmCancelButton = NO;
    [self getRSVPId];
    _rsvpArray = [NSMutableArray new];
    _locManager = [[CLLocationManager alloc] init];
    _locManager.delegate = self;
    _locManager.distanceFilter = kCLDistanceFilterNone;
    _locManager.desiredAccuracy = kCLLocationAccuracyBest;
        [self.locManager requestWhenInUseAuthorization];
    [_locManager startUpdatingLocation];
    if (_isFromShiftDetailScreen == YES) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self action:@selector(backButtonPressed)];
    }
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(returnToHomeScreen) name:@"push_type=3" object:nil];
}

-(void)getRSVPId{
    [NetworkingHelper seeIndividualRequestWithRequestID:self.requestIDNumber andSuccess:^(id responseObject) {
        NSMutableArray *mutArr = [NSMutableArray new];
        for (NSDictionary *dict in responseObject[@"rsvps"]) {
            RSVP *rsvp = [[RSVP alloc] initWithDictionary:dict];
            [mutArr addObject:rsvp];
        }
        self.rsvpArray = [mutArr copy];
        for (RSVP *rsvp in self->_rsvpArray) {
            self.RSVPIDNumber = rsvp.RSVPID;
            self->sessionDataIDString = rsvp.sessionDataID;
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        id responseObject = [NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:0 error:nil];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:[NSString stringWithFormat:@"%@", responseObject[@"error"]] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.tableView reloadData];
        }]];
        
        alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
        [self presentViewController:alert animated:YES completion:NULL];

    }];

}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



//ShiftDetailViewController

#pragma mark - Data


#pragma mark - UITableView Data Source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 2) {
        return 3;
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.0f;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0) {
        return self.view.frame.size.height * 0.30;
    }else if (indexPath.section == 1 && indexPath.row == 0){
        return self.view.frame.size.height * 0.28;
    }else if (indexPath.section == 2){
        if (indexPath.row == 0 && hideETAPicker == YES) {
            return 0.01;
           // return 60;
        }else if (indexPath.row == 0 && hideETAPicker == NO){
          //  return 300;
            return 0.01;
        }else if (indexPath.row == 1 && hideCancelReasonTextfield == YES && etaSuccessfullySubmitted == NO){
            return 60;
        }else if (indexPath.row == 1 && hideCancelReasonTextfield == YES && etaSuccessfullySubmitted == YES){
            return 60;
        }else if (indexPath.row == 1 && hideCancelReasonTextfield == NO){
            //return 140;
            return 60;
        }else if (indexPath.row == 2){
            return 60;
        }
        //return self.view.frame.size.height *0.35;
    }
    return 280;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 && indexPath.row == 0) {
        ShiftConfirmationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"shiftConfirmedTableviewCell"];
        Request *request = _acceptedJobArray[self.currentIndex];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:kDateFormat];
        NSDate *date = [formatter dateFromString:request.shiftStartTime];
        self.requestIDNumber = request.requestID;
        NSDate *date2 = [formatter dateFromString:request.shiftEndTime];
        [formatter setDateFormat:kDisplayDateFormat];
        NSString *output = [formatter stringFromDate:date];
        NSString *output2 = [formatter stringFromDate:date2];
        
        cell.shiftTimesLabel.text = [NSString stringWithFormat:@"%@ - %@", output, output2];
        cell.jobTitleLabel.text = @"Personal Care Service";
        CLGeocoder* geocoder = [[CLGeocoder alloc] init];
        [geocoder geocodeAddressString:request.recipientAddress
                     completionHandler:^(NSArray* placemarks, NSError* error){
                         for (CLPlacemark* aPlacemark in placemarks)
                         {
                             double latCoord = aPlacemark.location.coordinate.latitude;
                             double longCoord = aPlacemark.location.coordinate.longitude;
                             self.latitudeCoord = [NSString stringWithFormat:@"%f", latCoord];
                             self.longitudeCoord = [NSString stringWithFormat:@"%f", longCoord];
                         }
                     }];
         

        //   cell.delegate = self;
        return cell;
    } else if (indexPath.section == 1 && indexPath.row == 0) {
        ShiftAddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"shiftAddressTableViewCell"];
        cell.getDirectionsButton.layer.borderColor = [UIColor grayColor].CGColor;
        Request *request = _acceptedJobArray[self.currentIndex];
        cell.shiftAddressTextView.text = [NSString stringWithFormat:@"%@ %@ \r%@", request.recipient.firstName, request.recipient.lastName, request.recipientAddress];
        cell.getDirectionsButton.layer.borderWidth = 1.0f; //make border 1px thick
        return cell;
        
    }else if (indexPath.section == 2 && indexPath.row == 0){
        SubmitETABeginShiftTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"submitETATableViewCell"];
        Request *request = _acceptedJobArray[self.currentIndex];
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setDateFormat:kDateFormat];
        NSDate *dateStr = [format dateFromString:request.rsvp.eta];
        self.etaString = [format stringFromDate:dateStr];
     //   NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
       // NSDate *currentDate = [NSDate date];
      //  NSDateComponents *comps = [[NSDateComponents alloc] init];
      //  [comps setDay:0];
      //  NSDate *minDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
      //  [comps setHour:30];
       // NSDate *maxDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
     //   [cell.submitETADatePicker setMaximumDate:maxDate];
  /////////////////      [cell.submitETADatePicker setDate:dateStr];
      //  [cell.submitETADatePicker setMinimumDate:minDate];
        
        if (hideETAPicker == YES) {
            cell.submitETAButton.hidden = YES;
            cell.chooseETAButton.hidden = NO;
        }else if (hideETAPicker == NO){
            cell.submitETAButton.hidden = NO;
            cell.chooseETAButton.hidden = YES;
        }
        return cell;
    }else if (indexPath.section == 2 && indexPath.row == 1){
        CancelTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cancelTableViewCell"];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        if (![cell.cancelReasonTextfield.text isEqualToString:@""]) {
            NSString *text = cell.cancelReasonTextfield.text;
            self.cancelationReasonString = text;
        }
        if (shouldShowConfirmCancelButton == NO) {
            cell.confirmCancelButton.hidden = YES;
            cell.cancelButton.hidden = NO;
        }else if (shouldShowConfirmCancelButton == YES){
            cell.confirmCancelButton.hidden = NO;
            cell.cancelButton.hidden = YES;
        }
        
        return cell;
    }else if (indexPath.section == 2 && indexPath.row == 2){
        BeginShiftTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"beginShiftTableViewCell"];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

//        if (etaSuccessfullySubmitted == NO) {
//            cell.beginShiftButton.alpha = 0.5;
//        }else if (etaSuccessfullySubmitted == YES){
//            cell.beginShiftButton.alpha = 1.0;
//        }
            return cell;
    }
    
    return nil;
}

//-(UITableViewCell *)tableView:(UITableView *)tableView loadingCellForRowAtIndexPath:(NSIndexPath *)indexPath andText:(NSString *)text {
//    LoadingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"loadingCell"];
//    if (cell.activityIndicator.isAnimating) {
//        [cell.activityIndicator stopAnimating];
//    }
//    cell.statusLabel.text = text;
//    return cell;
//}
-(IBAction)chooseETAButtonPressed:(id)sender{
    if (hideETAPicker == YES) {
        hideETAPicker = NO;
        [self.tableView reloadData];
    }else if (hideETAPicker == NO){
        hideETAPicker = YES;
        [self.tableView reloadData];
    }
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    NSLog(@"identifier:%@", identifier);
    if ([identifier isEqualToString:@"showAddressDetailViewControllerSegue"]) {
        NSLog(@"self.locManager.location.coordinate:%@", self.locManager.location);
        if (self.locManager.location.coordinate.latitude == 0.0 && self.locManager.location.coordinate.latitude == 0.0) {
            [self openInMapsPressed:nil];
            return NO;
        }
        return YES;
    }
    return YES;
}

-(void)openInMapsPressed:(id)sender {
    double latitude = [self.longitudeCoord doubleValue];
    double longitude = [self.latitudeCoord doubleValue];
    NSLog(@"latitude:%f", latitude);
    NSLog(@"longitude:%f", longitude);
    CLLocationCoordinate2D endingCoord = CLLocationCoordinate2DMake(latitude, longitude);
    MKPlacemark *endLocation = [[MKPlacemark alloc] initWithCoordinate:endingCoord addressDictionary:nil];
    MKMapItem *endingItem = [[MKMapItem alloc] initWithPlacemark:endLocation];
    
    NSMutableDictionary *launchOptions = [[NSMutableDictionary alloc] init];
    [launchOptions setObject:MKLaunchOptionsDirectionsModeDriving forKey:MKLaunchOptionsDirectionsModeKey];
    
    [endingItem openInMapsWithLaunchOptions:launchOptions];
    //    NSString *urlString = [[NSString alloc] initWithFormat:@"http://maps.apple.com/?q=%.5f,%.5f",
    //                           _address.latitude, _address.longitude];
    //    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showAddressDetailViewControllerSegue"]) {
    AddressDetailViewController *vc = [segue destinationViewController];
    vc.userLatitude = [NSString stringWithFormat:@"%f", self.locManager.location.coordinate.latitude];
    vc.userLongitude = [NSString stringWithFormat:@"%f", self.locManager.location.coordinate.longitude];
    vc.recipientLongitude = self.longitudeCoord;
    vc.recipientLatitude = self.latitudeCoord;
        NSLog(@"vc,userlate %@", vc.userLatitude);
        NSLog(@"vc.recip lat %@", vc.recipientLatitude);
    
    }
    
}


-(IBAction)submitButtonpressed:(id)sender{
    if (!self.cancelationReasonString) {
        self.cancelationReasonString = @" ";
    }
    NSDate *currentDate = [NSDate date];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:kDateFormat];
    NSString *dateStr = [format stringFromDate:currentDate];

    
    [NetworkingHelper editAnRSPVWithRSPVPID:self.RSVPIDNumber andPCAID:User.currentUser.pca.idNumber andRequestID:self.requestIDNumber andETA:dateStr andCancellationReason:self.cancelationReasonString andAcceptedBool:YES andDeclinedBool:NO andSuccess:^(id responseObject) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Success" message:@"Your RSVP has been sent to the recipient. You will be notified if they cancel your RSVP." preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }]];
        
        alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
        [self presentViewController:alert animated:YES completion:NULL];

        self->etaSuccessfullySubmitted = YES;
        self->hideETAPicker = YES;
        [self.tableView reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"failure %@", error);
        id responseObject = [NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:0 error:nil];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:[NSString stringWithFormat:@"%@", responseObject[@"error"]] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.tableView reloadData];
        }]];
        
        alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
        [self presentViewController:alert animated:YES completion:NULL];

    }];
}
-(IBAction)cancelButtonPressed:(id)sender{
    
    hideCancelReasonTextfield = NO;
    shouldShowConfirmCancelButton = YES;
    [self.tableView reloadData];

}


-(IBAction)confirmCancelButtonPressed:(id)sender {
    
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Are you sure you want to cancel your RSVP for this shift?" preferredStyle:UIAlertControllerStyleAlert];
//    [alert addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:NULL]];
//    [alert addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (!self.cancelationReasonString) {
            self.cancelationReasonString = @" ";
        }
//    NSDate *currentDate = [NSDate date];
//    NSDateFormatter *format = [[NSDateFormatter alloc] init];
//    [format setDateFormat:kDateFormat];
//    NSString *dateStr = [format stringFromDate:currentDate];

//        [NetworkingHelper editAnRSPVWithRSPVPID:self.RSVPIDNumber andPCAID:User.currentUser.pca.idNumber andRequestID:self.requestIDNumber andETA:dateStr andCancellationReason:self.cancelationReasonString andAcceptedBool:NO andDeclinedBool:YES andSuccess:^(id responseObject) {
//            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Success" message:@"Your RSVP has been canceled." preferredStyle:UIAlertControllerStyleAlert];
//            [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"cancelReason"];
//                for (UIViewController *controllers in self.navigationController.viewControllers) {
//                    if ([controllers isKindOfClass:[HomeTableViewController class]]) {
//                        HomeTableViewController *vc = (HomeTableViewController *)controllers;
//                        [self.navigationController popToViewController:vc animated:YES];
//                        break;
//                    }
//                }
//            }]];
//            
//            alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
//            [self presentViewController:alert animated:YES completion:NULL];
//            
//            etaSuccessfullySubmitted = YES;
//            hideETAPicker = YES;
//            [self.tableView reloadData];
//        } failure:^(NSURLSessionDataTask *task, NSError *error) {
//            NSLog(@"failure %@", error);
//        }];
        [self.tableView reloadData];
        [NetworkingHelper deleteRSVPWithRSVPId:self.RSVPIDNumber andSuccess:^(id responseObject) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Success" message:@"Your RSVP has been canceled." preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self returnToHomeScreen];
            }]];
            
            alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
            [self presentViewController:alert animated:YES completion:NULL];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            id responseObject = [NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:0 error:nil];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:[NSString stringWithFormat:@"%@", responseObject[@"error"]] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.tableView reloadData];
            }]];
            
            alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
            [self presentViewController:alert animated:YES completion:NULL];
        }];
}

-(void)returnToHomeScreen {
    for (UIViewController *controllers in self.navigationController.viewControllers) {
        if ([controllers isKindOfClass:[HomeTableViewController class]]) {
            HomeTableViewController *vc = (HomeTableViewController *)controllers;
            [self.navigationController popToViewController:vc animated:YES];
            break;
        }
    }
}

-(IBAction)beginTimesheetButtonPressed:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Confirm" message:@"This will take you to Timesheets. Are you ready to go there?" preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:NULL]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *customURL = [NSString stringWithFormat:@"homrcare-timesheets-system://?%@&sessionID=%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"token"], self->sessionDataIDString];
       NSLog(@"custom url is %@", customURL);
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:customURL]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:customURL]];
            
        } else {
            UIAlertController* alert2 = [UIAlertController alertControllerWithTitle:@"Error" message:@"Your version of Timesheets is outdated, or needs to be installed. Go to App Store?" preferredStyle:UIAlertControllerStyleAlert];
            
            [alert2 addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:NULL]];
            [alert2 addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                NSString *customURL = @"https://appsto.re/us/M6_4bb.i";
                if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:customURL]]) {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:customURL]];
                }
                }]];
                
                alert2.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
                [self presentViewController:alert2 animated:YES completion:NULL];
            
        }
    
    }]];
        alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
        [self presentViewController:alert animated:YES completion:NULL];
    
    
}

#pragma mark - Get Location Permission

-(void)getLocationPermission {
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways) {

    } else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
        _locManager = [CLLocationManager new];
        _locManager.delegate = self;
        [_locManager requestWhenInUseAuthorization];
    } else {
//        NSString *appName = [[[NSBundle mainBundle] infoDictionary] objectForKey:(id)kCFBundleNameKey];
//        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Location" message:[NSString stringWithFormat:@"To get the most out of %@, it is best to have location enabled. Would you like to change this in your phone settings?", appName] preferredStyle:UIAlertControllerStyleAlert];
//        [alert addAction:[UIAlertAction actionWithTitle:@"Sure" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//            [self goToPhoneSettings];
//        }]];
//        //        [alert addAction:[UIAlertAction actionWithTitle:@"No thanks" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
//        //            AddressDetailViewController *vc = [[UIStoryboard storyboardWithName:@"JobDetail" bundle:nil] instantiateViewControllerWithIdentifier:@"AddressDetailViewController"];
//        //            vc.office = _assignment.office;
//        //            [self.navigationController showViewController:vc sender:self];
//        //        }]];
//        [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:NULL]];
//        [self presentViewController:alert animated:YES completion:NULL];
    }
}

-(void)goToPhoneSettings {
    NSURL *appSettings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    [[UIApplication sharedApplication] openURL:appSettings];
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == kCLAuthorizationStatusAuthorizedAlways || status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [self getLocationPermission];
    } else if (status == kCLAuthorizationStatusNotDetermined) {
        [self.locManager requestWhenInUseAuthorization];
    } else if (status == kCLAuthorizationStatusDenied || kCLAuthorizationStatusRestricted) {
//        NSString *appName = [[[NSBundle mainBundle] infoDictionary] objectForKey:(id)kCFBundleNameKey];
//        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Location" message:[NSString stringWithFormat:@"To get the most out of %@, it is best to have location enabled. Would you like to change this in your phone settings?", appName] preferredStyle:UIAlertControllerStyleAlert];
//        [alert addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//            [self goToPhoneSettings];
//        }]];
//        //        [alert addAction:[UIAlertAction actionWithTitle:@"No thanks" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
//        //            AddressDetailViewController *vc = [[UIStoryboard storyboardWithName:@"JobDetail" bundle:nil] instantiateViewControllerWithIdentifier:@"AddressDetailViewController"];
//        //            vc.office = _assignment.office;
//        //            [self.navigationController showViewController:vc sender:self];
//        //        }]];
//        [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:NULL]];
//        [self presentViewController:alert animated:YES completion:NULL];

//        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
//            //            AddressDetailViewController *vc = [[UIStoryboard storyboardWithName:@"JobDetail" bundle:nil] instantiateViewControllerWithIdentifier:@"AddressDetailViewController"];
//            //            vc.office = _assignment.office;
//            //            [self.navigationController showViewController:vc sender:self];
//            [self goToPhoneSettings];
//        }]];
//        [self presentViewController:alert animated:YES completion:NULL];
    }
}





-(void)backButtonPressed{
    HomeTableViewController *homeVC = [[UIStoryboard storyboardWithName:@"OnDemandWorker" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeTableViewController"];
    homeVC.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationController setViewControllers:@[[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"loginTableViewController"], homeVC, [[UIStoryboard storyboardWithName:@"OnDemandWorker" bundle:nil] instantiateViewControllerWithIdentifier:@"UpcomingShiftsViewController"]] animated:YES];

    
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
             
             [self.locManager stopUpdatingLocation];
             self.locManager = manager;
             if (locations.count > 0) {
             }
             
}


//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.section == 2) {
//        }if (indexPath.row == 1 && hideCancelReasonTextfield == YES) {
//            hideCancelReasonTextfield = NO;
//            [self.tableView reloadData];
//
//        }else if (indexPath.row == 1 && hideCancelReasonTextfield == NO) {
//            hideCancelReasonTextfield = YES;
//            [self.tableView reloadData];
//            
//        }    
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
