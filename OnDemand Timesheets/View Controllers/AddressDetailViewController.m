//
//  AddressDetailViewController.m
//  Mimi
//
//  Created by Scott Mahonis on 2/16/16.
//  Copyright © 2016 The Jed Mahonis Group. All rights reserved.
//

#import "AddressDetailViewController.h"

@interface AddressDetailViewController ()

@end

@implementation AddressDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self fillLabels];
    [self mapViewSetUp];
   // NSString *urlString = [[NSString alloc] initWithFormat:@"http://maps.apple.com/?q=%.5f,%.5f",
                          // _address.latitude, _address.longitude];
    NSString* directionsURL = [NSString stringWithFormat:@"http://maps.apple.com/?saddr=%@,%@&daddr=%@,%@", self.userLatitude, self.userLongitude, self.recipientLatitude, self.recipientLongitude];
    NSLog(@"dicrections url %@", directionsURL);
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:directionsURL]]) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Directions" style:UIBarButtonItemStylePlain target:self action:@selector(openInMapsPressed:)];
    }
    
    
}

-(void)fillLabels {
    //self.title = @"address name";
    self.addressTopLabel.text = @"line 1 text";
//    if (_address.line2 && ![_address.line2 isEqualToString:@""]) {
//        self.addressTopLabel.text = [NSString stringWithFormat:@"%@ %@", _address.line1, _address.line2];
//    }
//    
//    self.addressBottomLabel.text = [NSString stringWithFormat:@"%@ %@, %@", _address.city, _address.region, _address.postalCode];
//    if (!_address.line1) {
//        self.addressTopLabel.text = @"Unknown Address";
//    }
//    if (!_address.city && !_address.region && !_address.postalCode) {
//        self.addressBottomLabel.text = @"Unknown Location";
//    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Location Delegate 

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    
}



#pragma mark - Map Delegate 

-(void)mapViewSetUp {
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways) {
//        _locManager = [[CLLocationManager alloc] init];
//        _locManager.delegate = self;
//        _locManager.desiredAccuracy = kCLLocationAccuracyBest;
//        [_locManager startUpdatingLocation];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appToBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appReturnsActive) name:UIApplicationDidBecomeActiveNotification object:nil];
        [self.mapView setDelegate:self];
        [self.mapView setUserTrackingMode:MKUserTrackingModeFollow];
        [self addDirectionsRequest];
    } else {
        double latitude = [self.recipientLatitude doubleValue];
        double longitude = [self.recipientLongitude doubleValue];
        self.mapView.centerCoordinate = CLLocationCoordinate2DMake(latitude,longitude);
        [_mapView setShowsUserLocation:NO];
    }
}

-(void)addDirectionsRequest {
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    double latitude = [self.recipientLatitude doubleValue];
    double longitude = [self.recipientLongitude doubleValue];
    [annotation setCoordinate:CLLocationCoordinate2DMake(latitude, longitude)];
    [_mapView addAnnotation:annotation];
    MKPlacemark *destPlacemark = [[MKPlacemark alloc] initWithCoordinate:CLLocationCoordinate2DMake(latitude, longitude) addressDictionary:nil];
    MKMapItem *destinationMapItem = [[MKMapItem alloc] initWithPlacemark:destPlacemark];
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
    [request setSource:[MKMapItem mapItemForCurrentLocation]];
    [request setDestination:destinationMapItem];
    [request setTransportType:MKDirectionsTransportTypeAny]; // This can be limited to automobile and walking directions.
    [request setRequestsAlternateRoutes:NO]; // Gives you several route options.
    MKDirections *directions = [[MKDirections alloc] initWithRequest:request];
    [directions calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
        if (!error) {
            for (MKRoute *route in [response routes]) {
                [self->_mapView addOverlay:[route polyline] level:MKOverlayLevelAboveRoads]; // Draws the route above roads, but below labels.
                // You can also get turn-by-turn steps, distance, advisory notices, ETA, etc by accessing various route properties.
//                [self centerMapBetweenCurrentAndEndingLocation];
                
                [self->_mapView setVisibleMapRect:[[route polyline] boundingMapRect] edgePadding:UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0) animated:YES];
            }
        }
    }];
}

-(MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay
{
    if ([overlay isKindOfClass:[MKPolyline class]]) {
        MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
        [renderer setStrokeColor:[UIColor blueColor]];
        [renderer setLineWidth:5.0];
        return renderer;
    }
    return nil;
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    MKMapRect mapRect = [self getZoomingRectOnMap:_mapView toFitAllOverlays:YES andAnnotations:YES includeUserLocation:YES];
    [_mapView setVisibleMapRect:mapRect edgePadding:UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0) animated:YES];
//    CLLocation *destination = [[CLLocation alloc] initWithLatitude:_address.latitude longitude:_address.longitude];
//    CLLocation *current = [[CLLocation alloc] initWithLatitude:userLocation.coordinate.latitude longitude:userLocation.coordinate.longitude];
//    CLLocationDistance distance = [current distanceFromLocation:destination];
//    float distanceDividedByOneHundredThousand = distance/100000;
//    [self.mapView setRegion:MKCoordinateRegionMake(userLocation.coordinate, MKCoordinateSpanMake(distanceDividedByOneHundredThousand, distanceDividedByOneHundredThousand))animated:YES];
    
}
                         
-(MKMapRect)getZoomingRectOnMap:(MKMapView*)map toFitAllOverlays:(BOOL)overlays andAnnotations:(BOOL)annotations includeUserLocation:(BOOL)userLocation {
    if (!map) {
        return MKMapRectNull;
    }
    
    NSMutableArray* overlaysAndAnnotationsCoordinateArray = [[NSMutableArray alloc]init];
    if (overlays) {
        for (id <MKOverlay> overlay in map.overlays) {
            MKMapPoint overlayPoint = MKMapPointForCoordinate(overlay.coordinate);
            NSArray* coordinate = @[[NSNumber numberWithDouble:overlayPoint.x], [NSNumber numberWithDouble:overlayPoint.y]];
            [overlaysAndAnnotationsCoordinateArray addObject:coordinate];
        }
    }
    
    if (annotations) {
        for (id <MKAnnotation> annotation in map.annotations) {
            MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
            NSArray* coordinate = @[[NSNumber numberWithDouble:annotationPoint.x], [NSNumber numberWithDouble:annotationPoint.y]];
            [overlaysAndAnnotationsCoordinateArray addObject:coordinate];
        }
    }
    
    MKMapRect zoomRect = MKMapRectNull;
    if (userLocation) {
        MKMapPoint annotationPoint = MKMapPointForCoordinate(map.userLocation.coordinate);
        zoomRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
    }
    
    for (NSArray* coordinate in overlaysAndAnnotationsCoordinateArray) {
        MKMapRect pointRect = MKMapRectMake([coordinate[0] doubleValue], [coordinate[1] doubleValue], 0.1, 0.1);
        zoomRect = MKMapRectUnion(zoomRect, pointRect);
    }
    
    return zoomRect;
}

- (void)appToBackground{
    [_mapView setShowsUserLocation:NO];
}

- (void)appReturnsActive{
    [_mapView setShowsUserLocation:YES];
}

-(void)openInMapsPressed:(id)sender {
    double latitude = [self.recipientLatitude doubleValue];
    double longitude = [self.recipientLongitude doubleValue];
    NSLog(@"latitude %f", latitude);
    NSLog(@"longitude %f", longitude);

    CLLocationCoordinate2D endingCoord = CLLocationCoordinate2DMake(latitude, longitude);
    MKPlacemark *endLocation = [[MKPlacemark alloc] initWithCoordinate:endingCoord addressDictionary:nil];
    MKMapItem *endingItem = [[MKMapItem alloc] initWithPlacemark:endLocation];
    
    NSMutableDictionary *launchOptions = [[NSMutableDictionary alloc] init];
    [launchOptions setObject:MKLaunchOptionsDirectionsModeDriving forKey:MKLaunchOptionsDirectionsModeKey];
    
    [endingItem openInMapsWithLaunchOptions:launchOptions];
//    NSString *urlString = [[NSString alloc] initWithFormat:@"http://maps.apple.com/?q=%.5f,%.5f",
//                           _address.latitude, _address.longitude];
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
}

@end
