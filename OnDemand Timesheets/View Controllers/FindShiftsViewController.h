//
//  FindShiftsViewController.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 10/17/16.
//  Copyright © 2016 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Request.h"
#import "RSVP.h"
#import <CoreLocation/CoreLocation.h>
#import "User.h"
#import "Shift.h"
#import "UserSettingsOptionsTableViewController.h"

typedef NS_ENUM(NSUInteger, DataStatus) {
    DataStatus_Loading,
    DataStatus_NotAvailable,
    DataStatus_None,
    DataStatus_AtLeastOneJobIsAvailable,
    DataStatus_Error,
};

typedef NS_ENUM(NSUInteger, UpcomingShiftStatus){
    UpcomingShiftStatus_NoUpcomingShifts,
    UpcomingShiftStatus_AtLeastOneShift,
    
};

@interface FindShiftsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate, UserSettingsOptionsTableViewControllerDelegate, UIPopoverPresentationControllerDelegate, UIPopoverControllerDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) User *currentUser;
@property (nonatomic, strong) NSMutableArray *availableJobsArray;
@property (nonatomic, strong) NSMutableArray *upcomingShiftsArray;
@property (nonatomic, strong) NSMutableArray *allRequestsArray;
@property (nonatomic, strong) NSMutableArray *acceptedJobsArray;
@property (nonatomic, strong) CLLocationManager *locManager;
@property (nonatomic) DataStatus status;
@property (nonatomic) UpcomingShiftStatus upcomingStatus;
@property (nonatomic, strong) Request *request;
@property (nonatomic, strong) RSVP *rsvp;
@property (nonatomic, strong) Shift *shift;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *settingsBarButtonItem;
@property (nonatomic, strong) NSString *errorString;

@end
