//
//  PushNotification.m
//  OnDemand Timesheets
//
//  Created by Scott Mahonis on 5/11/17.
//  Copyright © 2017 Jed Mahonis Group. All rights reserved.
//

#import "PushNotification.h"

@implementation PushNotification

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self == [super init]) {
        int pushNumberInt = [dictionary[@"push_type"] intValue] - 1;
        self.pushType = pushNumberInt;
    }
    return self;
}

@end
