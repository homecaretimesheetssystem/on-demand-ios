//
//  DirectRequestDetailViewController.h
//  OnDemand Timesheets
//
//  Created by Scott Mahonis on 3/26/18.
//  Copyright © 2018 Jed Mahonis Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Request.h"

@interface DirectRequestDetailViewController : UIViewController

@property (nonatomic, weak) IBOutlet UILabel *recipientNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *startTimeLabel;
@property (nonatomic, weak) IBOutlet UILabel *endTimeLabel;
@property (nonatomic, weak) IBOutlet UILabel *addressLabel;
@property (nonatomic, weak) IBOutlet UILabel *additionalRequirementsLabel;
@property (nonatomic, weak) IBOutlet UIButton *acceptButton;
@property (nonatomic, weak) IBOutlet UIButton *declineButton;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, strong) Request *request;

-(IBAction)acceptButtonPressed:(id)sender;
-(IBAction)declineButtonPressed:(id)sender;

@end
