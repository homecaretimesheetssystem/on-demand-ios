//
//  NetworkingHelper.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 3/26/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import <AFOAuth2Manager/AFOAuth2Manager.h>
#import "PCA.h"

@interface NetworkingHelper : NSObject

@property (strong) AFOAuthCredential *credential;

/*
 *
 */
+(void)loginWithParams:(NSDictionary *)params success:(void (^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask *task, NSError *error))failure;

+(void)resetPasswordWithParams:(NSDictionary *)params success:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+(void)getSessionDataWithSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


/*=================================== ON DEMAND API =========================================*/

+(void)getPCAWithID:(NSNumber *)pcaIDNumber andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+(void)updatePCAProfileWithPCAID:(NSNumber *)pcaIDNumber andFirstName:(NSString*)firstName andLastName:(NSString*)lastName andAddressString:(NSString*)address andYearsOfExperience:(NSString *)yearsOfExperience andGenderInt:(NSInteger)gender andQualificationsArray:(NSArray*)qualificationsArray andCountiesArray:(NSArray*)countiesArray andIsAvailable:(NSNumber *)available andAboutMe:(NSString *)aboutMe andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+(void)postPCAProfilePictureWithImageData:(NSData *)imageData andPCA:(PCA *)pca andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+(void)requestAPCAWithRecipientID:(NSNumber *)recipientID andAddress:(NSString *)address andGenderPreference:(NSInteger )genderPreference andShiftStartTime:(NSString*)shiftStartTime andShiftEndTime:(NSString*)shiftEndTime andYOE:(NSInteger )yearsOfExperience andQualificationsArray:(NSMutableArray *)qualificationsArray andAdditionalRequirements:(NSString *)additionalReqirements andPCAID:(NSNumber *)pcaID andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+(void)editAPCARequestWithRequestID:(NSNumber *)requestID andRecipientID:(NSNumber *)recipientID andAddress:(NSString *)address andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+(void)postAnRSVPWithPCAID:(NSNumber *)pcaID andRequestID:(NSNumber *)requestID andETA:(NSString *)etaDate andCancellationReason:(NSString *)cancellationReason andAcceptedBool:(BOOL)accepted andDeclinedBool:(BOOL)declined andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


+(void)editAnRSPVWithRSPVPID:(NSNumber *)rsvpID andPCAID:(NSNumber *)pcaID andRequestID:(NSNumber *)requestID andETA:(NSString *)etaDate andCancellationReason:(NSString *)cancellationReason andAcceptedBool:(BOOL)accepted andDeclinedBool:(BOOL)declined andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+(void)deleteRSVPWithRSVPId:(NSNumber *)rsvpID andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+(void)editAnRSPVWithPCAID:(NSNumber *)pcaID andRSPVPID:(NSNumber *)rsvpID andRequestID:(NSNumber *)requestID andETA:(NSString *)etaDate andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+(void)findAvailableShiftsPCA:(NSNumber *)agencyID andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+(void)sendTestNotifictionWithDeviceID:(NSString *)deviceID andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+(void)sendTestDATAWithDeviceID:(NSString *)deviceID andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+(void)getDeviceTokenWithUserID:(int)userID andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+(void)POSTDeviceToken:(BOOL)isAndroid andIsIOS:(BOOL)isIOS andIsActive:(BOOL)isActive andDeviceToken:(NSString*)deviceToken andUserID:(NSNumber *)userID andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+(void)seeUpcomingShiftsPCA:(NSNumber *)pcaID andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+(void)seeAllRequestsWithRecipID:(NSNumber *)recipID andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+(void)getCountiesWithSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+(void)getQualificationsWithSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+(void)seeIndividualRequestWithRequestID:(NSNumber *)requestID andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


+(void)deleteRequestWithRequestID:(NSNumber *)requestID andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+(void)getPCAContactsWithSuccess:(void (^)(NSArray *workersArray))successs failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+(NSString *)clientSecret;
+(NSString *)clientID;
+(void)setNetworkingHelperOAuthToken:(NSString *)token;

@end
