//
//  NetworkingHelper.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 3/26/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "NetworkingHelper.h"
#import <AFOAuth2Manager/AFOAuth2Manager.h>
#import <AFHTTPRequestSerializer+OAuth2.h>
#import "Constants.h"


@implementation NetworkingHelper

+(AFHTTPSessionManager *)sharedManager {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    AFHTTPRequestSerializer *reqSerializer = [AFJSONRequestSerializer serializer];
    [reqSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [reqSerializer setValue:[NSString stringWithFormat:@"Bearer %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"token"]] forHTTPHeaderField:@"Authorization"];
    AFOAuthCredential *credential = [NetworkingHelper credential];
    [reqSerializer setAuthorizationHeaderFieldWithCredential:credential];
    manager.requestSerializer = reqSerializer;
    return manager;
}

+(AFOAuthCredential *)credential {
    AFOAuthCredential *cred = [[AFOAuthCredential alloc] initWithOAuthToken:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] tokenType:@"Bearer"];
    long dateValue = -[[[NSUserDefaults standardUserDefaults] objectForKey:@"credExpiration"] longValue];
    [cred setExpiration:[NSDate dateWithTimeIntervalSinceNow:dateValue]];
    return cred;
}

//+(NSString *)urlForGettingToken {
//   return [NSString stringWithFormat:@"http://homecare-timesheet.herokuapp.com/oauth/v1/token?client_id=%@&client_secret=%@&grant_type=client_credentials", [NetworkingHelper clientID], [NetworkingHelper clientSecret]];
  
//}

+(void)loginWithParams:(NSDictionary *)params success:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSString *url = [NSString stringWithFormat:@"%@/login", kURL];
    [[self sharedManager] POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"response is %@", responseObject);
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error is %@", error);
        failure(task, error);
    }];
    
}

+(void)resetPasswordWithParams:(NSDictionary *)params success:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSString *url = [NSString stringWithFormat:@"%@/forgot", kURL];
    [[self sharedManager] POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        failure(task, error);
    }];
    
}



+(void)getSessionDataWithSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    [[self sharedManager] GET:[NSString stringWithFormat:@"%@/data", kURL] parameters:nil progress:NULL success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}




/*=================================== ON DEMAND API =========================================*/

+(void)updatePCAProfileWithPCAID:(NSNumber *)pcaIDNumber andFirstName:(NSString*)firstName andLastName:(NSString*)lastName andAddressString:(NSString*)address andYearsOfExperience:(NSString *)yearsOfExperience andGenderInt:(NSInteger)gender andQualificationsArray:(NSMutableArray*)qualificationsArray andCountiesArray:(NSMutableArray*)countiesArray andIsAvailable:(NSNumber *)available andAboutMe:(NSString *)aboutMe andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSLog(@"pca id number %@", yearsOfExperience);
    NSDictionary *params = @{@"homecare_profile":@{@"firstName":firstName,
                             @"lastName":lastName,
                             @"homeAddress":address,
                             @"gender":[NSNumber numberWithInteger:gender],
                             @"available":available,
                             @"qualifications":qualificationsArray,
                             @"counties":countiesArray,
                             @"about_me":aboutMe,
                             @"yearsOfExperience":yearsOfExperience}};
  //  NSLog(@"PARAMS %@", params);
    [[self sharedManager] PATCH:[NSString stringWithFormat:@"%@/pcas/%@", kURL, pcaIDNumber] parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
 //       NSLog(@"success response object %@", responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
        NSLog(@"failure error PATCH %@", error);
    }];
}


+(void)getPCAWithID:(NSNumber *)pcaIDNumber andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    [[self sharedManager] GET:[NSString stringWithFormat:@"%@/pcas/%@", kURL, pcaIDNumber] parameters:nil progress:NULL success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
        NSLog(@"success pca profile response object %@", responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
        NSLog(@"failure error %@", error);
    }];
}

+(void)postPCAProfilePictureWithImageData:(NSData *)imageData andPCA:(PCA *)pca andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
 
    [[self sharedManager] POST:[NSString stringWithFormat:@"%@/pcas/%@/image", kURL, pca.idNumber] parameters:nil  constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        [formData appendPartWithFileData:imageData name:@"profilePhoto" fileName:[NSString stringWithFormat:@"%@_%@.jpg", pca.firstName, pca.lastName] mimeType:@"image/jpeg"];//profilePhoto
    } progress:NULL success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"success response %@", responseObject);
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error fail %@", error);
        failure(task, error);
    }];
}

+(void)requestAPCAWithRecipientID:(NSNumber *)recipientID andAddress:(NSString *)address andGenderPreference:(NSInteger )genderPreference andShiftStartTime:(NSString*)shiftStartTime andShiftEndTime:(NSString*)shiftEndTime andYOE:(NSInteger )yearsOfExperience andQualificationsArray:(NSMutableArray *)qualificationsArray andAdditionalRequirements:(NSString *)additionalReqirements andPCAID:(NSNumber *)pcaID andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSMutableDictionary *mutDict = @{@"recipient":recipientID, @"address":address, @"genderPreference":[NSNumber numberWithInteger:genderPreference], @"startShiftTime":shiftStartTime, @"endShiftTime":shiftEndTime, @"yearsOfExperience":[NSNumber numberWithInteger:yearsOfExperience], @"additional_requirements":additionalReqirements, @"qualifications":qualificationsArray}.mutableCopy;
    if (![pcaID isEqualToNumber:@(-1)]) {
        [mutDict setObject:pcaID forKey:@"requested_id"];
    }
[[self sharedManager] POST:[NSString stringWithFormat:@"%@/requests", kURL] parameters:@{@"homecare_request":mutDict.copy} progress:NULL  success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    NSLog(@"REPSONSE %@", responseObject);
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error in call %@", error);
        failure(task, error);
    }];
}

+(void)findAvailableShiftsPCA:(NSNumber *)pcaID andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    [[self sharedManager] GET:[NSString stringWithFormat:@"%@/pcas/%@/available_requests", kURL, pcaID] parameters:nil progress:NULL success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"response object rrequestsssssss %@", responseObject);
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error in call %@", error);
        failure(task, error);
    }];
}


+(void)editAPCARequestWithRequestID:(NSNumber *)requestID andRecipientID:(NSNumber *)recipientID andAddress:(NSString *)address andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    [[self sharedManager] PATCH:[NSString stringWithFormat:@"%@/requests/%@", kURL, requestID] parameters:@{@"homecare_request":@{@"recipient":recipientID, @"address":address}} success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

+(void)postAnRSVPWithPCAID:(NSNumber *)pcaID andRequestID:(NSNumber *)requestID andETA:(NSString *)etaDate andCancellationReason:(NSString *)cancellationReason andAcceptedBool:(BOOL)accepted andDeclinedBool:(BOOL)declined andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSLog(@"address string %@", requestID);
    NSLog(@"gender string %@", etaDate);
    NSLog(@"end string %@", cancellationReason);

    [[self sharedManager] POST:[NSString stringWithFormat:@"%@/rsvps", kURL] parameters:@{@"homecare_rsvp":@{@"pca":pcaID, @"request":requestID, @"eta":etaDate, @"cancellationReason":cancellationReason, @"accepted":[NSNumber numberWithBool:accepted], @"declined":[NSNumber numberWithBool:declined]}} progress:NULL success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
   //     NSLog(@"response object post %@", responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

+(void)editAnRSPVWithRSPVPID:(NSNumber *)rsvpID andPCAID:(NSNumber *)pcaID andRequestID:(NSNumber *)requestID andETA:(NSString *)etaDate andCancellationReason:(NSString *)cancellationReason andAcceptedBool:(BOOL)accepted andDeclinedBool:(BOOL)declined andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    [[self sharedManager] PATCH:[NSString stringWithFormat:@"%@/rsvps/%@", kURL, rsvpID] parameters:@{@"homecare_rsvp":@{@"pca":pcaID, @"request":requestID, @"eta":etaDate, @"cancellationReason":cancellationReason, @"accepted":[NSNumber numberWithBool:accepted], @"declined":[NSNumber numberWithBool:declined]}} success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

+(void)deleteRSVPWithRSVPId:(NSNumber *)rsvpID andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    [[self sharedManager] DELETE:[NSString stringWithFormat:@"%@/rsvp/%@", kURL, rsvpID] parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
    
}


+(void)sendTestNotifictionWithDeviceID:(NSString *)deviceID andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSLog(@"notification call insiode netowkring helper");
    [[self sharedManager] GET:[NSString stringWithFormat:@"%@/notification/push", kURL] parameters:@{@"deviceToken":deviceID} progress:NULL success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
     //   NSLog(@"response object %@", responseObject);
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error in call %@", error);
        failure(task, error);
    }];
}


+(void)sendTestDATAWithDeviceID:(NSString *)deviceID andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSLog(@"notification call netowkring helper");
    [[self sharedManager] GET:[NSString stringWithFormat:@"%@/data/push", kURL] parameters:@{@"deviceToken":deviceID} progress:NULL success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
     //   NSLog(@"response object %@", responseObject);
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error in call %@", error);
        failure(task, error);
    }];
}
///api/v1/notification/push.{_format}
//GET  /api/v1/users/{user}/devices.{_format}

+(void)getDeviceTokenWithUserID:(int)userID andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
  //  NSLog(@"user id %d", userID);
    [[self sharedManager] GET:[NSString stringWithFormat:@"%@/users/%d/devices", kURL, userID] parameters:nil progress:NULL success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
      //  NSLog(@"response object %@", responseObject);
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error in call %@", error);
        failure(task, error);
    }];
}
+(void)POSTDeviceToken:(BOOL)isAndroid andIsIOS:(BOOL)isIOS andIsActive:(BOOL)isActive andDeviceToken:(NSString*)deviceToken andUserID:(NSNumber *)userID andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSLog(@"url for post deviceToken:%@", [NSString stringWithFormat:@"%@/devices", kURL] );
    [[self sharedManager] POST:[NSString stringWithFormat:@"%@/devices", kURL] parameters:@{@"homecare_device":@{@"isAndroid":[NSNumber numberWithBool:isAndroid], @"isIos":[NSNumber numberWithBool:isIOS], @"isActive":[NSNumber numberWithBool:isActive], @"deviceToken":deviceToken, @"user":userID}} progress:NULL success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
     //   NSLog(@"response object %@", responseObject);
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error in call %@", error);
        failure(task, error);
    }];
}


+(void)seeUpcomingShiftsPCA:(NSNumber *)pcaID andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    [[self sharedManager] GET:[NSString stringWithFormat:@"%@/pcas/%@/accepted_requests", kURL, pcaID] parameters:nil progress:NULL success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
     //   NSLog(@"response object upcoming shifts %@", responseObject);
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error in upcoming shifts %@", error);
        failure(task, error);
    }];
}

+(void)seeAllRequestsWithRecipID:(NSNumber *)recipID andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
  //  NSLog(@"pca id %@", pcaID);
    [[self sharedManager] GET:[NSString stringWithFormat:@"%@/recipient/%@/requests", kURL, recipID] parameters:nil progress:NULL success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
    //    NSLog(@"response object see all requests %@", responseObject);
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error in see all requests %@", error);
        failure(task, error);
    }];
}

+(void)seeIndividualRequestWithRequestID:(NSNumber *)requestID andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    //  NSLog(@"pca id %@", pcaID);
    NSLog(@"%@", [NSString stringWithFormat:@"%@/requests/%@", kURL, requestID]);
    [[self sharedManager] GET:[NSString stringWithFormat:@"%@/requests/%@", kURL, requestID] parameters:nil progress:NULL success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
    //    NSLog(@"response object individual requests %@", responseObject);
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error in see individual %@", error);
        failure(task, error);
    }];
}


+(void)getCountiesWithSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    [[self sharedManager] GET:[NSString stringWithFormat:@"%@/counties", kURL] parameters:nil progress:NULL success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
     //   NSLog(@"response object get counties %@", responseObject);
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error in get counties %@", error);
        failure(task, error);
    }];
}

+(void)getQualificationsWithSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    [[self sharedManager] GET:[NSString stringWithFormat:@"%@/qualifications", kURL] parameters:nil progress:NULL success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
   //     NSLog(@"response object get qualifications %@", responseObject);
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error in get qualification %@", error);
        failure(task, error);
    }];
}



+(void)deleteRequestWithRequestID:(NSNumber *)requestID andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    //  NSLog(@"pca id %@", pcaID);
    NSLog(@"%@", [NSString stringWithFormat:@"%@/requests/%@", kURL, requestID]);
    [[self sharedManager] DELETE:[NSString stringWithFormat:@"%@/requests/%@", kURL, requestID] parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
     //   NSLog(@"response object individual requests %@", responseObject);
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error in see individual %@", error);
        failure(task, error);
    }];
}

+(void)getPCAContactsWithSuccess:(void (^)(NSArray *))successs failure:(void (^)(NSURLSessionDataTask *, NSError *))failure {
    [[self sharedManager] GET:[NSString stringWithFormat:@"%@/pca_contacts", kURL] parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSMutableArray *mutArr = [NSMutableArray new];
        for (NSDictionary *dict in responseObject[@"pcas"]) {
            [mutArr addObject:[[PCA alloc] initWithDictionary:dict]];
        }
        successs(mutArr.copy);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

+(NSString *)clientID {
    return kClientID;
}

+(void)setNetworkingHelperOAuthToken:(NSString *)token {
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"token"];
}

@end
