//
//  BrowseWorkersTableViewController.m
//  OnDemand Timesheets
//
//  Created by Scott Mahonis on 3/26/18.
//  Copyright © 2018 Jed Mahonis Group. All rights reserved.
//

#import "BrowseWorkersTableViewController.h"
#import "CountiesTableViewCell.h"
#import "ViewProfileTableViewController.h"

@interface BrowseWorkersTableViewController ()

@end

@implementation BrowseWorkersTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"DataStatusTableViewCell" bundle:nil] forCellReuseIdentifier:@"DataStatusTableViewCell"];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self refresh:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)refresh:(id)sender {
    if ([sender isKindOfClass:[UIRefreshControl class]]) {
        [self.refreshControl endRefreshing];
    }
    self.isLoading = YES;
    self.errorString = nil;
    [self.tableView reloadData];
    [NetworkingHelper getPCAContactsWithSuccess:^(NSArray *workersArray) {
        self.dataArray = workersArray;
        self.errorString = nil;
        self.isLoading = NO;
        [self sortPcasIntoSections];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%s error:%@", __PRETTY_FUNCTION__, error);
        id responseObject = [NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:0 error:nil];
        NSLog(@"responseObject:%@", responseObject);
        self.errorString = responseObject[@"error"];
        self.isLoading = NO;
        [self.tableView reloadData];
    }];
}

-(void)sortPcasIntoSections {
    _sections = [[NSMutableDictionary alloc] init];
    BOOL found;
    NSMutableArray *array = self.isSearching ? self.searchArray.mutableCopy : self.dataArray.mutableCopy;
    // Loop through the books and create our keys
    for (PCA *temp in array)
    {
        NSString *c = [temp.firstName substringToIndex:1];
        found = NO;
        
        for (NSString *str in [_sections allKeys])
        {
            if ([str isEqualToString:c])
            {
                found = YES;
            }
        }
        
        if (!found)
        {
            [_sections setValue:[[NSMutableArray alloc] init] forKey:c];
        }
    }
    for (PCA *temp in self.dataArray) {
        [[_sections objectForKey:[temp.firstName substringToIndex:1]] addObject:temp];
        
    }
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.isLoading || self.errorString) {
        return 1;
    }
    return [[_sections allKeys] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.isLoading || self.errorString) {
        return 1;
    }
    return [[_sections valueForKey:[[[_sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section]] count];
}

-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return [[_sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [[[_sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.isLoading || self.errorString || self.dataArray.count == 0) {
        return self.view.frame.size.height - (self.navigationController.navigationBar.frame.size.height + [UIApplication sharedApplication].statusBarFrame.size.height + 44.);
    } else {
        return 44.;
    }
}

-(PCA *)pcaAtIndexPath:(NSIndexPath *)indexPath {
    return [[_sections valueForKey:[[[_sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.errorString) {
        DataStatusTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DataStatusTableViewCell"];
        [cell setUIToShowErrorWithErrorString:self.errorString];
        return cell;
    } else if (self.isLoading) {
        DataStatusTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DataStatusTableViewCell"];
        [cell setUIToShowLoadingWithText:nil];
        return cell;
    } else if (self.dataArray.count == 0) {
        DataStatusTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DataStatusTableViewCell"];
        [cell setUIToShowNoObjectsFoundWithImageNamed:nil andMainLabelText:@"No Workers Found!" andSubLabelText:@"Come back later when more people have signed up for PCA OnDemand!"];
        return cell;
    }
    CountiesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CountiesTableViewCell" forIndexPath:indexPath];
    PCA *pca = [self pcaAtIndexPath:indexPath];
    cell.countyLabel.text = [NSString stringWithFormat:@"%@ %@", pca.firstName, pca.lastName];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.errorString) {
        [self refresh:nil];
    }
    if (!self.isLoading && self.dataArray.count > 0) {
        ViewProfileTableViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewProfileTableViewController"];
        vc.pca = [self pcaAtIndexPath:indexPath];
        [self.navigationController showViewController:vc sender:nil];
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

#pragma mark - Search

-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    if (searchBar.text.length > 0) {
        _isSearching = YES;
        [self sortPcasIntoSections];
    }
    return YES;
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if (![searchText isEqualToString:@""]) {
        [self buildSearchArraysWithSearchText:searchText];
        _isSearching = YES;
        
    } else {
        _isSearching = NO;
    }
    [self sortPcasIntoSections];
}

-(void)buildSearchArraysWithSearchText:(NSString *)searchText {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.firstName contains[c] %@ OR SELF.lastName contains[c] %@",searchText, searchText];
    _searchArray = [NSMutableArray arrayWithArray:[_dataArray filteredArrayUsingPredicate:predicate]];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

@end
