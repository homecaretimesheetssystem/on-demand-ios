//
//  CountiesTableViewCell.h
//  OnDemand Timesheets
//
//  Created by Joey Kjornes on 3/17/17.
//  Copyright © 2017 Jed Mahonis Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountiesTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *countyLabel;

@end
